﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Proyecto_1.Models;
using System.Xml.Linq;
using System.Drawing.Printing;

namespace Proyecto_1.Models
{
    public class TipoGastosHandle
    {
        private SqlConnection con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["EscuelaCon"].ToString();
            con = new SqlConnection(constring);
        }
       

        public bool AgregarTipoGasto(TipoGastos tg)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarTipoGasto", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TipoGasto", tg.TipoGasto);
           
            con.Open();

            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
            {
                return true;
            }
            else
                return false;
        }

        public List<TipoGastos> ListarTipoGastos()
        {
            connection();
            List<TipoGastos> ListaTipoGastos = new List<TipoGastos>();

            SqlCommand cmd = new SqlCommand("ListarTipoGastos", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {

                ListaTipoGastos.Add(
                    new TipoGastos
                    {
                        idTipoGasto = Convert.ToInt32(dr["idTipoGasto"]),
                        TipoGasto = Convert.ToString(dr["Tipo"]),
                       
                    });
            }
            return ListaTipoGastos;

        }

        public List<TipoGastos> ListarTipoGastos2()
        {
            connection();
            List<TipoGastos> ListaTipoGastos = new List<TipoGastos>();

            SqlCommand cmd = new SqlCommand("ListarTipoGastos2", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {

                ListaTipoGastos.Add(
                    new TipoGastos
                    {
                        idTipoGasto = Convert.ToInt32(dr["idTipoGasto"]),
                        TipoGasto = Convert.ToString(dr["Tipo"]),

                    });
            }
            return ListaTipoGastos;

        }

        public bool EsTipoGastoRepetido(String tipogasto)
        {
            connection();
            SqlCommand cmd = new SqlCommand("EsTipoGastoRepetido", con);
            cmd.Parameters.AddWithValue("@tipo", tipogasto);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}