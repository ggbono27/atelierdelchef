﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Proyecto_1.Models
{
    public class ProductosHandle
    {
        private SqlConnection con;

        private void connection()
        {
            string ConString = ConfigurationManager.ConnectionStrings["EscuelaCon"].ToString();
            con = new SqlConnection(ConString);
        }


        public bool AgregarProductoInsumo(Productos P)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarProductoInsumo", con);
            cmd.CommandType = CommandType.StoredProcedure;
            P.Tipo = "Insumo";

            cmd.Parameters.AddWithValue("@Nombre", P.Nombre);
            cmd.Parameters.AddWithValue("@Unidad", P.Unidad);
            cmd.Parameters.AddWithValue("@Tipo", P.Tipo);


            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;

            else
                return false;
        }

        public bool AgregarProductoBazar(Productos P)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarProductoBazar", con);
            cmd.CommandType = CommandType.StoredProcedure;
            P.Tipo = "Bazar";

            cmd.Parameters.AddWithValue("@Nombre", P.Nombre);
            cmd.Parameters.AddWithValue("@Tipo", P.Tipo);
            cmd.Parameters.AddWithValue("@Precio", P.Precio);


            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;

            else
                return false;
        }

        public List<Productos> ListarProductosInsumos()
        {
            connection();
            List<Productos> ListadoProductos = new List<Productos>();

            SqlCommand cmd = new SqlCommand("ListarStockInsumo", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoProductos.Add(
                    new Productos
                    {
                        idProducto = Convert.ToInt32(dr["idProducto"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Unidad = Convert.ToString(dr["Unidad"]),
                        Stock = Convert.ToDecimal(dr["Stock"])
                    });
            }
            return ListadoProductos;

        }


        public bool EsProductoRepetido(String nombre)
        {
            connection();
            List<Alumno> ListadoAlumnos = new List<Alumno>();

            SqlCommand cmd = new SqlCommand("EsProductoRepetido", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            cmd.Parameters.AddWithValue("@nombre", nombre);
            con.Open();
            sd.Fill(dt);
            con.Close();

            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool EsInsumoRepetido(String nombre)
        {
            connection();
            List<Alumno> ListadoAlumnos = new List<Alumno>();

            SqlCommand cmd = new SqlCommand("EsInsumoRepetido", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            cmd.Parameters.AddWithValue("@nombre", nombre);
            con.Open();
            sd.Fill(dt);
            con.Close();

            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public List<Productos> ListarProductosBazar()
        {
            connection();
            List<Productos> ListadoProductos = new List<Productos>();

            SqlCommand cmd = new SqlCommand("ListarStockBazar", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoProductos.Add(
                    new Productos
                    {
                        idProducto = Convert.ToInt32(dr["idProducto"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Precio = Convert.ToInt32(dr["Precio"]),
                        Stock = Convert.ToInt32(dr["Stock"])
                    });
            }
            return ListadoProductos;

        }

        public bool EditarProducto(Productos producto)
        {

            connection();
            SqlCommand cmd = new SqlCommand("EditarProducto", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@id", producto.idProducto);
            cmd.Parameters.AddWithValue("@Nombre", producto.Nombre);
            cmd.Parameters.AddWithValue("@Unidad", producto.Unidad);


            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public bool EditarProductoBazar(Productos producto)
        {

            connection();
            SqlCommand cmd = new SqlCommand("EditarProductoBazar", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@id", producto.idProducto);
            cmd.Parameters.AddWithValue("@Nombre", producto.Nombre);
            cmd.Parameters.AddWithValue("@Precio", producto.Precio);


            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public bool BorrarProducto(int id)
        {
            connection();
            SqlCommand cmd = new SqlCommand("BorrarProducto", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i > 1)
                return true;
            else
                return false;
        }

        public List<Productos> ListarInsumos()
        {
            connection();
            List<Productos> ListadoProductos = new List<Productos>();

            SqlCommand cmd = new SqlCommand("ListarInsumosParaRecetas", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoProductos.Add(
                    new Productos
                    {
                        idProducto = Convert.ToInt32(dr["idProducto"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Unidad = Convert.ToString(dr["Unidad"]),
                        Tipo = Convert.ToString(dr["Tipo"]),
                    });
            }
            return ListadoProductos;
        }


        public List<Productos> BuscarProductosVentas(String cadena)
        {
            connection();
            List<Productos> ListadoProductos = new List<Productos>();
            SqlCommand cmd = new SqlCommand("BuscarProductoBazar", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@cadena", cadena);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoProductos.Add(
                    new Productos
                    {
                        idProducto = Convert.ToInt32(dr["idProducto"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Precio = Convert.ToInt32(dr["Precio"]),
                        Stock = Convert.ToInt32(dr["Stock"])

                    });
            }
            return ListadoProductos;
        }

        public List<Productos> BuscarProductosInsumo(String cadena)
        {
            connection();
            List<Productos> ListadoProductos = new List<Productos>();
            SqlCommand cmd = new SqlCommand("BuscarProdInsumo", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@cadena", cadena);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoProductos.Add(
                    new Productos
                    {
                        idProducto = Convert.ToInt32(dr["idProducto"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Unidad = Convert.ToString(dr["Unidad"]),
                        Stock = Convert.ToInt32(dr["Stock"])

                    });
            }
            return ListadoProductos;
        }

    }
}




