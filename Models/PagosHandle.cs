﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Proyecto_1.ViewModels;

namespace Proyecto_1.Models
{
    public class PagosHandle
    {
        private SqlConnection con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["EscuelaCon"].ToString();
            con = new SqlConnection(constring);
        }

        public bool AgregarPago(Pagos p)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarPago", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Concepto", p.Concepto);
            cmd.Parameters.AddWithValue("@Monto", p.Monto);
            cmd.Parameters.AddWithValue("@Fecha", p.Fecha);
            cmd.Parameters.AddWithValue("@Usuario", p.Usuario);
            cmd.Parameters.AddWithValue("@idAlumno", p.idAlumno);
            cmd.Parameters.AddWithValue("@idTurno", p.idTurno);
            cmd.Parameters.AddWithValue("@idCurso", p.idCurso);
            cmd.Parameters.AddWithValue("@control", p.Control);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public List<Pagos> ListarPagos()
        {
            connection();
            List<Pagos> ListadoPagos = new List<Pagos>();

            SqlCommand cmd = new SqlCommand("ListarPagos", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoPagos.Add(
                    new Pagos
                    {
                        idTurno = Convert.ToInt32(dr["idPago"]),
                        idPago = Convert.ToInt32(dr["Monto"]),
                        idCurso = Convert.ToInt32(dr["idCurso"]),
                        idAlumno = Convert.ToInt32(dr["idAlumno"]),
                        Monto = Convert.ToInt32(dr["Monto"]),
                        Fecha = Convert.ToDateTime(dr["Fecha"]),
                        Concepto = Convert.ToString(dr["Concepto"]),
                    });
            }
            return ListadoPagos;
        }



        public List<Caja> ListarPagosCaja()
        {
            connection();
            List<Caja> ListadoCaja = new List<Caja>();
            SqlCommand cmd = new SqlCommand("ListarPagos", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoCaja.Add(
                    new Caja
                    {
                        Monto = Convert.ToInt32(dr["Monto"]),
                        Fecha = Convert.ToDateTime(dr["Fecha"]),
                        Concepto = Convert.ToString(dr["Concepto"]) + " - " + Convert.ToString(dr["Nombre"]),
                        Usuario = Convert.ToString(dr["Usuario"]),
                        Concepto2 = Convert.ToString(dr["Concepto"])
                    });
            }
            return ListadoCaja;
        }


        public List<Caja> ListarPagosCajaPorRangoFecha(DateTime fecha1, DateTime fecha2)
        {
            connection();
            List<Caja> ListadoCaja = new List<Caja>();

            SqlCommand cmd = new SqlCommand("ListarPagosPorFecha", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fecha", fecha1);
            cmd.Parameters.AddWithValue("@fecha2", fecha2);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoCaja.Add(
                    new Caja
                    {
                        Monto = Convert.ToDecimal(dr["Monto"]),
                        Fecha = Convert.ToDateTime(dr["Fecha"]),
                        Usuario = Convert.ToString(dr["Usuario"]),
                        Concepto = Convert.ToString(dr["Concepto"]) + " - " + Convert.ToString(dr["Nombre"]),
                        Concepto2 = Convert.ToString(dr["Concepto"])
                    });
            }

            return ListadoCaja;
        }

    }
}