﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Proyecto_1.Models
{
    public class Cursos
    {
        public int idCurso { get; set; }

        [Required]
        [DisplayName("Nombre:")]
        public string Nombre { get; set; }

        [Required]
        [DisplayName("Descripción:")]
        public string Descripcion { get; set; }

        [Required]
        [DisplayName("Costo:")]
        [RegularExpression("^[0-9]{1,6}$", ErrorMessage = "Debe ingresar solo numeros")]
        public int Costo { get; set; }

        public Cursos()
        { }

        public Cursos(int idC, String Nomb, String Desc, int Cost)
        {
            idCurso = idC;
            Nombre = Nomb;
            Costo = Cost;
            Descripcion = Desc;
        }

        public Cursos(String Nomb, String Desc, int Cost)
        {
            Nombre = Nomb;
            Costo = Cost;
            Descripcion = Desc;
        }
    }
}