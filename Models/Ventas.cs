﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_1.Models
{
    public class Ventas
    {
        public int idVenta { get; set; }
        public DateTime FechaVenta { get; set; }
        public String TipoVenta { get; set; }

        public int Total { get; set; }
         
        public String Descripcion { get; set; }
        public String Usuario { get; set; }

        public Ventas()
        { }


        public Ventas(int idV, DateTime fechaventa, String tipo, int total, String usuario, string descripcion)
        {
            idVenta = idV;
            FechaVenta = fechaventa;
            TipoVenta = tipo;
            Total = total;
            Usuario = usuario;
            Descripcion = descripcion;
        }

        public Ventas(DateTime fechaventa, String tipo, String usuario, string descripcion )
        {
            FechaVenta = fechaventa;
            TipoVenta = tipo;
            Usuario = usuario;
            Descripcion = descripcion;
        }

       


    } 
}