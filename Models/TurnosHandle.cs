﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Proyecto_1.Models
{
    public class TurnosHandle
    {
        private SqlConnection con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["EscuelaCon"].ToString();
            con = new SqlConnection(constring);
        }

        public bool AgregarTurno(Turnos t)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarTurno", con);
            cmd.CommandType = CommandType.StoredProcedure;

            switch (t.FechaInicio.DayOfWeek.ToString())
            {
                case "Monday":
                    t.Dia = "Lunes";
                    break;
                case "Tuesday":
                    t.Dia = "Martes";
                    break;
                case "Wednesday":
                    t.Dia = "Miercoles";
                    break;
                case "Thursday":
                    t.Dia = "Jueves";
                    break;
                case "Friday":
                    t.Dia = "Viernes";
                    break;
                case "Saturday":
                    t.Dia = "Sabado";
                    break;
                case "Sunday":
                    t.Dia = "Domingo";
                    break;
            }

            cmd.Parameters.AddWithValue("@idCurso", t.idCurso);
            cmd.Parameters.AddWithValue("@Dia", t.Dia);
            cmd.Parameters.AddWithValue("@Inicio", t.Inicio);
            cmd.Parameters.AddWithValue("@Fin", t.Fin);
            cmd.Parameters.AddWithValue("@FechaInicio", t.FechaInicio.Date);
            cmd.Parameters.AddWithValue("@CantidadAlumnos", t.Alumnos);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public bool EditarTurno(Turnos t)
        {
            connection();
            SqlCommand cmd = new SqlCommand("EditarTurno", con);
            cmd.CommandType = CommandType.StoredProcedure;

            switch (t.FechaInicio.DayOfWeek.ToString())
            {
                case "Monday":
                    t.Dia = "Lunes";
                    break;
                case "Tuesday":
                    t.Dia = "Martes";
                    break;
                case "Wednesday":
                    t.Dia = "Miercoles";
                    break;
                case "Thursday":
                    t.Dia = "Jueves";
                    break;
                case "Friday":
                    t.Dia = "Viernes";
                    break;
                case "Saturday":
                    t.Dia = "Sabado";
                    break;
                case "Sunday":
                    t.Dia = "Domingo";
                    break;
            }

            //cmd.Parameters.AddWithValue("@idCurso", t.idCurso);
            cmd.Parameters.AddWithValue("@idTurno", t.idTurno);
            cmd.Parameters.AddWithValue("@Dia", t.Dia);
            cmd.Parameters.AddWithValue("@Inicio", t.Inicio);
            cmd.Parameters.AddWithValue("@Fin", t.Fin);
            cmd.Parameters.AddWithValue("@FechaInicio", t.FechaInicio);
            cmd.Parameters.AddWithValue("@CantidadAlumnos", t.Alumnos);
            
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public bool BorrarTurno(int idturno)
        {
            connection();
            SqlCommand cmd = new SqlCommand("BorrarTurno", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@idturno", idturno);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public void Desinscribir(int idAlumno, int idTurno)
        {
            connection();
            SqlCommand cmd = new SqlCommand("Desinscribir", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@idTurno", idTurno);
            cmd.Parameters.AddWithValue("@idAlumno", idAlumno);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();
            
        }

        public List<Turnos> ListarTurnos()
        {
            connection();
            List<Turnos> ListadoTurnos = new List<Turnos>();

            SqlCommand cmd = new SqlCommand("ListarTurnos", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoTurnos.Add(
                    new Turnos
                    {
                        idTurno= Convert.ToInt32(dr["idTurno"]),
                        idCurso = Convert.ToInt32(dr["idCurso"]),
                        Dia = Convert.ToString(dr["Dia"]),
                        Inicio = Convert.ToString(dr["Inicio"]),
                        Fin = Convert.ToString(dr["Fin"]),
                        FechaInicio = Convert.ToDateTime(dr["FechaInicio"]),
                    });
            }
            return ListadoTurnos;
        }

        public List<Turnos> ListarTurnosDeUnCurso(int id)
        {
            connection();
            List<Turnos> ListadoTurnos = new List<Turnos>();

            SqlCommand cmd = new SqlCommand("ListarTurnosC", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idCurso", id);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoTurnos.Add(
                    new Turnos
                    {
                        idTurno = Convert.ToInt32(dr["idTurno"]),
                        idCurso = Convert.ToInt32(dr["idCurso"]),
                        Dia = Convert.ToString(dr["Dia"]),
                        Inicio = Convert.ToString(dr["Inicio"]),
                        Fin = Convert.ToString(dr["Fin"]),
                        FechaInicio = Convert.ToDateTime(dr["FechaInicio"]),
                        s_FechaInicio = Convert.ToString(dr["fecha"]),
                        Alumnos = Convert.ToInt32(dr["CantidadAlumnos"])
                    });
            }
            return ListadoTurnos;
        }

        public List<Turnos> ListarTurnosDeUnCursoSinRestriccion(int id)
        {
            connection();
            List<Turnos> ListadoTurnos = new List<Turnos>();

            SqlCommand cmd = new SqlCommand("ListarTurnosDeUnCursoSinRestriccion", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idCurso", id);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoTurnos.Add(
                    new Turnos
                    {
                        idTurno = Convert.ToInt32(dr["idTurno"]),
                        idCurso = Convert.ToInt32(dr["idCurso"]),
                        Dia = Convert.ToString(dr["Dia"]),
                        Inicio = Convert.ToString(dr["Inicio"]),
                        Fin = Convert.ToString(dr["Fin"]),
                        FechaInicio = Convert.ToDateTime(dr["FechaInicio"]),
                        s_FechaInicio = Convert.ToString(dr["fecha"]),
                        Alumnos = Convert.ToInt32(dr["CantidadAlumnos"])
                    });
            }
            return ListadoTurnos;
        }

        public int RecuperarIdTurno()
        {
            int idTurno=0;
            connection();

            SqlCommand cmd = new SqlCommand("RecuperarIdTurno", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                idTurno = Convert.ToInt32(dr["idTurno"]);
            }

            return idTurno;
        }

        public List<Alumno> AlumnosInscriptos(int idturno)
        {
            connection();
            List<Alumno> ListaAlumnos = new List<Alumno>();
            SqlCommand cmd = new SqlCommand("AlumnosInscriptos", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idTurno", idturno);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListaAlumnos.Add(
                    new Alumno
                    {
                        idAlumno = Convert.ToInt32(dr["idAlumno"]),
                        Apellido = Convert.ToString(dr["Apellido"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Dni = Convert.ToInt32(dr["DNI"]),
                        Telefono= Convert.ToString(dr["Telefono"]),
                        Mail= Convert.ToString(dr["Mail"])
                    });
            }
            return ListaAlumnos;
        }

        public List<Alumno> AlumnosInscriptos2(int idturno, int idcurso)
        {
            CursosHandle cuh = new CursosHandle();
            TurnosHandle th = new TurnosHandle();
             var costo = cuh.TraerCostoCurso(idcurso).FirstOrDefault().Costo;
            //var sumaPago = th.DetallePagoAlumno(idturno, id).Sum(x => x.Monto);
            //var Adeuda = costo - sumaPago;
            connection();
            List<Alumno> ListaAlumnos = new List<Alumno>();
            SqlCommand cmd = new SqlCommand("AlumnosInscriptos", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idTurno", idturno);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                var sumaPago = th.DetallePagoAlumno(idturno, Convert.ToInt32(dr["idAlumno"])).Sum(x => x.Monto);
                //var Adeuda = costo - sumaPago;
                ListaAlumnos.Add(
                    new Alumno
                    {
                        idAlumno = Convert.ToInt32(dr["idAlumno"]),
                        Apellido = Convert.ToString(dr["Apellido"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Dni = costo - sumaPago,
                        Telefono = Convert.ToString(dr["Telefono"]),
                        Mail = Convert.ToString(dr["Mail"])
                    });
            }
            return ListaAlumnos;
        }

        public bool PagoAlumno(int monto, int idalumno, int idturno, int idcurso, string usuario, DateTime fecha)
        {
            connection();

            SqlCommand cmd = new SqlCommand("PagoAlumno", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idTurno", idturno);
            cmd.Parameters.AddWithValue("@idAlumno", idalumno);
            cmd.Parameters.AddWithValue("@idCurso", idcurso);
            cmd.Parameters.AddWithValue("@Monto", monto);
            cmd.Parameters.AddWithValue("@Concepto", "Cuota");
            cmd.Parameters.AddWithValue("@Fecha", fecha);
            cmd.Parameters.AddWithValue("@Usuario", usuario);
            cmd.Parameters.AddWithValue("@control", "-");

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;

        }

        public List<Pagos> DetallePagoAlumno(int idturno, int idAlumno)
        {
            connection();
            List<Pagos> ListaPagos = new List<Pagos>();
            SqlCommand cmd = new SqlCommand("DetallePagoAlumno", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idTurno", idturno);
            cmd.Parameters.AddWithValue("@idAlumno", idAlumno);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListaPagos.Add(
                    new Pagos
                    {
                        Concepto = Convert.ToString(dr["Concepto"]),
                        Monto = Convert.ToInt32(dr["Monto"]),
                        Fecha = Convert.ToDateTime(dr["Fecha"]),
                        Usuario= Convert.ToString(dr["Usuario"]),
                        Control= Convert.ToString(dr["Contr"])
                    });
            }
            return ListaPagos;
        }

        public List<Turnos> ListarTurnos2()
        {
            connection();
            List<Turnos> ListadoTurnos = new List<Turnos>();

            SqlCommand cmd = new SqlCommand("ListarTurnos2", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoTurnos.Add(
                    new Turnos
                    {
                        idTurno = Convert.ToInt32(dr["idTurno"]),
                        idCurso = Convert.ToInt32(dr["idCurso"]),
                        Dia = Convert.ToString(dr["Dia"]),
                        Inicio = Convert.ToString(dr["Inicio"]),
                        Fin = Convert.ToString(dr["Fin"]),
                        FechaInicio = Convert.ToDateTime(dr["FechaInicio"]),
                        Alumnos= Convert.ToInt32(dr["cantInsc"]),
                    });
            }
            return ListadoTurnos;
        }

        public List<Turnos> ListarTurnosDeUnHorario(string dia, string inicio, string fin, DateTime fechaInicio)
        {
            connection();
            List<Turnos> ListadoTurnos = new List<Turnos>();

            SqlCommand cmd = new SqlCommand("ListarTurnosDeUnHorario", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Dia", dia);
            cmd.Parameters.AddWithValue("@Inicio", inicio);
            cmd.Parameters.AddWithValue("@Fin", fin);
            cmd.Parameters.AddWithValue("@FechaInicio", fechaInicio);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoTurnos.Add(
                    new Turnos
                    {
                        idTurno = Convert.ToInt32(dr["idTurno"]),
                        idCurso = Convert.ToInt32(dr["idCurso"]),
                        Dia = Convert.ToString(dr["Dia"]),
                        Inicio = Convert.ToString(dr["Inicio"]),
                        Fin = Convert.ToString(dr["Fin"]),
                        FechaInicio = Convert.ToDateTime(dr["FechaInicio"]),
                    });
            }
            return ListadoTurnos;
        }

        public List<Clases> ListarTurnoDeClase(int idClase)
        {
            connection();
            List<Clases> ListadoClases = new List<Clases>();

            SqlCommand cmd = new SqlCommand("ListarTurnoDeClase", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idClase", idClase);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoClases.Add(
                    new Clases
                    {
                        idTurno = Convert.ToInt32(dr["idTurno"]),
                        idClase= Convert.ToInt32(dr["idClase"]),
                        idCurso = Convert.ToInt32(dr["idCurso"]),
                       Descripcion= Convert.ToString(dr["Descripcion"]),
                       FechaFin= Convert.ToDateTime(dr["HoraFin"]),
                      FechaInicio= Convert.ToDateTime(dr["HoraInicio"]),
                    });
            }
            return ListadoClases;


        }

    

        public List<Turnos> CantidadAlumnosPorTurno(int idTurno)
        {
            connection();
            List<Turnos> ListadoTurnos = new List<Turnos>();

            SqlCommand cmd = new SqlCommand("CantidadAlumnosPorTurno", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idTurno", idTurno);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoTurnos.Add(
                    new Turnos
                    {
                        Alumnos = Convert.ToInt32(dr["CantidadAlumnos"]),
                    });
            }
            return ListadoTurnos;
        }

        public List<Turnos> TraeridCurso(int idTurno)
        {
            connection();
            List<Turnos> ListadoTurno = new List<Turnos>();

            SqlCommand cmd = new SqlCommand("TraeridCurso", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idTurno", idTurno);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoTurno.Add(
                    new Turnos
                    {
                        idCurso = Convert.ToInt32(dr["idCurso"]),
                    });
            }
            return ListadoTurno;
        }
    }
}