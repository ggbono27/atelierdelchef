﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_1.Models
{
    public class Usuarios
    {
        public int idUsuario { get; set; }
        public String UserName { get; set; }
        public String Pass { get; set; }

        public String Salt { get; set; }
        public Usuarios()
        { }

        public Usuarios(int idU, string user, string pass, string salt)
        {
            idUsuario = idU;
            UserName = user;
            Pass = pass;
            Salt = salt;
        }
        public Usuarios(string user, string pass)
        {
            UserName = user;
            Pass = pass;
        }
    }
}