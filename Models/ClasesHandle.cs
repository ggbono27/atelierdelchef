﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Proyecto_1.ViewModels;

namespace Proyecto_1.Models
{
    public class ClasesHandle
    {
        private SqlConnection _con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["EscuelaCon"].ToString();
            _con = new SqlConnection(constring);
        }

        public bool AgregarClase(Clases clase)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarClase", _con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Descripcion", clase.Descripcion);
            cmd.Parameters.AddWithValue("@FechaInicio", clase.FechaInicio);
            cmd.Parameters.AddWithValue("@FechaFin", clase.FechaFin);
            cmd.Parameters.AddWithValue("@idCurso", clase.idCurso);
            cmd.Parameters.AddWithValue("@idTurno", clase.idTurno);

            _con.Open();
            int i = cmd.ExecuteNonQuery();
            _con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public List<Clases> ListarClases()
        {
            connection();
            List<Clases> ListadoClases = new List<Clases>();

            SqlCommand cmd = new SqlCommand("ListarClases", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoClases.Add(
                    new Clases
                    {
                        Descripcion = Convert.ToString(dr["Descripcion"]),
                        idClase = Convert.ToInt32(dr["idClase"]),
                        idCurso = Convert.ToInt32(dr["idCurso"]),
                        idTurno = Convert.ToInt32(dr["idTurno"]),
                        fecha = Convert.ToString(dr["fecha"]),
                        FechaInicio = Convert.ToDateTime(dr["HoraInicio"]),
                        FechaFin = Convert.ToDateTime(dr["HoraFin"])
                    });
            }
            return ListadoClases;
        }


        public List<Clases> ListarClasesDeUnaFecha(DateTime fechainicio, DateTime fechafin, int idTurno)
        {
            connection();
            List<Clases> ListadoClases = new List<Clases>();

            SqlCommand cmd = new SqlCommand("ListarClasesDeUnaFecha", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FechaInicio", fechainicio.AddMinutes(1));
            cmd.Parameters.AddWithValue("@FechaFin", fechafin.AddMinutes(-1));
            cmd.Parameters.AddWithValue("@idTurno", idTurno);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoClases.Add(
                    new Clases
                    {
                        Descripcion = Convert.ToString(dr["Descripcion"]),
                        idClase = Convert.ToInt32(dr["idClase"]),
                        idCurso = Convert.ToInt32(dr["idCurso"]),
                        idTurno = Convert.ToInt32(dr["idTurno"]),
                        fecha = Convert.ToString(dr["fecha"]),
                        FechaInicio = Convert.ToDateTime(dr["HoraInicio"]),
                        FechaFin = Convert.ToDateTime(dr["HoraFin"])
                    });
            }
            return ListadoClases;
        }

        public List<Clases> ListarClasesDeUnaFechaDeCualquierTurno(DateTime fechainicio, DateTime fechafin)
        {
            connection();
            List<Clases> ListadoClases = new List<Clases>();

            SqlCommand cmd = new SqlCommand("ListarClasesDeUnaFechaDeCualquierTurno", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FechaInicio", fechainicio.AddMinutes(1));
            cmd.Parameters.AddWithValue("@FechaFin", fechafin.AddMinutes(-1));
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoClases.Add(
                    new Clases
                    {
                        Descripcion = Convert.ToString(dr["Descripcion"]),
                        idClase = Convert.ToInt32(dr["idClase"]),
                        idCurso = Convert.ToInt32(dr["idCurso"]),
                        idTurno = Convert.ToInt32(dr["idTurno"]),
                        fecha = Convert.ToString(dr["fecha"]),
                        FechaInicio = Convert.ToDateTime(dr["HoraInicio"]),
                        FechaFin = Convert.ToDateTime(dr["HoraFin"])
                    });
            }
            return ListadoClases;
        }

        public List<Clases> ListarClasesConHoraInicioYHoraFin(DateTime fechainicio, DateTime fechafin)
        {
            connection();
            List<Clases> ListadoClases = new List<Clases>();

            SqlCommand cmd = new SqlCommand("ListarClasesConHoraInicioYHoraFin", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FechaInicio", fechainicio);
            cmd.Parameters.AddWithValue("@FechaFin", fechafin);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoClases.Add(
                    new Clases
                    {
                        Descripcion = Convert.ToString(dr["Descripcion"]),
                        idClase = Convert.ToInt32(dr["idClase"]),
                        idCurso = Convert.ToInt32(dr["idCurso"]),
                        idTurno = Convert.ToInt32(dr["idTurno"]),
                        fecha = Convert.ToString(dr["fecha"]),
                        FechaInicio = Convert.ToDateTime(dr["HoraInicio"]),
                        FechaFin = Convert.ToDateTime(dr["HoraFin"])
                    });
            }
            return ListadoClases;
        }

        public List<Clases> ListarClasesDeUnTurno(int idTurno)
        {
            connection();
            List<Clases> ListadoClases = new List<Clases>();

            SqlCommand cmd = new SqlCommand("ListarClasesT", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idTurno", idTurno);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoClases.Add(
                    new Clases
                    {
                        Descripcion = Convert.ToString(dr["Descripcion"]),
                        idClase = Convert.ToInt32(dr["idClase"]),
                        idCurso = Convert.ToInt32(dr["idCurso"]),
                        idTurno = Convert.ToInt32(dr["idTurno"]),
                        fecha = Convert.ToString(dr["fecha"]),
                        FechaInicio = Convert.ToDateTime(dr["HoraInicio"]),
                        FechaFin = Convert.ToDateTime(dr["HoraFin"])
                    });
            }
            return ListadoClases;
        }

        public bool EditarClase(Clases c)
        {
            connection();
            SqlCommand cmd = new SqlCommand("EditarClase", _con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@id", c.idClase);
            cmd.Parameters.AddWithValue("@Descripcion", c.Descripcion);
            cmd.Parameters.AddWithValue("@HoraInicio", c.FechaInicio);
            cmd.Parameters.AddWithValue("@HoraFin", c.FechaFin);
            
            _con.Open();
            int i = cmd.ExecuteNonQuery();
            _con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }
        public bool BorrarClase(int idclase)
        {
            connection();
            SqlCommand cmd = new SqlCommand("BorrarClase", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idclase", idclase);
            _con.Open();
            int i = cmd.ExecuteNonQuery();
            _con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public int RecuperarIdClase()
        {
            int idClase = 0;
            connection();

            SqlCommand cmd = new SqlCommand("RecuperarIdClase", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                idClase = Convert.ToInt32(dr["idClase"]);
            }

            return idClase;
        }

        public bool AsignarRecetaClase(int idReceta, int idCurso, int idTurno, int idClase)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AsignarRecetaClase", _con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@idReceta", idReceta);
            cmd.Parameters.AddWithValue("@idCurso", idCurso);
            cmd.Parameters.AddWithValue("@idTurno", idTurno);
            cmd.Parameters.AddWithValue("@idClase", idClase);

            _con.Open();
            int i = cmd.ExecuteNonQuery();
            _con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public bool QuitarRecetaClase(int idReceta, int idClase)
        {
            connection();
            SqlCommand cmd = new SqlCommand("QuitarRecetaClase", _con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@idReceta", idReceta);
            cmd.Parameters.AddWithValue("@idClase", idClase);

            _con.Open();
            int i = cmd.ExecuteNonQuery();
            _con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public List<Recetas> ListarRecetasDeClase(int idClase)
        {
            connection();
            List<Recetas> ListadoRecetas = new List<Recetas>();

            SqlCommand cmd = new SqlCommand("ListarRecetasDeClase", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idClase", idClase);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoRecetas.Add(
                    new Recetas
                    {
                        Preparacion = Convert.ToString(dr["Preparacion"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        idReceta = Convert.ToInt32(dr["idReceta"]),
                        Tips = Convert.ToString(dr["Tips"]),
                        Ingredientes = Convert.ToString(dr["Ingredientes"])
                    });
            }
            return ListadoRecetas;
        }

        public List<RecetasDeUnTurno> ListarRecetasDeTurno(int idTurno)
        {
            connection();
            List<RecetasDeUnTurno> ListadoRecetas = new List<RecetasDeUnTurno>();

            SqlCommand cmd = new SqlCommand("ListarRecetasDeTurno", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idTurno", idTurno);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoRecetas.Add(
                    new RecetasDeUnTurno
                    {
                        Preparacion = Convert.ToString(dr["Preparacion"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        idReceta = Convert.ToInt32(dr["idReceta"]),
                        idClase = Convert.ToInt32(dr["idClase"])
                    });
            }
            return ListadoRecetas;
        }

        public bool AsistenciaDeAlumnoEnClase(int idcurso, int idturno, int idclase, int idalumno)
        {
            String estado = "presente";
            connection();
            List<RecetasDeUnTurno> ListadoRecetas = new List<RecetasDeUnTurno>();

            SqlCommand cmd = new SqlCommand("AsistenciaDeAlumnoEnClase", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idcurso", idcurso);
            cmd.Parameters.AddWithValue("@idturno", idturno);
            cmd.Parameters.AddWithValue("@idclase", idclase);
            cmd.Parameters.AddWithValue("@idalumno", idalumno);
            cmd.Parameters.AddWithValue("@estado", estado);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);

            _con.Open();
            int i = cmd.ExecuteNonQuery();
            _con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public List<AsistenciaAlumno> ListaDeAsistencia(int idturno)
        {
            connection();
            List<AsistenciaAlumno> ListadoAsistencias = new List<AsistenciaAlumno>();

            SqlCommand cmd = new SqlCommand("ListaDeAsistencia", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idTurno", idturno);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoAsistencias.Add(
                    new AsistenciaAlumno
                    {
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Apellido= Convert.ToString(dr["Apellido"]),
                        Estado = Convert.ToString(dr["Asistencia"])
                    });
            }
            return ListadoAsistencias;
        }

        public List<EventosFullCalendar> ListarEventosFullCalendar()
        {
            connection();
            List<EventosFullCalendar> ListadoClases = new List<EventosFullCalendar>();

            SqlCommand cmd = new SqlCommand("ListarClasesForCalendar", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoClases.Add(
                    new EventosFullCalendar
                    {
                        title = Convert.ToString(dr["Nombre"]) + "   " + Convert.ToInt32(dr["alumnos"]) + 'A',
                        start = Convert.ToDateTime(dr["HoraInicio"]),
                        end = Convert.ToDateTime(dr["HoraFin"]),
                        Alumnos = Convert.ToInt32(dr["alumnos"]),
                        url = "Turnos/" + "ClasesDeTurnoCalendario/" + Convert.ToInt32(dr["idTurno"]),
                     //   backgroundColor = "red"
                    });
            }
            return ListadoClases;
        }

        public List<Alumno> AsistenciaTomada(int idclase, int idturno)
        {
            connection();
            List<Alumno> ListaAlumnos = new List<Alumno>();
            SqlCommand cmd = new SqlCommand("AsistenciaTomada", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idTurno", idturno);
            cmd.Parameters.AddWithValue("@idClase", idclase);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListaAlumnos.Add(
                    new Alumno
                    {
                        idAlumno = Convert.ToInt32(dr["idAlumno"]),
                        Apellido = Convert.ToString(dr["Apellido"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Mail= Convert.ToString(dr["Asistencia"])
                    });
            }
            return ListaAlumnos;
        }


        //Este action trae la descripcion de cada clase del ultimo turno de un curso dado.
        public List<Clases> TraerDescripcionDeClasesDeUnCurso(int idCurso)
        {
            connection();
            List<Clases> ListadoClases = new List<Clases>();

            SqlCommand cmd = new SqlCommand("TraerDescripcionDeClasesDeUnCurso", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idCurso", idCurso);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoClases.Add(
                    new Clases
                    {
                        Descripcion = Convert.ToString(dr["Descripcion"]),
                        idClase = Convert.ToInt32(dr["idClase"]),
                        idTurno = Convert.ToInt32(dr["idTurno"]),
                    });
            }
            return ListadoClases;
        }

    }
}