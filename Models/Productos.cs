﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Proyecto_1.Models
{
    public class Productos
    {
        public int idProducto { set; get; }
        public String Nombre { set; get; }
        public String Unidad { set; get; }

       
        public String Tipo {set; get;}

        public float Precio { set; get; }

        public Decimal Stock { get; set; }
        public Productos()
        { }

        public Productos(int idP, String Nomb, String Uni, String Tip, float pre, Decimal St)
        {
            idProducto = idP;
            Nombre = Nomb;
            Unidad = Uni;
            Tipo = Tip;
            Precio = pre;
            Stock = St;
        }

        public Productos(String Nomb, String Uni, String Tip, float pre, Decimal St)
          {
            Nombre = Nomb;
            Unidad = Uni;
            Tipo = Tip;
            Precio = pre;
            Stock = St;
          }
    }
}