﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Proyecto_1.Models
{
    public class LineaVentaHandle
    {
        private SqlConnection con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["EscuelaCon"].ToString();
            con = new SqlConnection(constring);
        }


        public bool AgregarLinea(LineaVentas lv)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarLineaVenta", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@idVenta", lv.idVenta);
            cmd.Parameters.AddWithValue("@idProducto", lv.idProducto);
            cmd.Parameters.AddWithValue("@Cantidad", lv.Cantidad);
            cmd.Parameters.AddWithValue("@Precio", lv.Precio);

   
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public List<LineaVentas> LineasDeClase(int idclase)
        {
            connection();
            List<LineaVentas> ListadoLineasDeClase = new List<LineaVentas>();

            SqlCommand cmd = new SqlCommand("LineasDeClase", con);
            cmd.Parameters.AddWithValue("@idClase", idclase);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoLineasDeClase.Add(
                    new LineaVentas
                    {
                        idProducto = Convert.ToInt32(dr["idProducto"]),
                        Cantidad = Convert.ToDecimal(dr["cantidad"]),
                        Nombre = Convert.ToString(dr["unidad"])
                    });
            }
            return ListadoLineasDeClase;

        }

    }
}