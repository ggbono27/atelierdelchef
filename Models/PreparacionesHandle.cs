﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Proyecto_1.Models
{
    public class PreparacionesHandle
    {
        private SqlConnection _con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["EscuelaCon"].ToString();
            _con = new SqlConnection(constring);
        }
        public bool AgregarPreparacion(Preparaciones prep)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarPreparacion", _con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@idReceta", prep.idReceta);
            cmd.Parameters.AddWithValue("@Cantidad", prep.Cantidad);
            cmd.Parameters.AddWithValue("@idProducto", prep.idProducto);
            cmd.Parameters.AddWithValue("@Unidad", prep.Unidad);

            _con.Open();
            int i = cmd.ExecuteNonQuery();
            _con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public bool QuitarIngrediente(int idProducto, int idReceta)
        {
            connection();
            SqlCommand cmd = new SqlCommand("QuitarIngrediente", _con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@idProducto", idProducto);
            cmd.Parameters.AddWithValue("@idReceta", idReceta);

            _con.Open();
            int i = cmd.ExecuteNonQuery();
            _con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

    }
}