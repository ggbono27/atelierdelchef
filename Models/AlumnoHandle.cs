﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Proyecto_1.Models
{
    public class AlumnoHandle
    {
        private SqlConnection con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["EscuelaCon"].ToString();
            con = new SqlConnection(constring);
        }

        public bool AgregarAlumno(Alumno alumno)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarAlumno", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Apellido", alumno.Apellido);
            cmd.Parameters.AddWithValue("@Nombre", alumno.Nombre);
            cmd.Parameters.AddWithValue("@Direccion", alumno.Direccion);
            cmd.Parameters.AddWithValue("@Dni", alumno.Dni);
            cmd.Parameters.AddWithValue("@FechaNac",alumno.FechaNac);
            cmd.Parameters.AddWithValue("@Mail", alumno.Mail);
            cmd.Parameters.AddWithValue("@Telefono", alumno.Telefono);
            cmd.Parameters.AddWithValue("@Telefono2", alumno.Telefono2);
            cmd.Parameters.AddWithValue("@Usuario", alumno.Usuario);
            if (alumno.Sexo == "Varon")
            {
                cmd.Parameters.AddWithValue("@Sexo", 0);
            }
            else
            {
                cmd.Parameters.AddWithValue("@Sexo", 1);
            }
           

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public int AgregarAlumnoYRecuperarId(Alumno alumno)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarAlumnoYRecuperarId", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@lastId", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Parameters.AddWithValue("@Apellido", alumno.Apellido);
            cmd.Parameters.AddWithValue("@Nombre", alumno.Nombre);
            cmd.Parameters.AddWithValue("@Direccion", alumno.Direccion);
            cmd.Parameters.AddWithValue("@Dni", alumno.Dni);
            cmd.Parameters.AddWithValue("@FechaNac", alumno.FechaNac);
            cmd.Parameters.AddWithValue("@Mail", alumno.Mail);
            cmd.Parameters.AddWithValue("@Telefono", alumno.Telefono);
            cmd.Parameters.AddWithValue("@Telefono2", alumno.Telefono2);
            cmd.Parameters.AddWithValue("@Usuario", alumno.Usuario);
            if (alumno.Sexo == "Varon")
            {
                cmd.Parameters.AddWithValue("@Sexo", 0);
            }
            else
            {
                cmd.Parameters.AddWithValue("@Sexo", 1);
            }

            con.Open();
            int i = cmd.ExecuteNonQuery();
            int idAlumno = Convert.ToInt32(cmd.Parameters["@lastId"].Value);
            con.Close();
            return idAlumno;
        }

        public int TraerUltimoIdAlumno()
        {
            connection();
            SqlCommand cmd = new SqlCommand("TraerUltimoIdAlumno", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@lastId", SqlDbType.Int).Direction = ParameterDirection.Output;
            con.Open();

            int i = cmd.ExecuteNonQuery();
            int idAlumno = Convert.ToInt32(cmd.Parameters["@lastId"].Value);
            con.Close();
            return idAlumno;
        }

        public List<Alumno> ListarAlumnos()
        {
            connection();
            List<Alumno> ListadoAlumnos = new List<Alumno>();

            SqlCommand cmd = new SqlCommand("ListaAlumnos", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoAlumnos.Add(
                    new Alumno
                    {
                        idAlumno = Convert.ToInt32(dr["idAlumno"]),
                        Apellido = Convert.ToString(dr["Apellido"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Direccion = Convert.ToString(dr["Direccion"]),
                        Telefono = Convert.ToString(dr["Telefono"]),
                        Mail = Convert.ToString(dr["Mail"]),
                        Dni = Convert.ToInt32(dr["DNI"]),
                        fecha = Convert.ToString(dr["fecha"]),
                        FechaNac= Convert.ToDateTime(dr["FechaNac"]),
                        Telefono2 = Convert.ToString(dr["Telefono2"]),
                        Usuario = Convert.ToString(dr["Usuario"]),
                        Sexo= Convert.ToString(dr["Sexo"])
                    });
            }
            return ListadoAlumnos;
        }

        public List<Alumno> FiltroCurso(DateTime fecha1, DateTime fecha2)
        {
            connection();
            List<Alumno> ListadoAlumnos = new List<Alumno>();

            SqlCommand cmd = new SqlCommand("ListarAlumnosConCursos", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fecha1",fecha1);
            cmd.Parameters.AddWithValue("@fecha2",fecha2);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoAlumnos.Add(
                    new Alumno
                    {
                        idAlumno = Convert.ToInt32(dr["idAlumno"]),
                        Apellido = Convert.ToString(dr["Apellido"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Telefono = Convert.ToString(dr["Telefono"]),
                        Mail = Convert.ToString(dr["Mail"]),
                        Direccion = Convert.ToString(dr["Curso"]),
                        FechaNac = Convert.ToDateTime(dr["fecha"]),
                    });
            }
            return ListadoAlumnos;
        }

        public List<Alumno> FiltroCurso2()
        {
            connection();
            List<Alumno> ListadoAlumnos = new List<Alumno>();

            SqlCommand cmd = new SqlCommand("ListarAlumnosConCursos2", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoAlumnos.Add(
                    new Alumno
                    {
                        idAlumno = Convert.ToInt32(dr["idAlumno"]),
                        Apellido = Convert.ToString(dr["Apellido"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Telefono = Convert.ToString(dr["Telefono"]),
                        Mail = Convert.ToString(dr["Mail"]),
                        Direccion = Convert.ToString(dr["Curso"]),
                        FechaNac = Convert.ToDateTime(dr["fecha"]),
                    });
            }
            return ListadoAlumnos;
        }

        public bool EditarAlumno (Alumno alumno)
        {
            connection();
            SqlCommand cmd = new SqlCommand("EditarAlumno", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@id", alumno.idAlumno);
            cmd.Parameters.AddWithValue("@Apellido", alumno.Apellido);
            cmd.Parameters.AddWithValue("@Nombre", alumno.Nombre);
            cmd.Parameters.AddWithValue("@Direccion", alumno.Direccion);
            cmd.Parameters.AddWithValue("@Dni", alumno.Dni);
            cmd.Parameters.AddWithValue("@FechaNac", alumno.FechaNac);
            cmd.Parameters.AddWithValue("@Mail", alumno.Mail);
            cmd.Parameters.AddWithValue("@Telefono", alumno.Telefono);
            cmd.Parameters.AddWithValue("@Telefono2", alumno.Telefono2);
            cmd.Parameters.AddWithValue("@Usuario", alumno.Usuario);
            if (alumno.Sexo == "Varon")
            {
                cmd.Parameters.AddWithValue("@Sexo", 0);
            }
            else
            {
                cmd.Parameters.AddWithValue("@Sexo", 1);
            }

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public IList<Alumno> BuscarAlumno(string cadena)
        {
            connection();
            List<Alumno> ListadoAlumnos = new List<Alumno>();

            SqlCommand cmd = new SqlCommand("BuscarAlumno", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@cadena", cadena);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoAlumnos.Add(
                    new Alumno
                    {
                        idAlumno = Convert.ToInt32(dr["idAlumno"]),
                        Apellido = Convert.ToString(dr["Apellido"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Direccion = Convert.ToString(dr["Direccion"]),
                        Telefono = Convert.ToString(dr["Telefono"]),
                        Telefono2 = Convert.ToString(dr["Telefono2"]),
                        Mail = Convert.ToString(dr["Mail"]),
                        Dni = Convert.ToInt32(dr["DNI"]),
                        fecha = Convert.ToString(dr["fecha"]),
                        FechaNac = Convert.ToDateTime(dr["fecha"]),
                        Usuario = Convert.ToString(dr["Usuario"]),
                        Sexo= Convert.ToString(dr["Sexo"])
                    });
            }
            return ListadoAlumnos;
        }

        public bool BorrarAlumno(int id)
        {
            connection();
            SqlCommand cmd = new SqlCommand("BorrarAlumno", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@id",id);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public bool EsRepetido(int dni)
        {
            connection();
            List<Alumno> ListadoAlumnos = new List<Alumno>();

            SqlCommand cmd = new SqlCommand("EsAlumnoRepetido", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            cmd.Parameters.AddWithValue("@Dni", dni);
            con.Open();
            sd.Fill(dt);
            con.Close();

            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public List<Alumno> AlumnosQueCursaron(int idcurso1)
        {
            connection();
            List<Alumno> ListadoAlumnos = new List<Alumno>();

            SqlCommand cmd = new SqlCommand("AlumnosQueCursaron", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idcurso", idcurso1);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoAlumnos.Add(
                    new Alumno
                    {
                        idAlumno = Convert.ToInt32(dr["idAlumno"]),
                        Apellido = Convert.ToString(dr["Apellido"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Telefono = Convert.ToString(dr["Telefono"]),
                        FechaNac = Convert.ToDateTime(dr["Fecha"]),
                        Sexo = Convert.ToString(dr["Sexo"]),
                        Mail = Convert.ToString(dr["Mail"])
                    });
            }
            return ListadoAlumnos;

        }

        public List<Alumno> AlumnosQueNoCursaron(int idcurso1)
        {
            connection();
            List<Alumno> ListadoAlumnos = new List<Alumno>();

            SqlCommand cmd = new SqlCommand("AlumnosQueNoCursaron", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idcurso", idcurso1);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoAlumnos.Add(
                    new Alumno
                    {
                        idAlumno = Convert.ToInt32(dr["idAlumno"]),
                        Apellido = Convert.ToString(dr["Apellido"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Telefono = Convert.ToString(dr["Telefono"]),
                        FechaNac = Convert.ToDateTime(dr["Fecha"]),
                        Sexo = Convert.ToString(dr["Sexo"]),
                        Mail = Convert.ToString(dr["Mail"])
                    });
            }
            return ListadoAlumnos;

        }

        public List<Alumno> AlumnosConFiltro2(int idcurso1, int idcurso2)
        {
            connection();
            List<Alumno> ListadoAlumnos = new List<Alumno>();

            SqlCommand cmd = new SqlCommand("AlumnosConFiltro2", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idcurso", idcurso1);
            cmd.Parameters.AddWithValue("@idcurso2", idcurso2);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoAlumnos.Add(
                    new Alumno
                    {
                        idAlumno = Convert.ToInt32(dr["idAlumno"]),
                        Apellido = Convert.ToString(dr["Apellido"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Telefono = Convert.ToString(dr["Telefono"]),
                        FechaNac = Convert.ToDateTime(dr["Fecha"]),
                        Sexo= Convert.ToString(dr["Sexo"]),
                        Mail=Convert.ToString(dr["Mail"])
                    });
            }
            return ListadoAlumnos;

        }
    }
}