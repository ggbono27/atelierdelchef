﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Proyecto_1.Models
{
    public class Pagos
    {
        public String Concepto { get; set; }
        public int Monto { get; set; }

        public string Control { get; set; }

        public DateTime Fecha { get; set; }
        public int idPago { get; set; }
        public int idCurso { get; set; }
        public int idAlumno { get; set; }
        public int idTurno { get; set; }

        [DisplayName("Nombre del usuario que inscribe:")]
        [RegularExpression("^[a-zA-Z\\s\\u00E0-\u00FC]+$", ErrorMessage = "El usuario debe contener solo letras")]   //solo acepta letras mayusculas o minusculas con o sin acentos y tambien espacios
        public string Usuario { get; set; }


        public Pagos()
        { }

        public Pagos(int monto, String concepto, int idA, int idC, int idT, string user, string control)
        {
            Monto = monto;
            Concepto = concepto;
            Fecha = DateTime.UtcNow;
            idAlumno = idA;
            idCurso = idC;
            idTurno = idT;
            Usuario = user;
            Control = control;
        }

        public Pagos(int monto, String concepto, int idP, int idA, int idC, int idT, string control)
        {
            idPago = idP;
            Monto = monto;
            Concepto = concepto;
            Fecha = DateTime.UtcNow;
            idAlumno = idA;
            idCurso = idC;
            idTurno = idT;
            Control = control;
        }

        public Pagos(int monto, String concepto, DateTime fecha)
        {
            Monto = monto;
            Concepto = concepto;
            Fecha = fecha;
        }
    }

}