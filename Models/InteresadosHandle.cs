﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Proyecto_1.Models
{
    public class InteresadosHandle
    {
        private SqlConnection _con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["EscuelaCon"].ToString();
            _con = new SqlConnection(constring);
        }

        public void AgregarInteresado(int idcurso, string nombre, string telefono,string usuario, string observacion)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarInteresado", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Nombre", nombre);
            cmd.Parameters.AddWithValue("@Telefono",telefono);
            cmd.Parameters.AddWithValue("@Usuario", usuario);
            cmd.Parameters.AddWithValue("@Observacion", observacion);
            cmd.Parameters.AddWithValue("@Fecha", DateTime.UtcNow);
            cmd.Parameters.Add("@lastId", SqlDbType.Int).Direction = ParameterDirection.Output;

            _con.Open();
            int i = cmd.ExecuteNonQuery();
            int idinteresado = Convert.ToInt32(cmd.Parameters["@lastId"].Value);
             _con.Close();

            if (i < 1)
            {
                _con.Open();
                cmd= new SqlCommand("AgregarInteres", _con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idCurso", idcurso);
                cmd.Parameters.AddWithValue("@idInteresado", idinteresado);
                cmd.ExecuteNonQuery();
                _con.Close();
            }
        }

        public bool EsInteresadoRepetido(String telefono)
        {
            connection();
            List<Interesados> ListadoAlumnos = new List<Interesados>();

            SqlCommand cmd = new SqlCommand("EsInteresadoRepetido", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            cmd.Parameters.AddWithValue("@Telefono", telefono);
            _con.Open();
            sd.Fill(dt);
            _con.Close();

            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public List<Interesados> InteresadosDeCurso(int idcurso)
        {
            connection();
            List<Interesados> ListadoInteresados = new List<Interesados>();
            SqlCommand cmd = new SqlCommand("InteresadosDeCurso", _con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@idCurso", idcurso);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoInteresados.Add(
                    new Interesados
                    {
                        idInteresado = Convert.ToInt32(dr["idInteresado"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Telefono = Convert.ToString(dr["Telefono"]),
                        Fecha = Convert.ToDateTime(dr["Fecha"]),
                        Curso = Convert.ToString(dr["Curso"]),
                        Usuario= Convert.ToString(dr["Usuario"])
                    });
            }
            return ListadoInteresados;
        }

        public List<Interesados> ListarInteresados()
        {
            connection();
            List<Interesados> ListadoInteresados = new List<Interesados>();
            SqlCommand cmd = new SqlCommand("ListarInteresados", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoInteresados.Add(
                    new Interesados
                    {
                        idInteresado = Convert.ToInt32(dr["idInteresado"]),
                        idCurso= Convert.ToInt32(dr["idCurso"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Telefono = Convert.ToString(dr["Telefono"]),
                        Fecha= Convert.ToDateTime(dr["Fecha"]),
                        Curso= Convert.ToString(dr["Curso"]),
                        Usuario= Convert.ToString(dr["Usuario"]),
                        Observacion= Convert.ToString(dr["Observacion"])
                    });
            }
            return ListadoInteresados;
        }

        public bool EditInteresado(Interesados interesado, int idcurso)
        {
            connection();
            SqlCommand cmd = new SqlCommand("EditInteresado", _con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@idInteresado", interesado.idInteresado);
            cmd.Parameters.AddWithValue("@Nombre", interesado.Nombre);
            cmd.Parameters.AddWithValue("@Telefono", interesado.Telefono);
            cmd.Parameters.AddWithValue("@Usuario", interesado.Usuario);
            cmd.Parameters.AddWithValue("@Fecha", DateTime.UtcNow);
            cmd.Parameters.AddWithValue("@idCursoNuevo", idcurso);
            cmd.Parameters.AddWithValue("@idCursoViejo", interesado.idCurso);
            cmd.Parameters.AddWithValue("@Observacion", interesado.Observacion);

            _con.Open();
            int i = cmd.ExecuteNonQuery();
            _con.Close();

            
            if (i >= 1)
                return true;
            else
                return false;
        }

        public bool BorrarInteresadoDeCurso(int idinteresado, int idcurso)
        {
            connection();
            SqlCommand cmd = new SqlCommand("BorrarInteresadoDeCurso", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idInteresado", idinteresado);
            cmd.Parameters.AddWithValue("@idCurso", idcurso);

            _con.Open();
            int i = cmd.ExecuteNonQuery();
            _con.Close();

            if (i >= 1)
                return true;
            else
                return false;

        }

    }
}