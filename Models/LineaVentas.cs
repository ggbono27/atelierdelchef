﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_1.Models
{
    public class LineaVentas
    {
        public int idVenta { get; set; }
        public int idProducto { get; set; }
        public Decimal Precio { get; set; }
        public Decimal Cantidad { get; set; }

        public String Unidad { get; set; }

        public String Nombre { get; set; }
        public Decimal SubTotal { get; set; }
        
        public LineaVentas()
        { }

        public LineaVentas(int idV, int idP, Decimal Price, Decimal Cant, String nom, Decimal subtotal)
        {
            idVenta = idV;
            idProducto = idP;
            Precio = Price;
            Cantidad = Cant;
            Nombre = nom;
            SubTotal = subtotal;
        }

        public LineaVentas(int idP, Decimal Price, Decimal Cant, String nom, Decimal subtotal)
        {
            idProducto = idP;
            Precio = Price;
            Cantidad = Cant;
            Nombre = nom;
            SubTotal = subtotal;
        }

        public LineaVentas(int idproducto, int idventa, Decimal cantidad)
        {
            idProducto = idproducto;
            idVenta = idventa;
            Cantidad = cantidad;
        }

        public LineaVentas(int idproducto, int idventa, Decimal cantidad, Decimal precio)
        {
            idProducto = idproducto;
            idVenta = idventa;
            Cantidad = cantidad;
            Precio = precio;
        }

        public LineaVentas(int idventa, int idproducto,  Decimal cantidad, string nombre, string unidad)
        {
            idProducto = idproducto;
            Cantidad = cantidad;
            Nombre = nombre;
            Unidad = unidad;
            idVenta = idventa;
        }
    }
}