﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Proyecto_1.Models
{
    public class Interesados
    {
        public int idInteresado { get; set; }
        public int idCurso { get; set; }

        [DisplayName("Nombre (*):")]
        [Required]
        public String Nombre { get; set; }

        [DisplayName("Teléfono (*):")]
        [Phone]
        [Required]
        public String Telefono { get; set; }
        [DisplayName("Curso (*):")]
        public String Curso { get; set; }
        public DateTime Fecha { get; set; }
        [DisplayName("¿Quién lo anotó? (*):")]
        public String Usuario { get; set; }

        public String Observacion { get; set; }


        public Interesados()
        { }

        public Interesados(int idinteresado, string nombre, string telefono, string curso, DateTime fecha, string usuario, int idcurso, string observacion)
        {
            idInteresado = idinteresado;
            idCurso = idcurso;
            Nombre = nombre;
            Telefono = telefono;
            Curso = curso;
            Fecha = fecha;
            Usuario = usuario;
            Observacion = observacion;
        }

        public Interesados(string nombre, string telefono, DateTime fecha, string usuario, string observacion)
        {
            Nombre = nombre;
            Telefono = telefono;
            Fecha = fecha;
            Usuario = usuario;
            Observacion = observacion;
        }
    }
}