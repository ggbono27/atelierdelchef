﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Data;

namespace Proyecto_1.Models
{
    public class UsuariosHandle
    {
        private SqlConnection con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["EscuelaCon"].ToString();
            con = new SqlConnection(constring);
        }

        public void createUser(string user, string password, string salt)
        {
            connection();
            SqlCommand cmd = new SqlCommand("crearUsuario", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@user", user);
            cmd.Parameters.AddWithValue("@pass", password);
            cmd.Parameters.AddWithValue("@salt", salt);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public List<Usuarios> getUser(string user)
        {

            connection();
            List<Usuarios> usuario = new List<Usuarios>();

            SqlCommand cmd = new SqlCommand("getUser", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@user", user);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                usuario.Add(
                    new Usuarios
                    {
                        idUsuario = Convert.ToInt32(dr["idUsuario"]),
                        UserName= Convert.ToString(dr["Username"]),
                        Salt= Convert.ToString(dr["Salt"]),
                        Pass= Convert.ToString(dr["Pass"])
                        
                    });
            }
            return usuario;


        }

    }
    
}