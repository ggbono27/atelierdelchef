﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Proyecto_1.Models;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Proyecto_1.ViewModels;

namespace Proyecto_1.Models
{
    public class VentasHandle
    {
        private SqlConnection con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["EscuelaCon"].ToString();
            con = new SqlConnection(constring);
        }

        public bool AgregarVenta(List<LineaVentas> lv, String usuario)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarVenta", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@FechaVenta", DateTime.UtcNow.AddHours(-3));
            cmd.Parameters.AddWithValue("@FechaVenta", DateTime.UtcNow);
            cmd.Parameters.AddWithValue("@Concepto", "Venta bazar");
            cmd.Parameters.AddWithValue("@Usuario", usuario);
            cmd.Parameters.AddWithValue("@monto", 0);
            cmd.Parameters.AddWithValue("@descripcion", "-");
            cmd.Parameters.Add("@lastId", SqlDbType.Int).Direction = ParameterDirection.Output;

            con.Open();

            int i = cmd.ExecuteNonQuery();
            int idventa = Convert.ToInt32(cmd.Parameters["@lastId"].Value);
            con.Close();

            if (i >= 1)
            {
                foreach (var item in lv)
                {
                    LineaVentaHandle lvh = new LineaVentaHandle();
                    item.idVenta = idventa;
                    lvh.AgregarLinea(item);
                }
                return true;
            }
            else
                return false;
        }

        public void BorrarLinea(int idventa, int idproducto)
        {
            connection();
            SqlCommand cmd = new SqlCommand("BorrarLineaVenta", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idVenta", idventa);
            cmd.Parameters.AddWithValue("@idProducto", idproducto);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public void BorrarVenta(int idventa)
        {
            connection();
            SqlCommand cmd = new SqlCommand("BorrarVentaBazar", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idVenta", idventa);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public bool NuevoIngresoLibre(string usuario, string descripcion, int monto, string fecha)
        {
            Ventas v = new Ventas();
            connection();
            SqlCommand cmd = new SqlCommand("AgregarVenta", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FechaVenta", Convert.ToDateTime(fecha));
            cmd.Parameters.AddWithValue("@Concepto", "Ingreso libre");
            cmd.Parameters.AddWithValue("@Usuario", usuario);
            cmd.Parameters.AddWithValue("@descripcion", descripcion);
            cmd.Parameters.AddWithValue("@monto", monto);
            cmd.Parameters.Add("@lastId", SqlDbType.Int).Direction = ParameterDirection.Output;

            con.Open();

            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
            {
                return true;
            }
            else
                return false;
        }

        public bool AgregarCorreccion(LineaVentas lv)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarVenta", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FechaVenta", DateTime.UtcNow.AddHours(-3));
            cmd.Parameters.AddWithValue("@Concepto", "Correccion");
            cmd.Parameters.AddWithValue("@monto", 0);
            cmd.Parameters.AddWithValue("@descripcion", "-");
            cmd.Parameters.Add("@lastId", SqlDbType.Int).Direction = ParameterDirection.Output;

            con.Open();

            int i = cmd.ExecuteNonQuery();
            int idventa = Convert.ToInt32(cmd.Parameters["@lastId"].Value);
            con.Close();

            if (i >= 1)
            {
                LineaVentaHandle lvh = new LineaVentaHandle();
                lv.idVenta = idventa;
                lvh.AgregarLinea(lv);

                return true;
            }
            else
                return false;
        }

        public List<Caja> ListarVentasCaja()
        {
            connection();
            List<Caja> ListadoCaja = new List<Caja>();

            SqlCommand cmd = new SqlCommand("ListarVentasCaja", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoCaja.Add(
                    new Caja
                    {
                        Monto = Convert.ToInt32(dr["Monto"]),
                        Fecha = Convert.ToDateTime(dr["Fecha"]),
                        Concepto = Convert.ToString(dr["Concepto"]),
                        Usuario = Convert.ToString(dr["Usuario"]),
                        Concepto2 = Convert.ToString(dr["Concepto"])
                    });
            }
            return ListadoCaja;
        }

        public List<Caja> ListarIngresosLibreCaja()
        {
            connection();
            List<Caja> ListadoCaja = new List<Caja>();

            SqlCommand cmd = new SqlCommand("ListarIngresosLibreCaja", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoCaja.Add(
                    new Caja
                    {
                        Monto = Convert.ToInt32(dr["Monto"]),
                        Fecha = Convert.ToDateTime(dr["Fecha"]),
                        Concepto = Convert.ToString(dr["Concepto"])+ "- "+ Convert.ToString(dr["Descripcion"]),
                        Usuario = Convert.ToString(dr["Usuario"]),
                        Concepto2 = Convert.ToString(dr["Concepto"])
                    });
            }
            return ListadoCaja;
        }

        public List<Caja> ListarIngresosLibrePorFecha(DateTime fecha1, DateTime fecha2)
        {
            connection();
            List<Caja> ListadoCaja = new List<Caja>();

            SqlCommand cmd = new SqlCommand("ListarIngresosLibrePorFecha", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fecha", fecha1);
            cmd.Parameters.AddWithValue("@fecha2", fecha2);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoCaja.Add(
                    new Caja
                    {
                        Monto = Convert.ToInt32(dr["Monto"]),
                        Fecha = Convert.ToDateTime(dr["Fecha"]),
                        Concepto = Convert.ToString(dr["Concepto"]) + "- " + Convert.ToString(dr["Descripcion"]),
                        Usuario = Convert.ToString(dr["Usuario"]),
                        Concepto2 = Convert.ToString(dr["Concepto"])
                    });
            }
            return ListadoCaja;
        }

        public List<Caja> ListarVentasPorRangoFecha(DateTime fecha1, DateTime fecha2)
        {
            connection();
            List<Caja> ListadoCaja = new List<Caja>();

            SqlCommand cmd = new SqlCommand("ListarVentasCajaPorFecha", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fecha", fecha1);
            cmd.Parameters.AddWithValue("@fecha2", fecha2);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoCaja.Add(
                    new Caja
                    {
                        Monto = Convert.ToInt32(dr["Monto"]),
                        Fecha = Convert.ToDateTime(dr["Fecha"]),
                        Concepto = Convert.ToString(dr["Concepto"]),
                        Usuario = Convert.ToString(dr["Usuario"]),
                        Concepto2 = Convert.ToString(dr["Concepto"])
                    });
            }
            return ListadoCaja;
        }

        public List<Ventas> ListarVentas()
        {
            connection();
            List<Ventas> ListadoVentas = new List<Ventas>();

            SqlCommand cmd = new SqlCommand("ListaVentas", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoVentas.Add(
                    new Ventas
                    {
                        idVenta = Convert.ToInt32(dr["idVenta"]),
                        FechaVenta = Convert.ToDateTime(dr["FechaVenta"]),
                        Total = Convert.ToInt32(dr["total"]),
                        Usuario = Convert.ToString(dr["Usuario"]),

                    });
            }
            return ListadoVentas;
        }

        public List<LineaVentas> DetalleVenta(int idventa)
        {
            connection();
            List<LineaVentas> ListadoLineas = new List<LineaVentas>();

            SqlCommand cmd = new SqlCommand("DetalleVenta", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idventa", idventa);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoLineas.Add(
                    new LineaVentas
                    {
                        idVenta=Convert.ToInt32(dr["idVenta"]),
                        idProducto = Convert.ToInt32(dr["idProducto"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Cantidad = Convert.ToInt32(dr["Cantidad"]),
                        Precio = Convert.ToInt32(dr["Precio"]),
                        SubTotal = Convert.ToInt32(dr["subtotal"])

                    });

            }
            return ListadoLineas;
        }


        public List<Ventas> BuscarVenta(DateTime fecha1, DateTime fecha2)
        {
            connection();
            List<Ventas> ListadoVentas = new List<Ventas>();
            SqlCommand cmd = new SqlCommand("BuscarVenta", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fecha", fecha1);
            cmd.Parameters.AddWithValue("@fecha2", fecha2);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoVentas.Add(
                    new Ventas
                    {
                        idVenta = Convert.ToInt32(dr["idVenta"]),
                        FechaVenta = Convert.ToDateTime(dr["FechaVenta"]),
                        Total = Convert.ToInt32(dr["total"]),
                        Usuario = Convert.ToString(dr["Usuario"]),
                    });
            }
            return ListadoVentas;
        }

        public void EditarVenta(List<LineaVentas> lv, String usuario, int idventa)
        {
            connection();
            SqlCommand cmd = new SqlCommand("EditarVenta", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FechaVenta", DateTime.UtcNow);
            cmd.Parameters.AddWithValue("@Usuario", usuario);
            cmd.Parameters.AddWithValue("@idVenta", idventa);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            foreach (var item in lv)
            {
                LineaVentaHandle lvh = new LineaVentaHandle();
                item.idVenta = idventa;
                lvh.AgregarLinea(item);
            }
        }

        public bool AgregarUsoDeInsumo(List<LineaVentas> lineasDeClases, int cantidad) // cantidad es la cantidad de alumnos que asistieron a esa clase
        {
            Ventas v = new Ventas();
            v.TipoVenta = "Uso Insumo";
            v.Usuario = "Ninguno";
            v.FechaVenta = DateTime.UtcNow.AddHours(-3);
            connection();
            SqlCommand cmd = new SqlCommand("AgregarVenta", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FechaVenta", v.FechaVenta);
            cmd.Parameters.AddWithValue("@Concepto", v.TipoVenta);
            cmd.Parameters.AddWithValue("@Usuario", v.Usuario);
            cmd.Parameters.Add("@lastId", SqlDbType.Int).Direction = ParameterDirection.Output;

            con.Open();

            int i = cmd.ExecuteNonQuery();
            int idventa = Convert.ToInt32(cmd.Parameters["@lastId"].Value);
            con.Close();

            if (i >= 1)
            {
                LineaVentaHandle lvh = new LineaVentaHandle();
                foreach (var item in lineasDeClases)
                {

                    LineaVentas lv = new LineaVentas(item.idProducto, idventa, item.Cantidad * cantidad, 0);
                    lvh.AgregarLinea(lv);
                }

                return true;
            }

            else
                return false;
        }

        public bool RestarCorreccion(int idproducto, Decimal correccion, string nombre, string unidad)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarVenta", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FechaVenta", DateTime.UtcNow.AddHours(-3));
            cmd.Parameters.AddWithValue("@Concepto", "Restar Correccion");
            cmd.Parameters.AddWithValue("@Usuario", "Ninguno");

            cmd.Parameters.Add("@lastId", SqlDbType.Int).Direction = ParameterDirection.Output;

            con.Open();

            int i = cmd.ExecuteNonQuery();
            int idventa = Convert.ToInt32(cmd.Parameters["@lastId"].Value);
            con.Close();

            if (i >= 1)
            {
                LineaVentaHandle lvh = new LineaVentaHandle();
                LineaVentas lv = new LineaVentas(idproducto, idventa, correccion, 0);
                lvh.AgregarLinea(lv);
                return true;
            }

            else
                return false;
        }
    }
}