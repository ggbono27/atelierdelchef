﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_1.Models
{
    public class Asistencias
    {
        public int idAlumno;
        public int idClase;
        public int idCurso;
        public DateTime FechaHoraInicio;
        public DateTime FechaHoraFin;
        public String Estado;

        public Asistencias()
        { }

        public Asistencias(int idA, int idC, int idCur, DateTime FI, DateTime FF, String State)
        {
            idAlumno = idA;
            idClase = idC;
            idCurso = idCur;
            FechaHoraInicio = FI;
            FechaHoraFin = FF;
            Estado = State;
        }
    }
}