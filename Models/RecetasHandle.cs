﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Proyecto_1.Models
{
    public class RecetasHandle
    {
        private SqlConnection _con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["EscuelaCon"].ToString();
            _con = new SqlConnection(constring);
        }

        public bool AgregarReceta(Recetas receta)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarReceta", _con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Preparacion", receta.Preparacion);
            cmd.Parameters.AddWithValue("@Nombre", receta.Nombre);

            _con.Open();
            int i = cmd.ExecuteNonQuery();
            _con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public bool AgregarReceta2(Recetas receta)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarReceta2", _con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Preparacion", receta.Preparacion);
            cmd.Parameters.AddWithValue("@Nombre", receta.Nombre);
            cmd.Parameters.AddWithValue("@Ingredientes", receta.Ingredientes);
            cmd.Parameters.AddWithValue("@Tips", receta.Tips);

            _con.Open();
            int i = cmd.ExecuteNonQuery();
            _con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }


        public List<Recetas> ListarRecetas()
        {
            connection();
            List<Recetas> ListadoRecetas = new List<Recetas>();

            SqlCommand cmd = new SqlCommand("ListarRecetas", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoRecetas.Add(
                    new Recetas
                    {
                        Preparacion = Convert.ToString(dr["Preparacion"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        idReceta = Convert.ToInt32(dr["idReceta"])
                    });
            }
            return ListadoRecetas;
        }

        public List<Recetas> ListarRecetas2()
        {
            connection();
            List<Recetas> ListadoRecetas = new List<Recetas>();

            SqlCommand cmd = new SqlCommand("ListarRecetas2", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoRecetas.Add(
                    new Recetas
                    {
                        Preparacion = Convert.ToString(dr["Preparacion"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        idReceta = Convert.ToInt32(dr["idReceta"]),
                        Ingredientes= Convert.ToString(dr["Ingredientes"]),
                        Tips= Convert.ToString(dr["Tips"])
                    });
            }
            return ListadoRecetas;
        }


        //Este metodo trae los productos del tipo insumo que componen la receta y la correspondiente cantidad.
        //usa el modelo "producto" donde en el atributo precio, setea la cantidad (en bd cantidad se encuentra donde corresponde, es decir la tabla preparaciones)
        public List<Productos> ListarIngredientes(int idReceta)
        {
            connection();
            List<Productos> ListadoIngredientes = new List<Productos>();

            SqlCommand cmd = new SqlCommand("ListarIngredientes", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idReceta", idReceta);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoIngredientes.Add(
                    new Productos
                    {
                        Precio = Convert.ToInt32(dr["Cantidad"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        idProducto = Convert.ToInt32(dr["idProducto"]),
                        Unidad = Convert.ToString(dr["Unidad"])
                    });
            }
            return ListadoIngredientes;
        }

        public bool EditarReceta(Recetas r)
        {
            connection();
            SqlCommand cmd = new SqlCommand("EditarReceta", _con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@id", r.idReceta);
            cmd.Parameters.AddWithValue("@Preparacion", r.Preparacion);
            cmd.Parameters.AddWithValue("@Nombre", r.Nombre);

            _con.Open();
            int i = cmd.ExecuteNonQuery();
            _con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public bool EditarReceta2(Recetas r)
        {
            connection();
            SqlCommand cmd = new SqlCommand("EditarReceta2", _con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@id", r.idReceta);
            cmd.Parameters.AddWithValue("@Preparacion", r.Preparacion);
            cmd.Parameters.AddWithValue("@Nombre", r.Nombre);
            cmd.Parameters.AddWithValue("@Ingredientes", r.Ingredientes);
            cmd.Parameters.AddWithValue("@Tips", r.Tips);

            _con.Open();
            int i = cmd.ExecuteNonQuery();
            _con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public bool BorrarReceta(int id)
        {
            connection();
            SqlCommand cmd = new SqlCommand("BorrarReceta", _con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@id", id);

            _con.Open();
            int i = cmd.ExecuteNonQuery();
            _con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public int RecuperarIdReceta()
        {
            int idReceta = 0;
            connection();

            SqlCommand cmd = new SqlCommand("RecuperarIdReceta", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                idReceta = Convert.ToInt32(dr["idReceta"]);
            }

            return idReceta;
        }

        public bool EsRepetido(string nombre)
        {
            if (nombre == null)
            {
                return true;
            }
            connection();

            SqlCommand cmd = new SqlCommand("EsRecetaRepetida", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            cmd.Parameters.AddWithValue("@Nombre", nombre);
            _con.Open();
            sd.Fill(dt);
            _con.Close();

            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public List<Recetas> BuscarReceta(string cadena)
        {
            connection();
            List<Recetas> ListadoRecetas = new List<Recetas>();

            SqlCommand cmd = new SqlCommand("BuscarReceta", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@cadena", cadena);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoRecetas.Add(
                    new Recetas
                    {
                        Preparacion= Convert.ToString(dr["Preparacion"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        idReceta = Convert.ToInt32(dr["idReceta"])
                    });
            }
            return ListadoRecetas;

        }

        public List<Recetas> BuscarReceta2(string cadena)
        {
            connection();
            List<Recetas> ListadoRecetas = new List<Recetas>();

            SqlCommand cmd = new SqlCommand("BuscarReceta2", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@cadena", cadena);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoRecetas.Add(
                    new Recetas
                    {
                        Preparacion = Convert.ToString(dr["Preparacion"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        idReceta = Convert.ToInt32(dr["idReceta"]),
                        Ingredientes=Convert.ToString(dr["Ingredientes"]),
                        Tips=Convert.ToString(dr["Tips"])
                    });
            }
            return ListadoRecetas;

        }

        public List<Recetas> TraerReceta(int idReceta)
        {
            connection();
            List<Recetas> ListadoRecetas = new List<Recetas>();

            SqlCommand cmd = new SqlCommand("TraerReceta", _con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idReceta", idReceta);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            _con.Open();
            sd.Fill(dt);
            _con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoRecetas.Add(
                    new Recetas
                    {
                        Preparacion = Convert.ToString(dr["Preparacion"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        idReceta = Convert.ToInt32(dr["idReceta"]),
                        Ingredientes = Convert.ToString(dr["Ingredientes"]),
                        Tips = Convert.ToString(dr["Tips"])
                    });
            }
            return ListadoRecetas;

        }
    }
}