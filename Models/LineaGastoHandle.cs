﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Proyecto_1.Models
{
    public class LineaGastoHandle
    {

        private SqlConnection con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["EscuelaCon"].ToString();
            con = new SqlConnection(constring);
        }


        public bool AgregarLineaCompraInsumo(LineaGastos lc)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarLineaCompraInsumo", con);
            cmd.CommandType = CommandType.StoredProcedure;
            
            cmd.Parameters.AddWithValue("@idGasto", lc.idGasto);
            cmd.Parameters.AddWithValue("@idProducto", lc.idProducto);
            cmd.Parameters.AddWithValue("@Cantidad", lc.Cantidad);
            cmd.Parameters.AddWithValue("@idTipoGasto", lc.idTipoGasto);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public bool AgregarLineaGasto(LineaGastos lc)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarLineaGasto", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idGasto", lc.idGasto);
            cmd.Parameters.AddWithValue("@idProducto", lc.idProducto);
            cmd.Parameters.AddWithValue("@Cantidad", lc.Cantidad);
            cmd.Parameters.AddWithValue("@Precio", lc.Precio);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public bool AgregarLineaCompraBazar(LineaGastos lc)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarLineaGasto", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idTipoGasto", lc.idTipoGasto);
            cmd.Parameters.AddWithValue("@idGasto", lc.idGasto);
            cmd.Parameters.AddWithValue("@idProducto", lc.idProducto);
            cmd.Parameters.AddWithValue("@Cantidad", lc.Cantidad);
            cmd.Parameters.AddWithValue("@Precio", lc.Precio);


            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public bool EditarLineaCompraBazar(LineaGastos lc)
        {
            connection();
            SqlCommand cmd = new SqlCommand("EditarLineaGasto", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idGasto", lc.idGasto);
            cmd.Parameters.AddWithValue("@idProducto", lc.idProducto);
            cmd.Parameters.AddWithValue("@Cantidad", lc.Cantidad);
            cmd.Parameters.AddWithValue("@Precio", lc.Precio);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

    }
}