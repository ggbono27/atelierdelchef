﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Proyecto_1.Models
{
    public class Alumno
    {
        [DisplayName("Identificador del Alumno:")]
        public int idAlumno { get; set; }

        [DisplayName("Nombre:")]
        [RegularExpression("^[a-zA-Z\\s\\u00E0-\u00FC]+$", ErrorMessage = "El nombre debe contener solo letras")]   //solo acepta letras mayusculas o minusculas con o sin acentos y tambien espacios
        [Required]
        public string Nombre { get; set; }

        [DisplayName("Apellido:")]
        [RegularExpression("^[a-zA-Z\\s\\u00E0-\u00FC]+$", ErrorMessage = "El apellido debe contener solo letras")]
        [Required]
        public string Apellido { get; set; }

        [DisplayName("Dirección:")]
        public string Direccion { get; set; }

        [DisplayName("Teléfono:")]
        [Phone]
        [StringLength(14, MinimumLength = 7, ErrorMessage = "El télefono debe tener 7 a 14 caracteres")]
        [Required]
        public string Telefono { get; set; }

        [DisplayName("Teléfono opcional:")]
        [Phone]
        [StringLength(14, MinimumLength = 7, ErrorMessage = "El télefono debe tener 7 a 14 caracteres")]
        public string Telefono2 { get; set; }

        [DisplayName("DNI:")]
        //[DisplayFormat(NullDisplayText = "Solo numeros sin puntos")]
        //[RegularExpression("^[0-9]{7,8}$", ErrorMessage = "Dni debe tener 7 u 8 numeros sin puntos")]
        public int Dni { get; set; }

        [DisplayName("Fecha de nacimiento:")]
        public string fecha { get; set; }

        [DataType(DataType.DateTime)]
        //[DisplayName("Fecha de nacimiento:"),
        [DisplayName("Fecha de nacimiento:")]
        //DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        public DateTime FechaNac { get; set; }

        [DisplayName("E-Mail:")]
        public String Mail { get; set; }

        [DisplayName("¿Quién lo inscribe?:")]
        [RegularExpression("^[a-zA-Z\\s\\u00E0-\u00FC]+$", ErrorMessage = "El usuario debe contener solo letras")]   //solo acepta letras mayusculas o minusculas con o sin acentos y tambien espacios
        public string Usuario { get; set; }

        [DisplayName("Genero:")]
        public string Sexo { get; set; }

        public Alumno()
        { }

        public Alumno(int idAl, String Apel, String Nomb, String Dir, int D, DateTime fn, String Tel, String Mai, String sfn,  string sexo)
        {
            idAlumno = idAl;
            Apellido = Apel;
            Nombre = Nomb;
            Direccion = Dir;
            Dni = D;
            FechaNac = fn.Date;
            fecha = sfn;
            Telefono = Tel;
            Mail = Mai;
            Sexo = sexo;
        }
        public Alumno(String Apel, String Nomb, String Dir, int D, String fn, String Tel, String mail, string sexo)
        {
            Apellido = Apel;
            Nombre = Nomb;
            Direccion = Dir;
            Dni = D;
            fecha = fn;
            Telefono = Tel;
            Mail = mail;
            Sexo = sexo;
        }
    }
}