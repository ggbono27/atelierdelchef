﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_1.Models
{
    public class TipoGastos
    {
        public int idTipoGasto { set; get; }
        public String TipoGasto { set; get; }

        public TipoGastos()
        { }

        public TipoGastos(int idtipogastos, String tipogasto)
        {
            idTipoGasto = idtipogastos;
            TipoGasto = tipogasto;
        }

    }
}