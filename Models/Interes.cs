﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_1.Models
{
    public class Interes
    {
        public int idInteresado { get; set; }
        public int idCurso { get; set; }

        public Interes()
        {}

        public Interes(int idinteresado, int idcurso)
        {
            idInteresado = idinteresado;
            idCurso = idcurso;
        }
    }
}