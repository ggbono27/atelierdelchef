﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Proyecto_1.Models
{
    public class Recetas
    {
        public int idReceta { get; set; }

        [DisplayName("Preparación:")]
        public string Preparacion { get; set; }

        [Required]
        [DisplayName("Nombre:")]
        public string Nombre { get; set; }

        public string Ingredientes { get; set; }

        public string Tips { get; set; }

        public Recetas()
        { }

        public Recetas(int idR, string prep, string nombre)
        {
            idReceta = idR;
            Nombre = nombre;
            Preparacion = prep;  
        }

        public Recetas(string preparacion, string nombre, string ingredientes, string tips)
        {
            Nombre = nombre;
            Preparacion = preparacion;
            Ingredientes = ingredientes;
            Tips = tips;
        }

        public Recetas(int idRecet, string preparacion, string nombre, string ingredientes, string tips)
        {
            idReceta = idRecet;
            Nombre = nombre;
            Preparacion = preparacion;
            Ingredientes = ingredientes;
            Tips = tips;
        }
    }
}