﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_1.Models
{
    public class Preparaciones
    {
        public int idReceta { get; set; }
        public int idProducto { get; set; }
        public float Cantidad { get; set; }
        public string Unidad { get; set; }

        public Preparaciones()
        { }

        public Preparaciones(int idRec, int idProd, float Cant, string unidad)
        {
            idReceta = idRec;
            idProducto = idProd;
            Cantidad = Cant;
            Unidad = unidad;
        }
        
    }
}