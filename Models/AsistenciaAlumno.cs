﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_1.Models
{
    public class AsistenciaAlumno
    {
        public String Nombre { get; set; }
        public String Apellido { get; set; }
        public String Estado { get; set; }

        public AsistenciaAlumno()
        { }

        public AsistenciaAlumno(string nombre, string apellido, string estado)
        {
            Nombre = nombre;
            Apellido = apellido;
            Estado = estado; 
        }
    }
}