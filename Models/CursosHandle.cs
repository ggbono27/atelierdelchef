﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Data;

namespace Proyecto_1.Models
{
    public class CursosHandle
    {
        private SqlConnection con;

        private void connection()
        {
            String ConString = ConfigurationManager.ConnectionStrings["EscuelaCon"].ToString();
            con = new SqlConnection(ConString);
        }

        public List<Cursos> TraerCostoCurso(int idcurso)
        {
            connection();
            List<Cursos> ListadoCursos = new List<Cursos>();

            SqlCommand cmd = new SqlCommand("TraerCostoCurso", con);
            cmd.Parameters.AddWithValue("@idCurso", idcurso);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoCursos.Add(
                    new Cursos
                    {
                        Costo = Convert.ToInt32(dr["Costo"]),
                    });
            }
            return ListadoCursos;

        }

        public bool AgregarCursos(Cursos C1)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarCurso", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Nombre", C1.Nombre);
            cmd.Parameters.AddWithValue("@Descripcion", C1.Descripcion);
            cmd.Parameters.AddWithValue("@Costo", C1.Costo);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;

            else
                return false;
        }

        public List<Cursos> ListarCursos()
        {
            connection();
            List<Cursos> ListadoCursos = new List<Cursos>();

            SqlCommand cmd = new SqlCommand("ListarCursos", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoCursos.Add(
                    new Cursos
                    {
                        idCurso = Convert.ToInt32(dr["idCurso"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Descripcion = Convert.ToString(dr["Descripcion"]),
                        Costo = Convert.ToInt32(dr["Costo"]),
                    });
            }
            return ListadoCursos;
        }

        public List<Cursos> ListarCursos2()
        {
            connection();
            List<Cursos> ListadoCursos = new List<Cursos>();

            SqlCommand cmd = new SqlCommand("ListarCursos2", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoCursos.Add(
                    new Cursos
                    {
                        idCurso = Convert.ToInt32(dr["idCurso"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Descripcion = Convert.ToString(dr["Descripcion"]),
                        Costo = Convert.ToInt32(dr["Costo"]),
                    });
            }
            return ListadoCursos;
        }

        public bool BorrarCurso(int id)
        {
            connection();
            SqlCommand cmd = new SqlCommand("BorrarCurso",con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i > 1)
                return true;
            else
                return false;

        }

        public bool EditarCursos(Cursos Curso)
        {
            connection();
            SqlCommand cmd = new SqlCommand("EditarCurso", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", Curso.idCurso);
            cmd.Parameters.AddWithValue("@Nombre", Curso.Nombre);
            cmd.Parameters.AddWithValue("@Descripcion", Curso.Descripcion);
            cmd.Parameters.AddWithValue("@Costo", Curso.Costo);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i > 1)
                return true;
            else
                return false;

        }

        public IList<Cursos> BuscarCursos(String cadena)
        {
            connection();
            List<Cursos>  ListadoCursos = new List<Cursos>();
            SqlCommand cmd = new SqlCommand("BuscarCursos", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@cadena", cadena);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoCursos.Add(
                    new Cursos
                    {
                        idCurso = Convert.ToInt32(dr["idCurso"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Descripcion = Convert.ToString(dr["Descripcion"]),
                        Costo = Convert.ToInt32(dr["Costo"]),
                    });
            }
            return ListadoCursos;
        }
       
        public List<Alumno> AlumnosPotenciales(int idcurso)
        {
            connection();
            List<Alumno> ListadoAlumnos = new List<Alumno>();

            SqlCommand cmd = new SqlCommand("AlumnosPotenciales", con);
            cmd.Parameters.AddWithValue("@idCurso", idcurso);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoAlumnos.Add(
                    new Alumno
                    {
                        idAlumno = Convert.ToInt32(dr["idAlumno"]),
                        Apellido = Convert.ToString(dr["Apellido"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Direccion = Convert.ToString(dr["Direccion"]),
                        Telefono = Convert.ToString(dr["Telefono"]),
                        Mail = Convert.ToString(dr["Mail"]),
                        Dni = Convert.ToInt32(dr["DNI"])
                    });
            }
            return ListadoAlumnos;
        }

        public List<Cursos> TraerNombreCurso(int idcurso)
        {
            connection();
            List<Cursos> ListadoCurso = new List<Cursos>();

            SqlCommand cmd = new SqlCommand("TraerNombreCurso", con);
            cmd.Parameters.AddWithValue("@idCurso", idcurso);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoCurso.Add(
                    new Cursos
                    {
                        Nombre = Convert.ToString(dr["Nombre"]),
                    });
            }
            return ListadoCurso;
        }

        public List<Pagos> IngresosPorCursos()
        {
            connection();
            List<Pagos> ListadoPagosdeCursos = new List<Pagos>();

            SqlCommand cmd = new SqlCommand("IngresosPorCursos", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoPagosdeCursos.Add(
                    new Pagos
                    {
                        idCurso = Convert.ToInt32(dr["idCurso"]),
                        Concepto = Convert.ToString(dr["Nombre"]),
                        Monto = Convert.ToInt32(dr["total_ingresos"]),
                    });
            }
            return ListadoPagosdeCursos;
        }

        public List<Pagos> IngresosPorCursosEntreFecha(DateTime fecha1, DateTime fecha2)
        {
            connection();
            List<Pagos> ListadoCursos = new List<Pagos>();
            SqlCommand cmd = new SqlCommand("IngresosPorCursosEntreFecha", con);
            cmd.Parameters.AddWithValue("@fecha1", fecha1);
            cmd.Parameters.AddWithValue("@fecha2", fecha2);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoCursos.Add(
                    new Pagos
                    {
                        idCurso = Convert.ToInt32(dr["idCurso"]),
                        Concepto = Convert.ToString(dr["Nombre"]),
                        Monto = Convert.ToInt32(dr["total_ingresos"]),
                    });
            }
            return ListadoCursos;
        }

    }
}