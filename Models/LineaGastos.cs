﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_1.Models
{
    public class LineaGastos
    {
       public int idGasto { get; set; }
       public int idProducto { get; set; }

    public int idTipoGasto { get; set; }

       public Decimal Cantidad { get; set; }

      public  Decimal Precio { get; set; }

     public   Decimal SubTotal { get; set; }

     public   String Nombre { get; set; }

        public String Unidad { get; set; }

        public LineaGastos()
        {}

        public LineaGastos(int idgasto, int idproducto, Decimal cantidad, Decimal precio, Decimal subtotal, String nombre)
        {
            idGasto = idgasto;
            idProducto = idproducto;
            Cantidad = cantidad;
            Precio = precio;
            SubTotal = subtotal;
            Nombre = nombre;
        }

        public LineaGastos(int idproducto, Decimal cantidad, Decimal precio, Decimal subtotal, String nombre, String unidad)
        {
            idProducto = idproducto;
            Cantidad = cantidad;
            Precio = precio;
            SubTotal = subtotal;
            Nombre = nombre;
            Unidad = unidad;
        }

        public LineaGastos(int idproducto, Decimal cantidad, Decimal precio, Decimal subtotal, String nombre)
        {
            idProducto = idproducto;
            Cantidad = cantidad;
            Precio = precio;
            SubTotal = subtotal;
            Nombre = nombre;
        }

        public LineaGastos(int idproducto, Decimal cantidad, string nombre, string unidad)
        {
            idProducto = idproducto;
            Cantidad = cantidad;
            Nombre = nombre;
            Unidad = unidad;
        }

        public LineaGastos(int idgasto, int idproducto, Decimal cantidad, string nombre, string unidad)
        {
            idGasto = idgasto;
            idProducto = idproducto;
            Cantidad = cantidad;
            Nombre = nombre;
            Unidad = unidad;
        }

        public LineaGastos(int idgasto, int idproducto, Decimal cantidad)
        {
            idGasto = idgasto;
            idProducto = idproducto;
            Cantidad = cantidad;
        }
    }
}