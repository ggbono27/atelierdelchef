﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Proyecto_1.Models
{
    public class Turnos
    {
        public int idTurno { get; set; }
        public int idCurso { get; set; }

        [System.ComponentModel.DisplayName("Cantidad max de alumnos:")]
        public int Alumnos { get; set; }

        [Required]
        [System.ComponentModel.DisplayName("Dia:")]
        public string Dia { get; set; }

        [Required]
        [System.ComponentModel.DisplayName("Hora Inicio:")]
        [RegularExpression("^[0-9:]{1,5}$", ErrorMessage = "Solo se admiten numeros y un separador para minutos de 2 puntos. Ej: 14:30")]
        [StringLength(5, MinimumLength = 1, ErrorMessage = "Debe tener al menos un numero y maximo 4")]
        public string Inicio { get; set; }           //Inicio es la hora de comienzo del turno

        [Required]
        [System.ComponentModel.DisplayName("Hora Fin:")]
        [RegularExpression("^[0-9:]{1,5}$", ErrorMessage = "Solo se admiten numeros y un separador de 2 puntos. Ej: 9:15")]
        [StringLength(5, MinimumLength = 1, ErrorMessage = "Debe tener al menos un numero y maximo 4")]
        public string Fin { get; set; }

        [System.ComponentModel.DisplayName("Fecha inicio de clase:"),
         DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.DateTime)]
        [Required]
        public string s_FechaInicio { get; set; }

        [System.ComponentModel.DisplayName("Fecha inicio de clase:"),
         DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.DateTime)]
        [Required]
        public DateTime FechaInicio { get; set; }

        public Turnos()
        { }

        public Turnos(int idT, int idC, String Day, DateTime FI, String sFI, String inicio, String fin, int alumnos)
        {
            idTurno = idT;
            idCurso = idC;
            Dia = Day;
            FechaInicio = FI.Date;
            s_FechaInicio = sFI;
            Inicio = inicio;
            Fin = fin;
            Alumnos = alumnos;
        }

        public Turnos(int idC, String Day, DateTime FI, String inicio, String fin, int alumnos)
        {
            idCurso = idC;
            Dia = Day;
            FechaInicio = FI;
            Inicio = inicio;
            Fin = fin;
            Alumnos = alumnos;
        }
    }
}