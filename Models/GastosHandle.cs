﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Proyecto_1.Models;
using Proyecto_1.ViewModels;

namespace Proyecto_1.Models
{
    public class GastosHandle
    {


        private SqlConnection con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["EscuelaCon"].ToString();
            con = new SqlConnection(constring);
        }

        public List<Gastos> BuscarGastoPorTipo(int idTipoGasto)
        {
            connection();
            List<Gastos> ListadoGastos = new List<Gastos>();

            SqlCommand cmd = new SqlCommand("BuscarGastoPorTipo", con);
            cmd.Parameters.AddWithValue("@idTipoGasto", idTipoGasto);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoGastos.Add(
                    new Gastos
                    {
                        idTipoGasto = Convert.ToInt32(dr["idTipoGasto"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Tipo = Convert.ToString(dr["Tipo"]),

                    });
            }
            return ListadoGastos;
        }

        public void BorrarCompraBazar(int idgasto)
        {
            connection();
            SqlCommand cmd = new SqlCommand("BorrarCompraBazar", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idGasto", idgasto);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public bool AgregarCompra(List<LineaGastos> lc, String nombre)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarCompra", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Fecha", DateTime.UtcNow.AddHours(-3));
            cmd.Parameters.AddWithValue("@Nombre", "Agregar stock");
            cmd.Parameters.AddWithValue("@Usuario", nombre);
            cmd.Parameters.AddWithValue("@idTipoGasto", 5);
            cmd.Parameters.Add("@lastId", SqlDbType.Int).Direction = ParameterDirection.Output;

            con.Open();

            int i = cmd.ExecuteNonQuery();
            int idgasto = Convert.ToInt32(cmd.Parameters["@lastId"].Value);
            con.Close();

            if (i >= 1)
            {
                foreach (var item in lc)
                {
                    LineaGastoHandle lch = new LineaGastoHandle();
                    item.idGasto = idgasto;
                    item.idTipoGasto = 5;
                    lch.AgregarLineaCompraInsumo(item);
                }

                return true;
            }

            else
                return false;
        }

        public List<Caja> ListarGastosCaja()
        {
            connection();
            List<Caja> ListadoCaja = new List<Caja>();

            SqlCommand cmd = new SqlCommand("ListarGastosCaja", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoCaja.Add(
                    new Caja
                    {
                        Monto = Convert.ToDecimal(dr["Monto"]),
                        Fecha = Convert.ToDateTime(dr["Fecha"]),
                        Concepto = Convert.ToString(dr["Concepto"]),
                        Usuario = Convert.ToString(dr["Usuario"]),
                        Concepto2 = Convert.ToString(dr["Tipo"]) + " - "
                    });
            }

            cmd = new SqlCommand("ListarComprasBazarCaja", con);
            cmd.CommandType = CommandType.StoredProcedure;
            sd = new SqlDataAdapter(cmd);
            dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoCaja.Add(
                    new Caja
                    {
                        Monto = Convert.ToDecimal(dr["Monto"]),
                        Fecha = Convert.ToDateTime(dr["Fecha"]),
                        Concepto = Convert.ToString(dr["Concepto"]),
                        Usuario = Convert.ToString(dr["Usuario"])
                    });
            }
            return ListadoCaja;
        }

        public List<Caja> ListarGastosCajaPorRangoFecha(DateTime fecha1, DateTime fecha2)
        {
            connection();
            List<Caja> ListadoCaja = new List<Caja>();

            SqlCommand cmd = new SqlCommand("ListarGastosCajaPorFecha", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fecha", fecha1);
            cmd.Parameters.AddWithValue("@fecha2", fecha2);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoCaja.Add(
                    new Caja
                    {
                        Monto = Convert.ToDecimal(dr["Monto"]),
                        Fecha = Convert.ToDateTime(dr["Fecha"]),
                        Concepto = Convert.ToString(dr["Nombre"]),
                        Usuario = Convert.ToString(dr["Usuario"]),
                        Concepto2 = Convert.ToString(dr["Tipo"]) + " - "
                    });

            }

            cmd = new SqlCommand("ListarComprasBazarCajaPorFecha", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fecha", fecha1);
            cmd.Parameters.AddWithValue("@fecha2", fecha2);
            sd = new SqlDataAdapter(cmd);
            dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoCaja.Add(
                    new Caja
                    {
                        Monto = Convert.ToDecimal(dr["Monto"]),
                        Fecha = Convert.ToDateTime(dr["Fecha"]),
                        Concepto = Convert.ToString(dr["Concepto"]),
                        Usuario = Convert.ToString(dr["Usuario"]),
                        Concepto2 = ""//Convert.ToString(dr["Concepto"])
                    });

            }
            return ListadoCaja;
        }



        public bool SumarCorreccion(int idproducto, int correccion, string nombre, string unidad)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarCompra", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Fecha", DateTime.UtcNow.AddHours(-3));
            cmd.Parameters.AddWithValue("@Nombre", "Agregar Correccion");
            cmd.Parameters.AddWithValue("@idTipoGasto", 8);
            cmd.Parameters.AddWithValue("@Usuario", "Ninguno");
            cmd.Parameters.Add("@lastId", SqlDbType.Int).Direction = ParameterDirection.Output;

            con.Open();

            int i = cmd.ExecuteNonQuery();
            int idgasto = Convert.ToInt32(cmd.Parameters["@lastId"].Value);
            con.Close();

            if (i >= 1)
            {
                LineaGastoHandle lgh = new LineaGastoHandle();
                LineaGastos lg = new LineaGastos(idgasto, idproducto, correccion, nombre, unidad);
                lg.idTipoGasto = 8;
                lgh.AgregarLineaCompraInsumo(lg);
                return true;
            }

            else
                return false;
        }

        public bool PagarGasto(String usuario, int idtipogasto, Decimal monto, string nombre, DateTime fecha)
        {
            connection();
            SqlCommand cmd = new SqlCommand("PagarGasto", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Fecha", fecha);
            cmd.Parameters.AddWithValue("@Nombre", nombre);
            cmd.Parameters.AddWithValue("@Usuario", usuario);
            cmd.Parameters.AddWithValue("@Monto", monto);
            cmd.Parameters.AddWithValue("@idTipoGasto", idtipogasto);

            con.Open();

            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
            {
                return true;
            }
            else
                return false;
        }

        public bool AgregarCompraBazar(List<LineaGastos> lc, String usuario)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AgregarCompra", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@Fecha", DateTime.UtcNow.AddHours(-3));
            cmd.Parameters.AddWithValue("@Fecha", DateTime.UtcNow);
            cmd.Parameters.AddWithValue("@Nombre", "Compra bazar");
            cmd.Parameters.AddWithValue("@Usuario", usuario);
            cmd.Parameters.AddWithValue("@idTipoGasto", 3);
            cmd.Parameters.Add("@lastId", SqlDbType.Int).Direction = ParameterDirection.Output;

            con.Open();

            int i = cmd.ExecuteNonQuery();
            int idgasto = Convert.ToInt32(cmd.Parameters["@lastId"].Value);
            con.Close();

            if (i >= 1)
            {
                foreach (var item in lc)
                {
                    LineaGastoHandle lch = new LineaGastoHandle();
                    item.idGasto = idgasto;
                    item.idTipoGasto = 3;
                    lch.AgregarLineaCompraBazar(item);
                }

                return true;
            }

            else
                return false;
        }


        public void EditarCompraBazar(List<LineaGastos> lc, String usuario, int idgasto)
        {
            connection();
            SqlCommand cmd = new SqlCommand("EditarCompra", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Fecha", DateTime.UtcNow);
            cmd.Parameters.AddWithValue("@Usuario", usuario);
            cmd.Parameters.AddWithValue("@idGasto", idgasto);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

            foreach (var item in lc)
            {
                LineaGastoHandle lch = new LineaGastoHandle();
                item.idGasto = idgasto;
                item.idTipoGasto = 3;
                lch.AgregarLineaCompraBazar(item);
            }
        }


        public List<Gastos> ListarComprasInsumo()
        {
            connection();
            List<Gastos> ListadoCompras = new List<Gastos>();

            SqlCommand cmd = new SqlCommand("ListaCompras", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoCompras.Add(
                    new Gastos
                    {
                        idGasto = Convert.ToInt32(dr["idGasto"]),
                        FechaGasto = Convert.ToDateTime(dr["Fecha"]),
                        Usuario = Convert.ToString(dr["Usuario"]),

                    });
            }
            return ListadoCompras;
        }


        public List<LineaGastos> DetalleCompraInsumo(int idcompra)
        {
            connection();
            List<LineaGastos> ListadoLineas = new List<LineaGastos>();

            SqlCommand cmd = new SqlCommand("DetalleCompraInsumo", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idcompra", idcompra);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoLineas.Add(
                    new LineaGastos
                    {
                        idProducto = Convert.ToInt32(dr["idProducto"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Unidad = Convert.ToString(dr["Unidad"]),
                        Cantidad = Convert.ToInt32(dr["Cantidad"]),
                    });
            }
            return ListadoLineas;
        }

        public List<LineaGastos> DetalleCompraBazar(int idcompra)
        {
            connection();
            List<LineaGastos> ListadoLineas = new List<LineaGastos>();

            SqlCommand cmd = new SqlCommand("DetalleCompra", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idcompra", idcompra);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoLineas.Add(
                    new LineaGastos
                    {
                        idGasto = Convert.ToInt32(dr["idGasto"]),
                        idProducto = Convert.ToInt32(dr["idProducto"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Cantidad = Convert.ToInt32(dr["Cantidad"]),
                        Precio = Convert.ToDecimal(dr["Precio"]),
                        SubTotal = Convert.ToDecimal(dr["subtotal"])

                    });
            }
            return ListadoLineas;
        }

        public List<Gastos> ListarGastos()
        {
            connection();
            List<Gastos> ListadoGasto = new List<Gastos>();

            SqlCommand cmd = new SqlCommand("ListaGastos", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoGasto.Add(
                    new Gastos
                    {
                        idGasto = Convert.ToInt32(dr["idGasto"]),
                        FechaGasto = Convert.ToDateTime(dr["Fecha"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Total = Convert.ToDecimal(dr["Monto"]),
                        Usuario = Convert.ToString(dr["Usuario"]),
                        Tipo = Convert.ToString(dr["Tipo"])

                    });
            }
            return ListadoGasto;
        }

        public List<Gastos> ListarGastosParaPagar()
        {
            connection();
            List<Gastos> ListadoGasto = new List<Gastos>();

            SqlCommand cmd = new SqlCommand("ListarGastosParaPagar", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoGasto.Add(
                    new Gastos
                    {
                        idTipoGasto = Convert.ToInt32(dr["idTipoGasto"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Tipo = Convert.ToString(dr["Tipo"])
                    });
            }
            return ListadoGasto;
        }

        public List<Gastos> ListarComprasProductosVenta()
        {
            connection();
            List<Gastos> ListadoComprasBazar = new List<Gastos>();

            SqlCommand cmd = new SqlCommand("ListaComprasPV", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoComprasBazar.Add(
                    new Gastos
                    {
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Tipo = Convert.ToString(dr["Tipo"]),
                        idGasto = Convert.ToInt32(dr["idGasto"]),
                        FechaGasto = Convert.ToDateTime(dr["Fecha"]),
                        Total = Convert.ToDecimal(dr["total"]),
                        Usuario = Convert.ToString(dr["Usuario"]),

                    });
            }
            return ListadoComprasBazar;
        }



        public List<Gastos> BuscarGasto(DateTime fecha1, DateTime fecha2)
        {
            connection();
            List<Gastos> ListadoGasto = new List<Gastos>();
            SqlCommand cmd = new SqlCommand("BuscarGasto", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fecha", fecha1);
            cmd.Parameters.AddWithValue("@fecha2", fecha2);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoGasto.Add(
                    new Gastos
                    {
                        idGasto = Convert.ToInt32(dr["idGasto"]),
                        FechaGasto = Convert.ToDateTime(dr["Fecha"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Total = Convert.ToDecimal(dr["total"]),
                        Usuario = Convert.ToString(dr["Usuario"]),
                        Tipo = Convert.ToString(dr["Tipo"]),
                    });
            }
            return ListadoGasto;
        }

        public List<Gastos> BuscarGasto2(DateTime fecha1, DateTime fecha2, int idtipogasto)
        {
            connection();
            List<Gastos> ListadoGasto = new List<Gastos>();
            SqlCommand cmd = new SqlCommand("BuscarGasto2", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fecha", fecha1);
            cmd.Parameters.AddWithValue("@fecha2", fecha2);
            cmd.Parameters.AddWithValue("@idTipoGasto", idtipogasto);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoGasto.Add(
                    new Gastos
                    {
                        idGasto = Convert.ToInt32(dr["idGasto"]),
                        FechaGasto = Convert.ToDateTime(dr["Fecha"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Total = Convert.ToInt32(dr["total"]),
                        Usuario = Convert.ToString(dr["Usuario"]),
                        Tipo = Convert.ToString(dr["Tipo"]),
                    });
            }
            return ListadoGasto;
        }


        public List<Gastos> BuscarCompraInsumo(DateTime fecha)
        {
            connection();
            List<Gastos> ListadoGasto = new List<Gastos>();

            SqlCommand cmd = new SqlCommand("BuscarCompraInsumo", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fecha", fecha);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoGasto.Add(
                    new Gastos
                    {
                        idGasto = Convert.ToInt32(dr["idGasto"]),
                        FechaGasto = Convert.ToDateTime(dr["Fecha"]),
                        //Total = Convert.ToInt32(dr["total"]),
                        Usuario = Convert.ToString(dr["Usuario"]),
                    });
            }
            return ListadoGasto;
        }

        public bool EsGastoRepetido(String gasto)
        {
            connection();
            SqlCommand cmd = new SqlCommand("EsGastoRepetido", con);
            cmd.Parameters.AddWithValue("@nombre", gasto);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        public List<Gastos> BuscarCompraBazar(DateTime fecha, DateTime fecha2)
        {
            connection();
            List<Gastos> ListadoGasto = new List<Gastos>();
            // fecha2 = fecha2.AddHours(23).AddMinutes(59).AddSeconds(59);

            SqlCommand cmd = new SqlCommand("BuscarCompraBazar", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fecha", fecha);
            cmd.Parameters.AddWithValue("@fecha2", fecha2);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                ListadoGasto.Add(
                    new Gastos
                    {
                        Nombre = Convert.ToString(dr["Nombre"]),
                        Tipo = Convert.ToString(dr["Tipo"]),
                        idGasto = Convert.ToInt32(dr["idGasto"]),
                        FechaGasto = Convert.ToDateTime(dr["Fecha"]),
                        Total = Convert.ToDecimal(dr["total"]),
                        Usuario = Convert.ToString(dr["Usuario"]),
                    });
            }
            return ListadoGasto;
        }


        public bool PagarGastoConNuevoTipo(string gasto, decimal monto, string usuario, string nuevotipo, DateTime fecha)
        {
            GastosHandle gh = new GastosHandle();
            connection();
            SqlCommand cmd = new SqlCommand("AgregarTipoGasto2", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@tipo", nuevotipo);
            cmd.Parameters.Add("@lastId", SqlDbType.Int).Direction = ParameterDirection.Output;
            con.Open();

            int i = cmd.ExecuteNonQuery();
            int idtipogasto = Convert.ToInt32(cmd.Parameters["@lastId"].Value);
            con.Close();

            if (i >= 1)
            {
                gh.PagarGasto(usuario, idtipogasto, monto, gasto, fecha);
                return true;
            }

            else
                return false;
        }


        public List<Gastos> listaCuentas(DateTime fecha2, DateTime fecha1)
        {
            connection();

            List<Gastos> listaCuenta = new List<Gastos>();
            SqlCommand cmd = new SqlCommand("listaCuentas", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fecha2", fecha2);
            cmd.Parameters.AddWithValue("@fecha1", fecha1);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                listaCuenta.Add(
                    new Gastos
                    {
                        idGasto = Convert.ToInt32(dr["idGasto"]),
                        idTipoGasto = Convert.ToInt32(dr["idTipoGasto"]),
                        Tipo = Convert.ToString(dr["Tipo"]),
                        Nombre = Convert.ToString(dr["Nombre"]),
                        FechaGasto = Convert.ToDateTime(dr["Fecha"]),
                        Total = Convert.ToDecimal(dr["Monto"]),
                        Usuario = Convert.ToString(dr["Usuario"]),
                    });
            }
            return listaCuenta;
        }

        public void borrarCuenta(int idgasto)
        {
            connection();

            SqlCommand cmd = new SqlCommand("borrarCuenta", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idgasto", idgasto);

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

    }
}