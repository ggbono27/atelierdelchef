﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_1.Models
{
    public class Gastos
    {
        public int idGasto { get; set; }
        public int idTipoGasto { get; set; }
        public String Tipo { get; set; }
        public DateTime FechaGasto { get; set; }
        public String Nombre { get; set; }

        public String Usuario { get; set; }

        public Decimal Total { get; set; }

        public Gastos()
        { }

        public Gastos(int idgasto, String tipo, DateTime fechagasto, Decimal total, String nombre, String usuario, int idtipogasto)
        {
            idGasto = idgasto;
            Tipo = tipo;
            FechaGasto = fechagasto;
            Total = total;
            Nombre = nombre;
            Usuario = usuario;
        }

        public Gastos(DateTime fechagasto, string tipo, String usuario)
        {
            Tipo = tipo;
            FechaGasto = fechagasto;
            Usuario = usuario;
        }

        public Gastos(string nombre, int idTipoG, string tipo)
        {
            Nombre = nombre;
            idTipoGasto = idTipoG;
            Tipo = tipo;
        }
    }
}