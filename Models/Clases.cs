﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Proyecto_1.Models
{
    public class Clases
    {
        public int idClase { get; set; } 
        public int idCurso { get; set; }
        public int idTurno { get; set; }

        [System.ComponentModel.DisplayName("Fecha inicio de clase:"),
         DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.DateTime)]
        [Required]
        public DateTime FechaInicio { set; get; }     //esta variable se usa como la fecha de la clase. 
        public DateTime FechaFin { get; set; }

        [System.ComponentModel.DisplayName("Descripción:")]
        public String Descripcion { set; get; }

        [System.ComponentModel.DisplayName("Fecha inicio de clase:")]
        [Required]
        public String fecha { get; set; }

        public Clases()
        { }

        public Clases(int idC, int idCur, DateTime FI, DateTime FF, String Desc, int idturno)
        {
            idTurno = idturno;
            idClase = idC;
            idCurso = idCur;
            FechaInicio = FI;
            FechaFin = FF;
            Descripcion = Desc;
            fecha = FI.ToShortDateString();
        }


        public Clases(int idturno)
        {
            idTurno = idturno;
        }
    }
}