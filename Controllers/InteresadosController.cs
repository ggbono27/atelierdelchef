﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_1.Models;

namespace Proyecto_1.Controllers
{
    public class InteresadosController : Controller
    {
        private static string _nombreCurso;
        private static int _idcurso;
        private static String _mensajeError;
        private static String _mensajeExito;
        // GET: Interesados
        public ActionResult Index()
        {
            return View();
        }

        // GET: Interesados/Details/5
        public ActionResult EditarInteresado(int idinteresado, int idCurso)
        {
            CursosHandle ch = new CursosHandle();
            InteresadosHandle ih = new InteresadosHandle();
            try {
                var curso= ch.ListarCursos();
                ViewBag.listaCursos = curso;
                ViewBag.idCurso = curso.Find(x=> x.idCurso== idCurso).idCurso;
                ViewBag.nombreCurso = curso.Find(x => x.idCurso == idCurso).Nombre;
                return View(ih.ListarInteresados().Find(x => x.idInteresado == idinteresado));
            }
            catch (Exception e) {
                _mensajeError = e.Message;
                return RedirectToAction("Interesados");
            }
        }

        // GET: Interesados/Create
        public ActionResult NuevoInteresado()
        {
            CursosHandle ch = new CursosHandle();
            try {
                ViewBag.listadoCursos = ch.ListarCursos();
                return View();
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return View();
            }
           
        }

        // POST: Interesados/Create
        [HttpPost]
        public ActionResult CrearInteresado(int idCurso, string Nombre, string Telefono, string usuario, string observacion="")
        {
            
            InteresadosHandle ih = new InteresadosHandle();
            try
            {
                ih.AgregarInteresado(idCurso, Nombre, Telefono, usuario, observacion);
              _mensajeExito = "interesado agregado exitosamente";
                return RedirectToAction("Interesados", "Interesados");
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return View("NuevoInteresado");
            }
        }

        public ActionResult Interesados()
        {
            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }

            _mensajeExito = "";
            _mensajeError = "";

            CursosHandle ch = new CursosHandle();
            InteresadosHandle ih = new InteresadosHandle();
            // _idcurso = idcurso;
            //  _nombreCurso = nombre;
            try
            {
                ViewBag.listadoCursos = ch.ListarCursos();
                return View("ListaInteresados", ih.ListarInteresados());
            } catch (Exception e) {
                _mensajeError = e.Message;
                return View();
            }
        }


       // [HttpPost]
        public ActionResult FiltroPorCurso(int idCurso)
        {
            CursosHandle ch = new CursosHandle();
            InteresadosHandle ih = new InteresadosHandle();
            try {
                ViewBag.listadoCursos = ch.ListarCursos();
                return View("ListaInteresados", ih.InteresadosDeCurso(idCurso));
            }
            catch (Exception e) {
                _mensajeError = e.Message;
                return View("ListaInteresados", ih.ListarInteresados());
            }
        }

        // GET: Interesados/Edit/5
        public ActionResult Edit(int idInteresado)
        {
            return View();
        }


        public ActionResult BorrarInteresadoDeCurso(int idinteresado, int idcurso)
        {
            InteresadosHandle ih = new InteresadosHandle();
            try
            {
                CursosHandle ch = new CursosHandle();
                ih.BorrarInteresadoDeCurso(idinteresado, idcurso);
                _mensajeExito = "Interesado eliminado correctamente";
            }

            catch (Exception e)
            {
                _mensajeError = e.Message;
            }
            return RedirectToAction("Interesados");
        }

        // POST: Interesados/Edit/5
        [HttpPost]
        public ActionResult EditI(Interesados interesado, int idCurso2)
        {
            if (interesado.Observacion == null) interesado.Observacion = "";
            InteresadosHandle ih = new InteresadosHandle();
            try
            {
                ih.EditInteresado(interesado, idCurso2);
               _mensajeExito = "Interesado editado exitosamente";
                return RedirectToAction("Interesados");
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Interesados");
            }
        }

    }
}
