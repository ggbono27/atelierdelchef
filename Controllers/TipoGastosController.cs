﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_1.Models;
using System.IO;

namespace Proyecto_1.Controllers
{
    public class TipoGastosController : Controller
    {
        private static String _mensajeError;
        private static String _mensajeExito;
        // GET: TipoGastos
        public ActionResult Index()
        {
            return View();
        }

        // GET: TipoGastos/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: TipoGastos/Create
        public ActionResult CrearTipoDeGasto_form()
        {
            return View();
        }

        // POST: TipoGastos/Create
        [HttpPost]
        public ActionResult NuevoTipoGasto(TipoGastos tp)
        {
            try
            {
                TipoGastosHandle tgh = new TipoGastosHandle();
                tgh.AgregarTipoGasto(tp);

                return RedirectToAction("ListaCuentas","Productos");
            }
            catch(Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return View("CrearTipoGasto_form");
            }
        }

        // GET: TipoGastos/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: TipoGastos/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: TipoGastos/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: TipoGastos/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

    }
}
