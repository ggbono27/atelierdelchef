﻿using System;
using System.Linq;
using System.Web.Mvc;
using Proyecto_1.Models;
using System.Collections.Generic;

namespace Proyecto_1.Controllers
{
    public class RecetasController : Controller
    {
        private static String _mensajeExito;
        private static String _mensajeError;
        private static string _receta;
        private static int _idReceta = 0;
        private static IList<Recetas> _listRecetas;
        private static string _nombreReceta;
        private static string _preparacionReceta;

        public ActionResult Receta()
        {
            RecetasHandle rh = new RecetasHandle();
            ViewBag.listaRecetas = rh.ListarRecetas().ToList();
            _idReceta = 0;
            return View();
        }

        public ActionResult Receta2()
        {
            RecetasHandle rh = new RecetasHandle();
            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }
            _mensajeError = "";
            _mensajeExito = "";
           ViewBag.listaReceta = rh.BuscarReceta2("");

            return View();
        }

        public ActionResult TraerReceta(string idreceta)
        {
            RecetasHandle rh = new RecetasHandle();
            _idReceta = Convert.ToInt32(idreceta);
            ViewBag.control = 2;
            try
            {
                // var receta = rh.TraerReceta(_idReceta).Find(x => x.idReceta == _idReceta);
                var receta = rh.TraerReceta(_idReceta).FirstOrDefault();
                _receta = receta.Nombre;
                ViewBag.mensajeError = _mensajeError;
                ViewBag.control = 2;
                return View("EditReceta", receta);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Receta2");
            }
        }

        public ActionResult Receta2_1(string idReceta)
        {
            _idReceta = Convert.ToInt32(idReceta);
            RecetasHandle rh = new RecetasHandle();

            try
            {
                return View("Receta", rh.TraerReceta(_idReceta));
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return View("Receta");
            }
           
        }

        public ActionResult CrearReceta()
        {
            ProductosHandle ph = new ProductosHandle();
            ViewBag.listaProd = ph.ListarInsumos().ToList();
            return View();
        }

        [HttpPost]
        public ActionResult BuscarReceta2(string cadena)
        {
            RecetasHandle rh = new RecetasHandle();

            try
            {
                _listRecetas= rh.BuscarReceta2(cadena);
                if (_listRecetas.Count()==0)
                {
                    ViewBag.mensajeError = "No exite la receta buscada";
                }
                ViewBag.listaReceta = _listRecetas;
                return View("Receta2");
            }

            catch (Exception e)
            {
                _mensajeError = e.Message;
                return View("Receta2");
            }
        }

        public ActionResult CrearReceta2()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AgregarReceta2(Recetas receta)
        {
            if (receta.Tips == null)
            {
                receta.Tips = "";
            }

            RecetasHandle rh = new RecetasHandle();
            try
            {
                if (!rh.EsRepetido(receta.Nombre))
                {
                    if (ModelState.IsValid)
                    {
                        rh.AgregarReceta2(receta);
                        _mensajeExito = "Receta agregada exitosamente";
                        return RedirectToAction("Receta2", "Recetas");
                    }
                    return RedirectToAction("Receta2");
                }

                else
                {
                    ViewBag.mensajeError = "Ya existe una receta con ese nombre";
                    return View("CrearReceta2");
                }
            }

            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Receta2", "Recetas");
            }
        }

        //este metodo agrega la receta a la db, y recupera el id y luego va guardando ingrediente por ingrediente.
        public ActionResult AgregarReceta(Recetas receta, float cantidad = 0, int idProducto = 0, string unidad = "")
        {
            _mensajeError = "";
            _mensajeExito = "";
            RecetasHandle rh = new RecetasHandle();
            ProductosHandle ph = new ProductosHandle();
            ViewBag.listaProd = ph.ListarInsumos().ToList();

            if (_idReceta == 0) //se fija si es la primera vez que se hace click en agregar, si es asi, guarda la receta por primera vez y recupera el id.
            {
                if (!rh.EsRepetido(receta.Nombre))
                {
                    try
                    {
                        if (!rh.AgregarReceta(receta))
                        {
                            _nombreReceta = receta.Nombre;
                            _preparacionReceta = receta.Preparacion;
                            _mensajeExito = "Receta agregada exitosamente";
                            _idReceta = rh.RecuperarIdReceta();
                            ModelState.Clear();

                            Preparaciones prep = new Preparaciones(_idReceta, idProducto, cantidad, unidad);
                            PreparacionesHandle preph = new PreparacionesHandle();

                            if (!preph.AgregarPreparacion(prep))
                            {
                                _mensajeExito = "Ingrediente agregado exitosamente";
                                ModelState.Clear();
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _mensajeError = e.Message;
                    }
                }
                else
                {
                    if (receta.Nombre != null)
                        ViewBag.mensajeErrorRepetido = "Ya existe una receta con el nombre " + receta.Nombre;
                    else
                        ViewBag.mensajeErrorRepetido = "Debe ingresar un nombre para la receta";
                    return View("CrearReceta");
                }
            }

            else
            {
                Preparaciones prep = new Preparaciones(_idReceta, idProducto, cantidad, unidad);
                PreparacionesHandle preph = new PreparacionesHandle();
                try
                {
                    if (!preph.AgregarPreparacion(prep))
                    {
                        _mensajeExito = "Ingrediente agregado exitosamente";
                        ModelState.Clear();
                    }
                }
                catch (Exception e)
                {
                    _mensajeError = "El ingrediente ya se encuentra en la receta";
                }
            }

            ViewBag.nombreReceta = receta.Nombre;
            ViewBag.preparacionReceta = receta.Preparacion;
            ViewBag.listaIngredientes = rh.ListarIngredientes(_idReceta).ToList();
            ViewBag.mensajeExito = _mensajeExito;
            ViewBag.mensajeError = _mensajeError;
            return View("CrearReceta");
        }

        [HttpPost]
        public ActionResult EditarReceta2(Recetas receta)
        {
            if (receta.Tips == null)
            {
                receta.Tips = "";
            }

            RecetasHandle rh = new RecetasHandle();
            try
            {
                if (ModelState.IsValid)
                {
                    if (rh.EsRepetido(receta.Nombre) && receta.Nombre!= _receta)
                    {
                     // _mensajeError = "Ya existe una receta con ese nombre";
                        ViewBag.mensajeError= "Ya existe una receta con ese nombre";
                        return View("EditReceta", rh.TraerReceta(receta.idReceta).Find(x => x.idReceta == receta.idReceta));
                       // return RedirectToAction ("TraerReceta", "Recetas", new {idreceta = _idReceta.ToString() });
                    }

                    rh.EditarReceta2(receta);
                    _mensajeExito = "Receta modificada exitosamente";
                    return RedirectToAction("Receta2", "Recetas");
                }

                ViewBag.mensajeError = "Ocurrio un error al modificar, vuelva a intentar";
                return View("Receta2");

            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Receta2");
            }
        }

        public ActionResult EditarReceta(Recetas receta, float cantidad = 0, int idProducto = 0, string unidad ="")
        {
            _mensajeError = "";
            _mensajeExito = "";
            _idReceta = receta.idReceta;

            RecetasHandle rh = new RecetasHandle();
            ProductosHandle ph = new ProductosHandle();
            try
            {
                if (_nombreReceta != receta.Nombre)
                {
                    if (rh.EsRepetido(receta.Nombre))
                    {
                        if (_nombreReceta != null)
                            _mensajeError = "Ya existe una receta con el nombre " + receta.Nombre;
                        else
                            _mensajeError = "Debe ingresar un nombre para la receta";

                        ViewBag.listaProd = ph.ListarInsumos().ToList();

                        ViewBag.listaRecetas = rh.ListarRecetas().ToList();
                        ViewBag.listaIngredientes = rh.ListarIngredientes(_idReceta).ToList();
                        ViewBag.mensajeExito = _mensajeExito;
                        ViewBag.mensajeError = _mensajeError;
                        return View("Receta", rh.ListarRecetas().Find(x => x.idReceta == _idReceta));
                    }
                }

                if (!rh.EditarReceta(receta))
                {
                    _mensajeExito = "Receta modificada exitosamente";
                    ModelState.Clear();
                }
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
            }

            var producto = rh.ListarIngredientes(_idReceta).Find(x => x.idProducto == idProducto);   //busca si el producto q viene ya estaba en la preparacion. "producto" toma valor nulo si no es asi.

            if (producto == null)
            {
                PreparacionesHandle preph = new PreparacionesHandle();
                Preparaciones prep = new Preparaciones(_idReceta, idProducto, cantidad, unidad);
                try
                {
                    if (!preph.AgregarPreparacion(prep))
                    {
                        _mensajeExito = "Ingrediente agregado exitosamente";
                    }
                }
                catch (Exception e)
                {
                    _mensajeError = e.Message;
                }
            }
            else if (_nombreReceta == receta.Nombre && _preparacionReceta == receta.Preparacion)
            {
                _mensajeError = "El ingrediente ya se encuentra en la receta";
                _mensajeExito = "";
            }
            _nombreReceta = receta.Nombre;
            _preparacionReceta = receta.Preparacion;

            ViewBag.listaProd = ph.ListarInsumos().ToList();
            ViewBag.listaRecetas = rh.ListarRecetas().ToList();
            ViewBag.listaIngredientes = rh.ListarIngredientes(_idReceta).ToList();

            ViewBag.mensajeExito = _mensajeExito;
            ViewBag.mensajeError = _mensajeError;

            return View("Receta", rh.ListarRecetas().Find(x => x.idReceta == _idReceta));
        }

        public ActionResult QuitarIngrediente(int idProducto)
        {
            try
            {
                PreparacionesHandle preph = new PreparacionesHandle();
                if (!preph.QuitarIngrediente(idProducto, _idReceta))
                {
                    ViewBag.mensajeExito = "Ingrediente quitado exitosamente";
                }
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
            }

            ViewBag.nombreReceta = _nombreReceta;
            ViewBag.preparacionReceta = _preparacionReceta;

            ProductosHandle ph = new ProductosHandle();
            ViewBag.listaProd = ph.ListarInsumos().ToList();

            RecetasHandle rh = new RecetasHandle();
            ViewBag.listaIngredientes = rh.ListarIngredientes(_idReceta).ToList();

            return View("CrearReceta");
        }

        public ActionResult QuitarIngredienteEnEdicion(int idProducto)
        {
            try
            {
                PreparacionesHandle preph = new PreparacionesHandle();
                if (!preph.QuitarIngrediente(idProducto, _idReceta))
                {
                    ViewBag.mensajeExito = "Ingrediente quitado exitosamente";
                }
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
            }

            ProductosHandle ph = new ProductosHandle();
            ViewBag.listaProd = ph.ListarInsumos().ToList();

            RecetasHandle rh = new RecetasHandle();
            ViewBag.listaRecetas = rh.ListarRecetas().ToList();
            ViewBag.listaIngredientes = rh.ListarIngredientes(_idReceta).ToList();

            return View("Receta", rh.ListarRecetas().Find(x => x.idReceta == _idReceta));
        }

        public ActionResult Imprimir()
        {
            RecetasHandle rh = new RecetasHandle();

            ViewBag.listaIngredientes = rh.ListarIngredientes(_idReceta);
            var receta = rh.ListarRecetas().Find(x => x.idReceta == _idReceta);
         
            _nombreReceta = receta.Nombre;
            ViewBag.nombre = _nombreReceta;
            _preparacionReceta = receta.Preparacion;
            ViewBag.preparacion = receta.Preparacion;
            return View();
        }

        public ActionResult Imprimir2(int idreceta)
        {
            RecetasHandle rh = new RecetasHandle();
            try
            {
                var receta = rh.ListarRecetas2().Find(x => x.idReceta == idreceta);
                ViewBag.Ingredientes = receta.Ingredientes;
                ViewBag.Preparacion = receta.Preparacion;
                ViewBag.control = 1;
                return View("Imprimir3", receta);
            }

            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Receta2","Recetas");
            }
        }

        [HttpPost]
        public ActionResult BuscarReceta(string cadena)
        {
            RecetasHandle rh = new RecetasHandle();
            try
            {
                ViewBag.listaReceta = rh.BuscarReceta(cadena);
                return View("Receta");
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Receta");
            }
        }
    }
}
