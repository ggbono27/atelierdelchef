﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AtelierChef1.Models;
using Proyecto_1.Models;
using Proyecto_1.ViewModels;

namespace Proyecto_1.Controllers
{
    public class UsuariosController : Controller
    {
        private static String _mensajeExito;
        private static String _mensajeError;
        private static String _mensaje;
        // GET: Usuarios
        public ActionResult login()
        {

            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }

            _mensajeError = "";
            _mensajeExito = "";

            try
            {
                return View();

            }
            catch (Exception e)
            {
                return RedirectToAction("", "");
            }

        }

        public ActionResult Registrar(string username, string password)
        {

            System.Security.Cryptography.RNGCryptoServiceProvider rng = new System.Security.Cryptography.RNGCryptoServiceProvider();
            byte[] saltBytes = new byte[36];
            rng.GetBytes(saltBytes);
            string salt = Convert.ToBase64String(saltBytes);

            byte[] passwordAndSaltBytes = System.Text.Encoding.UTF8.GetBytes(password + salt);
            byte[] hashBytes = new System.Security.Cryptography.SHA256Managed().ComputeHash(passwordAndSaltBytes);
            string hashString = Convert.ToBase64String(hashBytes);

            UsuariosHandle uh = new UsuariosHandle();
            try
            {
                uh.createUser(username, hashString, salt);
                return RedirectToAction("cuentas", "gastos");
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("login");
            }

        }


        public ActionResult LogInClick_Click(string username, string password)
        {
            UsuariosHandle uh = new UsuariosHandle();
            try
            {
                var usuario = uh.getUser(username);
                if (usuario.Count == 0)
                {
                    ViewBag.mensajeError = "el usuario o contraseña ingresados son incorrectos";
                    return View("login");
                }
                else
                {
                    var pwd_db = usuario[0].Pass.ToString();
                    var salt = usuario[0].Salt.ToString();
                    byte[] passwordAndSaltBytes = System.Text.Encoding.UTF8.GetBytes(password + salt);
                    byte[] hashBytes = new System.Security.Cryptography.SHA256Managed().ComputeHash(passwordAndSaltBytes);
                    string hashString = Convert.ToBase64String(hashBytes);
                    if (hashString == pwd_db)
                    {
                        // AQUI HACER LA AUTENTICACION DE SESION PARA ESE USUARIO Y REDIRIGIR AL CALENDARIO
                       // FormsAuthentication.RedirectFromLoginPage(username, true);
                        return RedirectToAction("", "");
                    }
                    else
                    {
                        ViewBag.mensajeError = "error de autenticacion";
                        return View("login");
                       
                    }
                }
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("login");
            }

            /*
            string mystr = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
            SqlConnection myConn = new SqlConnection(mystr);
            myConn.Open();
            string sqlStr = @"Select UserName, PasswordHashed, Salt FROM [User] WHERE UserName=@UserName";
            SqlCommand cmd = new SqlCommand(sqlStr, myConn);
            cmd.Parameters.AddWithValue("@UserName", username);
            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            //string pwd_db = dt.Rows[0]["PasswordHashed"].ToString();
            // string salt = dt.Rows[0]["Salt"].ToString();
            // byte[] passwordAndSaltBytes = System.Text.Encoding.UTF8.GetBytes(password + salt);
            //byte[] hashBytes = new System.Security.Cryptography.SHA256Managed().ComputeHash(passwordAndSaltBytes);
            //string hashString = Convert.ToBase64String(hashBytes);
            */
        }

        // GET: Usuarios/Details/5
        /*
        public ActionResult login(string user, string pass)
        {
            UsuariosHandle uh = new UsuariosHandle();
            try
            {
                //var x = uh.login(user);
                if (x.Find(y => y.UserName == user).UserName == user && x.Find(y => y.UserName == user).Pass == pass)
                {

                }
            }
            catch (Exception e)
            {
                _mensaje = e.Message;
            }
            return View();
        }*/

        // GET: Usuarios/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Usuarios/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Usuarios/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Usuarios/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Usuarios/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Usuarios/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
