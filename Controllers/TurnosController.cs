﻿using System;
using System.Web.Mvc;
using Proyecto_1.Models;
using System.Linq;
using Proyecto_1.ViewModels;
using System.Collections.Generic;
using System.Data.SqlClient;
using AtelierChef1.ViewModels;
using System.Web.Configuration;

namespace Proyecto_1.Controllers
{
    public class TurnosController : Controller
    {
        private static String _mensajeExito;
        private static String _mensajeError;
        private static int _idCurso;
        private static int _idTurno;
        private static int _idClase;
        public static int _idAlumno;
        private static String _nombreCurso;
        private static String _nombreTurno;
        private static String _fechaTurno;
        private static string _horaInicioTurno;
        private static string _horaFinTurno;
        private static string _fechaClase;
        private static DateTime _fechaClaseDateTime;
        private static string _horaInicioClase;
        private static string _horaFinClase;
        private static IList<Alumno> _lista2;
        private static Decimal _total;
        private static string _mensaje;
        private static int _control;
        private static int _control2 = 0;
        private static string _cultura = WebConfigurationManager.AppSettings["cultura"]; //COMENTARIO22222


        public ActionResult Index()
        {
            return View();
        }
        public void setMensaje(string mensaje)
        {
            _mensajeExito = mensaje;
        }

        public ActionResult prueba(string cantidad, string precio, string idProducto)
        {
            var x = precio;
            return View();
        }

        // recibe id y nombre del curso del cual se muestran los turnos
        public ActionResult Turno(int id, String Nombre)
        {
            try
            {
                _idCurso = id;
                _nombreCurso = Nombre;
                TurnosHandle th = new TurnosHandle();
                ModelState.Clear();

                if (_mensajeExito != null)
                {
                    ViewBag.mensajeExito = _mensajeExito;
                }

                if (_mensajeError != null)
                {
                    ViewBag.mensajeError = _mensajeError;
                }

                _control = 1;
                _mensajeExito = "";
                _mensajeError = "";
                ViewBag.nombreCurso = Nombre;
                ViewBag.control2 = 0;
                return View(th.ListarTurnosDeUnCurso(id));
            }
            catch (Exception e)
            {
                CursosController cc = new CursosController();
                cc.SetMensajeError(e.Message);
                return RedirectToAction("Curso", "Cursos");
            }
        }

        //Este action se lo usa cuando se hace click en el boton "guardar" de la vista "CrearTurno" para retornar a la vista "Turno" (primero pasa por el action "agregar" que es quien lo llama)
        //Difiere del action anterior en que no recibe parametros, puesto q estos ya estan guardados en variables estaticas.
        public ActionResult Turno2()
        {
            TurnosHandle th = new TurnosHandle();
            ModelState.Clear();

            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }

            _mensajeExito = "";
            _mensajeError = "";
            ViewBag.nombreCurso = _nombreCurso;
            ViewBag.control2 = 0;
            return View("Turno", th.ListarTurnosDeUnCurso(_idCurso));
        }

        //Este action se lo usa al hacer click en el checkbox de la vista turno, para listar todos los turnos.
        public ActionResult Turno3()
        {
            TurnosHandle th = new TurnosHandle();
            ModelState.Clear();

            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }

            _mensajeExito = "";
            _mensajeError = "";
            ViewBag.nombreCurso = _nombreCurso;

            ViewBag.control2 = 1;
            return View("Turno", th.ListarTurnosDeUnCursoSinRestriccion(_idCurso));
        }

        public ActionResult CrearTurno()
        {
            ViewBag.nombre = _nombreCurso;
            return View("CrearTurno");
        }

        public ActionResult CrearClases()
        {
            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }
            CursosHandle cuh = new CursosHandle();
            TurnosHandle th = new TurnosHandle();
            ClasesHandle ch = new ClasesHandle();
            try
            {
                RecetasHandle rh = new RecetasHandle();
                ViewBag.listaRecetas = rh.ListarRecetas();
                ViewBag.listaAlumnos = th.AlumnosInscriptos2(_idTurno, _idCurso);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
            }

            ViewBag.idturno = _idTurno;
            ViewBag.fecha = _fechaTurno;
            ViewBag.nombreCurso = _nombreCurso;
            ViewBag.nombreTurno = _nombreTurno;
            ViewBag.mensajeExito = _mensajeExito;
            ViewBag.mensajeError = _mensajeError;

            _mensajeError = "";
            _mensajeExito = "";

            List<ClasesConRecetas> ListaClasesConRecetas = new List<ClasesConRecetas>();
            var ListaClasesDeUnTurno = ch.ListarClasesDeUnTurno(_idTurno);
            var ListaRecetasDeUnTurno = ch.ListarRecetasDeTurno(_idTurno);

            foreach (var item in ListaClasesDeUnTurno)
            {
                var ccr = new ClasesConRecetas(item.idClase, item.idCurso, item.FechaInicio, item.FechaFin, item.Descripcion);
                ListaClasesConRecetas.Add(ccr);
            }

            var i = 0;
            var nombres = "";
            foreach (var item in ListaRecetasDeUnTurno)
            {
                if (i == item.idClase)
                {
                    nombres = nombres + " - " + item.Nombre;
                    ListaClasesConRecetas.Find(x => x.idClase == item.idClase).receta = nombres;
                }
                else
                {
                    ListaClasesConRecetas.Find(x => x.idClase == item.idClase).receta = item.Nombre;
                    nombres = item.Nombre;
                }
                i = item.idClase;
            }

            return View("CrearClases", ListaClasesConRecetas);
        }

        public ActionResult CrearClases2(string id) //recibe idTurno y manda la vista para la creacion de las clases  y donde se listan todas las clases y alumnos. vista despues del calendario.
        {
            _idTurno = Convert.ToInt32(id);
            CursosHandle cuh = new CursosHandle();
            TurnosHandle th = new TurnosHandle();
            if (_idCurso == 0)
            {
                _idCurso = th.TraeridCurso(_idTurno).FirstOrDefault().idCurso;
            }
            var t1 = th.ListarTurnosDeUnCursoSinRestriccion(_idCurso).Find(y => y.idTurno == _idTurno);
            _nombreTurno = t1.Dia.ToUpper() + " de " + t1.Inicio + " a " + t1.Fin;

            ClasesHandle ch = new ClasesHandle();
            try
            {
                _fechaTurno = ch.ListarClasesDeUnTurno(_idTurno).Max(t => t.FechaInicio).AddDays(7).ToString("dd/MM/yyyy");
            }
            catch (Exception e)
            {
                _fechaTurno = t1.FechaInicio.ToString("dd/MM/yyyy");
            }

            try
            {
                RecetasHandle rh = new RecetasHandle();
                ViewBag.listaRecetas = rh.ListarRecetas();
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
            }

            ViewBag.idTurno = _idTurno;
            ViewBag.fecha = _fechaTurno;
            ViewBag.nombreCurso = _nombreCurso;
            ViewBag.nombreTurno = _nombreTurno;
            ViewBag.idCurso = _idCurso;
            ViewBag.mensajeExito = _mensajeExito;
            ViewBag.mensajeError = _mensajeError;
            _mensajeError = "";
            _mensajeExito = "";

            List<ClasesConRecetas> ListaClasesConRecetas = new List<ClasesConRecetas>();
            var ListaClasesDeUnTurno = ch.ListarClasesDeUnTurno(_idTurno);
            var ListaRecetasDeUnTurno = ch.ListarRecetasDeTurno(_idTurno);

            foreach (var item in ListaClasesDeUnTurno)
            {
                var ccr = new ClasesConRecetas(item.idClase, item.idCurso, item.FechaInicio, item.FechaFin, item.Descripcion, item.idTurno);
                ListaClasesConRecetas.Add(ccr);
            }

            var i = 0;
            var nombres = "";
            foreach (var item in ListaRecetasDeUnTurno)
            {
                if (i == item.idClase)
                {
                    nombres = nombres + " - " + item.Nombre;
                    ListaClasesConRecetas.Find(x => x.idClase == item.idClase).receta = nombres;
                }
                else
                {
                    ListaClasesConRecetas.Find(x => x.idClase == item.idClase).receta = item.Nombre;
                    nombres = item.Nombre;
                }

                i = item.idClase;
            }
            //var costo = cuh.TraerCostoCurso(_idCurso).FirstOrDefault().Costo;
            //var sumaPago = th.DetallePagoAlumno(_idTurno, id).Sum(x => x.Monto);
            //var Adeuda = costo - sumaPago;

            //ViewBag.deuda = Adeuda;
            ViewBag.control = _control;
            ViewBag.listaAlumnos = th.AlumnosInscriptos2(_idTurno, _idCurso);
            return View("CrearClases", ListaClasesConRecetas);
        }

        public ActionResult ClasesDeTurnoCalendario(int id) //recibe el id del turno para listar sus clases
        {
            TurnosHandle th = new TurnosHandle();
            CursosHandle ch = new CursosHandle();
            try
            {

                var idcurso = th.TraeridCurso(id).Max().idCurso;
                _idCurso = idcurso;
                _nombreCurso = ch.ListarCursos().Find(x => x.idCurso == idcurso).Nombre;
                return RedirectToAction("CrearClases2", "Turnos", new { id = id });
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return View("FullCalendar");
            }

        }
        public ActionResult ImprimirDiploma()
        {
            ViewBag.nombreCurso = "sushi 8";
            return View();
        }

        [HttpPost]
        public ActionResult AgregarClase(Clases c, int idReceta = 0, int idReceta2 = 0, int idReceta3 = 0, int idReceta4 = 0, int idReceta5 = 0, int idReceta6 = 0, int idReceta7 = 0, int idReceta8 = 0)
        {
            c.idTurno = _idTurno;
            c.idCurso = _idCurso;

            var f1 = c.fecha.Split('/');
            var dd = f1[0];
            var mm = f1[1];
            var yy = f1[2];
            var fecha2 = mm + "/" + dd + "/" + yy;
            c.FechaInicio = Convert.ToDateTime(fecha2);


            if (c.Descripcion == null)
                c.Descripcion = " - ";

            //si se crea la clase directamente despues de crear el turno, entonces las variables privadas estaticas sirven. Sino hay que recuperar hora de inicio y fin del turno desde la DB
            if (_horaInicioTurno != null)
            {
                var horaInicio = Convert.ToDateTime(_horaInicioTurno);
                c.FechaInicio = new DateTime(c.FechaInicio.Year, c.FechaInicio.Month, c.FechaInicio.Day, horaInicio.Hour, horaInicio.Minute, horaInicio.Second);
                var horaFin = Convert.ToDateTime(_horaFinTurno);
                c.FechaFin = new DateTime(c.FechaInicio.Year, c.FechaInicio.Month, c.FechaInicio.Day, horaFin.Hour, horaFin.Minute, horaFin.Second);
            }
            else
            {
                TurnosHandle th = new TurnosHandle();
                var horaInicio = Convert.ToDateTime(th.ListarTurnosDeUnCurso(_idCurso).Find(t => t.idTurno == _idTurno).Inicio);
                c.FechaInicio = new DateTime(c.FechaInicio.Year, c.FechaInicio.Month, c.FechaInicio.Day, horaInicio.Hour, horaInicio.Minute, horaInicio.Second);
                var horaFin = Convert.ToDateTime(th.ListarTurnosDeUnCurso(_idCurso).Find(t => t.idTurno == _idTurno).Fin);
                c.FechaFin = new DateTime(c.FechaInicio.Year, c.FechaInicio.Month, c.FechaInicio.Day, horaFin.Hour, horaFin.Minute, horaFin.Second);
            }
            _fechaTurno = c.FechaInicio.AddDays(7).ToString("dd/MM/yyyy");

            ClasesHandle ch = new ClasesHandle();
            try
            {
                var listita = ch.ListarClasesDeUnaFecha(c.FechaInicio, c.FechaFin, _idTurno);
                if (ch.ListarClasesDeUnaFecha(c.FechaInicio, c.FechaFin, _idTurno).Count() == 0)
                {
                    if (!ch.AgregarClase(c))
                    {
                        var idClase = ch.RecuperarIdClase();
                        if (idReceta != 0)
                        {
                            ch.AsignarRecetaClase(idReceta, _idCurso, _idTurno, idClase);
                        }
                        if (idReceta2 != 0)
                        {
                            ch.AsignarRecetaClase(idReceta2, _idCurso, _idTurno, idClase);
                        }
                        if (idReceta3 != 0)
                        {
                            ch.AsignarRecetaClase(idReceta3, _idCurso, _idTurno, idClase);
                        }
                        if (idReceta4 != 0)
                        {
                            ch.AsignarRecetaClase(idReceta4, _idCurso, _idTurno, idClase);
                        }
                        if (idReceta5 != 0)
                        {
                            ch.AsignarRecetaClase(idReceta5, _idCurso, _idTurno, idClase);
                        }
                        if (idReceta6 != 0)
                        {
                            ch.AsignarRecetaClase(idReceta6, _idCurso, _idTurno, idClase);
                        }
                        if (idReceta7 != 0)
                        {
                            ch.AsignarRecetaClase(idReceta7, _idCurso, _idTurno, idClase);
                        }
                        if (idReceta8 != 0)
                        {
                            ch.AsignarRecetaClase(idReceta8, _idCurso, _idTurno, idClase);
                        }
                        _mensajeExito = "Clase agregada exitosamente";
                        ModelState.Clear();
                    }
                }
                else
                {
                    _mensajeError = "Ya existe una clase en ese horario";
                }

                return RedirectToAction("CrearClases2", "Turnos", new { id = _idTurno.ToString() });
                //RecetasHandle rh = new RecetasHandle();
                //ViewBag.listaRecetas = rh.ListarRecetas();

                //ViewBag.fecha = _fechaTurno;
                //ViewBag.nombreTurno = _nombreTurno;
                //ViewBag.nombreCurso = _nombreCurso;

                //List<ClasesConRecetas> ListaClasesConRecetas = new List<ClasesConRecetas>();
                //var ListaClasesDeUnTurno = ch.ListarClasesDeUnTurno(_idTurno);
                //var ListaRecetasDeUnTurno = ch.ListarRecetasDeTurno(_idTurno);

                //foreach (var item in ListaClasesDeUnTurno)
                //{
                //    var ccr = new ClasesConRecetas(item.idClase, item.idCurso, item.FechaInicio, item.FechaFin, item.Descripcion);
                //    ListaClasesConRecetas.Add(ccr);
                //}

                //var i = 0;
                //var nombres = "";
                //foreach (var item in ListaRecetasDeUnTurno)
                //{
                //    if (i == item.idClase)
                //    {
                //        nombres = nombres + " - " + item.Nombre;
                //        ListaClasesConRecetas.Find(x => x.idClase == item.idClase).receta = nombres;
                //    }
                //    else
                //    {
                //        ListaClasesConRecetas.Find(x => x.idClase == item.idClase).receta = item.Nombre;
                //        nombres = item.Nombre;
                //    }
                //    i = item.idClase;
                //}

                //return View("CrearClases", ListaClasesConRecetas);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("CrearClases2", "Turnos", new { id = _idTurno.ToString() });
            }
        }

        [HttpPost]
        public ActionResult AgregarTurnoNuevo(Turnos t, int cantidadClases = 0, bool autocompletado = false)
        {
            List<Clases> listaClasesConDescripcionDelUltimoTurno = new List<Clases>();
            int cont = 0;
            int idclaseAnt = 0;
            _horaInicioTurno = t.Inicio;
            _horaFinTurno = t.Fin;
            t.idCurso = _idCurso;

            string[] f1;
            var dd = "";
            var mm = "";
            var yy = "";
            var fecha2 = "";

            try
            {
                f1 = t.s_FechaInicio.Split('/');
                dd = f1[0];
                mm = f1[1];
                yy = f1[2];
                fecha2 = mm + "/" + dd + "/" + yy;
                t.FechaInicio = Convert.ToDateTime(fecha2);

                var hInicio = Convert.ToDateTime(t.Inicio);
                var hFin = Convert.ToDateTime(t.Fin);
                TurnosHandle th = new TurnosHandle();
                ClasesHandle ch = new ClasesHandle();

                var cantidadClasesAnteriores = listaClasesConDescripcionDelUltimoTurno.Count;
                if (autocompletado == true)
                {
                    listaClasesConDescripcionDelUltimoTurno = ch.TraerDescripcionDeClasesDeUnCurso(_idCurso);     //este metodo en realidad trae una lista de clases del ultimo turno, con sus descripciones..
                    cantidadClasesAnteriores = listaClasesConDescripcionDelUltimoTurno.Count;
                    cont = cantidadClasesAnteriores;
                }

                Clases c = new Clases();
                c.FechaInicio = new DateTime(t.FechaInicio.Year, t.FechaInicio.Month, t.FechaInicio.Day, hInicio.Hour, hInicio.Minute, hInicio.Second);
                c.FechaFin = new DateTime(t.FechaInicio.Year, t.FechaInicio.Month, t.FechaInicio.Day, hFin.Hour, hFin.Minute, hFin.Second);
                c.idCurso = _idCurso;
                c.Descripcion = " - ";

                if (cantidadClasesAnteriores == cantidadClases)
                {
                    c.Descripcion = listaClasesConDescripcionDelUltimoTurno.FirstOrDefault().Descripcion;
                    idclaseAnt = listaClasesConDescripcionDelUltimoTurno.FirstOrDefault().idClase;
                    listaClasesConDescripcionDelUltimoTurno.RemoveAt(0);
                }

                for (int i = 0; i < cantidadClases - 1; i++)
                {
                    var x = ch.ListarClasesDeUnaFechaDeCualquierTurno(c.FechaInicio, c.FechaFin);
                    var y = ch.ListarClasesConHoraInicioYHoraFin(c.FechaInicio, c.FechaFin);
                    if (x.Count == 0 && y.Count == 0)
                    {
                        c.FechaInicio = c.FechaInicio.AddDays(7);
                        c.FechaFin = c.FechaFin.AddDays(7);
                    }
                    else
                    {
                        ViewBag.mensajeError = "No se puede crear el turno en ese horario, debido a que coincide con otro, o una de sus clases coincide con alguna de otro turno";
                        return View("CrearTurno");
                    }
                }

                if (!th.AgregarTurno(t))
                {
                    _nombreTurno = t.Dia + " de " + t.Inicio + " a " + t.Fin;
                    _fechaTurno = t.FechaInicio.ToString("dd/MM/yyyy");
                    ModelState.Clear();
                }

                var idTurno = th.RecuperarIdTurno();

                if (idTurno != 0)
                {
                    c.idTurno = idTurno;
                    _idTurno = idTurno;
                    // int idClaseUlti = 0;
                    for (int i = 0; i < cantidadClases; i++)
                    {
                        if (!ch.AgregarClase(c))
                        {
                            c.FechaInicio = c.FechaInicio.AddDays(-7);
                            c.FechaFin = c.FechaFin.AddDays(-7);

                            if (cantidadClasesAnteriores == cantidadClases && listaClasesConDescripcionDelUltimoTurno.Count != 0)
                            {
                                var item = listaClasesConDescripcionDelUltimoTurno.FirstOrDefault();
                                c.Descripcion = item.Descripcion;
                                var recetas = new List<Recetas>();
                                if (idclaseAnt != 0)
                                {
                                    recetas = ch.ListarRecetasDeClase(idclaseAnt);
                                }

                                var ultimoIdClase = ch.RecuperarIdClase();
                                foreach (var item2 in recetas)
                                {
                                    ch.AsignarRecetaClase(item2.idReceta, _idCurso, _idTurno, ultimoIdClase);
                                }
                                listaClasesConDescripcionDelUltimoTurno.Remove(item);
                                idclaseAnt = item.idClase;
                            }
                            else if (cantidadClasesAnteriores == cantidadClases && listaClasesConDescripcionDelUltimoTurno.Count == 0)
                            {
                                var recetas = new List<Recetas>();
                                if (idclaseAnt != 0)
                                {
                                    recetas = ch.ListarRecetasDeClase(idclaseAnt);
                                }

                                var ultimoIdClase = ch.RecuperarIdClase();
                                foreach (var item2 in recetas)
                                {
                                    ch.AsignarRecetaClase(item2.idReceta, _idCurso, _idTurno, ultimoIdClase);
                                }
                            }
                        }
                        else
                        {
                            ViewBag.mensajeError = "Ocurrio un error inesperado. Intentelo mas tarde";
                            return View("CrearTurno");
                        }
                        //  listaClasesConDescripcionDelUltimoTurno.RemoveAt(0);
                    }
                    _mensajeExito = "Turno agregado exitosamente con " + cantidadClases + " clases";
                    return RedirectToAction("Turno2", "Turnos");
                }
                ViewBag.mensajeError = "Error al intentar recuperar informacion de la base de datos. Intentelo mas tarde";
                return View("CrearTurno");
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return View("CrearTurno");
            }
        }

        public ActionResult EditTurno(string id)   //recibe id del turno a modificar
        {
            _idTurno = Convert.ToInt32(id);
            TurnosHandle th = new TurnosHandle();

            try
            {
                var turno = th.ListarTurnosDeUnCursoSinRestriccion(_idCurso).Find(t => t.idTurno == _idTurno);
                _fechaTurno = turno.FechaInicio.ToString("dd/MM/yyyy");
                _horaInicioTurno = turno.Inicio;
                _horaFinTurno = turno.Fin;
                var ini = turno.FechaInicio;
                ViewBag.dia = turno.Dia;
                ViewBag.inicio = turno.Inicio;
                ViewBag.fin = turno.Fin;
                return View(turno);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Turno2");
            }
        }

        [HttpPost]
        public ActionResult Edit(Turnos t)
        {
            string[] f1;
            var dd = "";
            var mm = "";
            var yy = "";
            var fecha2 = "";

            ClasesHandle ch = new ClasesHandle();
            List<Clases> listaClases = new List<Clases>();
            Clases c = new Clases();

            try
            {
                f1 = t.s_FechaInicio.Split('/');
                dd = f1[0];
                mm = f1[1];
                yy = f1[2];
                fecha2 = mm + "/" + dd + "/" + yy;
                t.FechaInicio = Convert.ToDateTime(fecha2);

                var hInicio = Convert.ToDateTime(t.Inicio);
                var hFin = Convert.ToDateTime(t.Fin);

                c.FechaInicio = new DateTime(t.FechaInicio.Year, t.FechaInicio.Month, t.FechaInicio.Day, hInicio.Hour, hInicio.Minute, hInicio.Second);
                c.FechaFin = new DateTime(t.FechaInicio.Year, t.FechaInicio.Month, t.FechaInicio.Day, hFin.Hour, hFin.Minute, hFin.Second);

                if (_fechaTurno != t.FechaInicio.ToString("dd/MM/yyyy") || _horaInicioTurno != t.Inicio || _horaFinTurno != t.Fin)
                {
                    foreach (var item in ch.ListarClasesDeUnTurno(t.idTurno))
                    {
                        if (ch.ListarClasesDeUnaFecha(c.FechaInicio, c.FechaFin, _idTurno).Count() != 0)
                        {
                            ViewBag.mensajeError = "Existe una clase que se superpone con el horario " + c.FechaInicio.ToString("HH:mm") + " a " + c.FechaFin.ToString("HH:mm") + " para la fecha: " + c.FechaInicio.ToString("dd/MM/yyyy");
                            return View("EditTurno");
                        }
                        item.FechaInicio = c.FechaInicio;
                        item.FechaFin = c.FechaFin;
                        listaClases.Add(item);
                        c.FechaInicio = item.FechaInicio.AddDays(7);
                        c.FechaFin = item.FechaFin.AddDays(7);
                    }

                    foreach (var item in listaClases)
                    {
                        ch.EditarClase(item);
                    }
                    ch.ListarClasesDeUnTurno(t.idTurno);
                    TurnosHandle th = new TurnosHandle();
                    th.EditarTurno(t);
                }
                _mensajeExito = "Turno modificado exitosamente";

                if (_control2 != 2)
                    return RedirectToAction("Turno2");
                else
                {
                    _control2 = 0;
                    return RedirectToAction("CrearClases2", new { id = _idTurno });
                }
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                if (_control2 != 2)
                    return RedirectToAction("Turno2");
                else
                {
                    _control2 = 0;
                    return RedirectToAction("PosponerTurno", new { idTurno = _idTurno });
                }
            }
        }

        public ActionResult BorrarTurno(int id)
        {
            try
            {
                TurnosHandle th = new TurnosHandle();
                if (th.BorrarTurno(id))
                {
                    _mensajeExito = "Turno borrado exitosamente";
                }
                return RedirectToAction("Turno2");
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Turno2");
            };
        }

        public ActionResult EditClase(int id)    //recibe id de la clase a editar
        {
            try
            {
                _idClase = id;
                RecetasHandle rh = new RecetasHandle();
                ClasesHandle ch = new ClasesHandle();
                var clase = ch.ListarClasesDeUnTurno(_idTurno).Find(t => t.idClase == id);
                _fechaClase = clase.FechaInicio.ToShortDateString();
                _fechaClaseDateTime = clase.FechaInicio;
                _horaInicioClase = clase.FechaInicio.ToString("HH:mm");
                _horaFinClase = clase.FechaFin.ToString("HH:mm");
                ViewBag.inicio = _horaInicioClase;
                ViewBag.fin = _horaFinClase;
                ViewBag.listaRecetasClase = ch.ListarRecetasDeClase(id);
                ViewBag.listaRecetas = rh.ListarRecetas();
                ViewBag.idTurno = _idTurno;
                return View(clase);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("CrearClases2", new { id = _idTurno });
            }
        }

        public ActionResult GuardarEditedClase(Clases c, string inicio, string fin, int idReceta = 0, int idReceta2 = 0, int idReceta3 = 0, int idReceta4 = 0, int idReceta5 = 0, int idReceta6 = 0, int idReceta7 = 0, int idReceta8 = 0)
        {
            try
            {
                string[] f1 = c.fecha.Split('/');
                var dd = f1[0];
                var mm = f1[1];
                var yy = f1[2];
                var fecha2 = mm + "/" + dd + "/" + yy;
                c.FechaInicio = Convert.ToDateTime(fecha2);

                var horaInicio = Convert.ToDateTime(inicio);
                c.FechaInicio = new DateTime(c.FechaInicio.Year, c.FechaInicio.Month, c.FechaInicio.Day, horaInicio.Hour, horaInicio.Minute, horaInicio.Second);
                var horaFin = Convert.ToDateTime(fin);
                c.FechaFin = new DateTime(c.FechaInicio.Year, c.FechaInicio.Month, c.FechaInicio.Day, horaFin.Hour, horaFin.Minute, horaFin.Second);

                ClasesHandle ch = new ClasesHandle();
                var horita = c.FechaInicio;
                var listaDeClases = ch.ListarClasesDeUnTurno(_idTurno);
                if (c.FechaInicio.ToShortDateString() != _fechaClase || inicio != _horaInicioClase || fin != _horaFinClase)
                {
                    if (ch.ListarClasesDeUnaFecha(c.FechaInicio, c.FechaFin, _idTurno).Count() != 0)
                    {
                        // _mensajeError = "Existe una clase que se superpone con el horario " + c.FechaInicio.ToString("HH:mm") + " a " + c.FechaFin.ToString("HH:mm") + " para la fecha: " + c.FechaInicio.ToString("dd/MM/yyyy");
                        RecetasHandle rh = new RecetasHandle();
                        var clase = listaDeClases.Find(t => t.idClase == _idClase);
                        try
                        {
                            ViewBag.inicio = clase.FechaInicio.ToString("HH:mm");
                            ViewBag.fin = clase.FechaFin.ToString("HH:mm");
                            ViewBag.listaRecetasClase = ch.ListarRecetasDeClase(_idClase);
                            ViewBag.listaRecetas = rh.ListarRecetas();
                        }
                        catch (Exception e)
                        {
                            _mensajeError = e.Message;
                        }
                        ViewBag.mensajeError = "Existe una clase que se superpone con el horario " + c.FechaInicio.ToString("HH:mm") + " a " + c.FechaFin.ToString("HH:mm") + " para la fecha: " + c.FechaInicio.ToString("dd/MM/yyyy");
                        ViewBag.idTurno = _idTurno;
                        return View("EditClase", clase);
                    }
                }

                ch.EditarClase(c);
                horita = horita.AddDays(7);
                foreach (var item in listaDeClases.Where(y => y.FechaInicio > (_fechaClaseDateTime.AddDays(1))))
                {
                    item.FechaInicio = horita;
                    ch.EditarClase(item);
                    horita = horita.AddDays(7);
                }

                if (idReceta != 0)
                {
                    ch.AsignarRecetaClase(idReceta, _idCurso, _idTurno, _idClase);
                }
                if (idReceta2 != 0)
                {
                    ch.AsignarRecetaClase(idReceta2, _idCurso, _idTurno, _idClase);
                }
                if (idReceta3 != 0)
                {
                    ch.AsignarRecetaClase(idReceta3, _idCurso, _idTurno, _idClase);
                }
                if (idReceta4 != 0)
                {
                    ch.AsignarRecetaClase(idReceta4, _idCurso, _idTurno, _idClase);
                }
                if (idReceta5 != 0)
                {
                    ch.AsignarRecetaClase(idReceta5, _idCurso, _idTurno, _idClase);
                }
                if (idReceta6 != 0)
                {
                    ch.AsignarRecetaClase(idReceta6, _idCurso, _idTurno, _idClase);
                }
                if (idReceta7 != 0)
                {
                    ch.AsignarRecetaClase(idReceta7, _idCurso, _idTurno, _idClase);
                }
                if (idReceta8 != 0)
                {
                    ch.AsignarRecetaClase(idReceta8, _idCurso, _idTurno, _idClase);
                }

                _mensajeExito = "Clase modificada exitosamente";
                return RedirectToAction("CrearClases2", new { id = _idTurno.ToString() });
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("CrearClases2", new { id = _idTurno.ToString() });
            }
        }

        public ActionResult QuitarRecetaDeClase(int idReceta)
        {
            ClasesHandle ch = new ClasesHandle();
            RecetasHandle rh = new RecetasHandle();
            try
            {
                ch.QuitarRecetaClase(idReceta, _idClase);
                ViewBag.mensajeExito = "Receta quitada exitosamente";
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
            }

            var clase = ch.ListarClasesDeUnTurno(_idTurno).Find(t => t.idClase == _idClase);
            try
            {
                ViewBag.inicio = clase.FechaInicio.ToString("HH:mm");
                ViewBag.fin = clase.FechaFin.ToString("HH:mm");
                ViewBag.listaRecetasClase = ch.ListarRecetasDeClase(_idClase);
                ViewBag.listaRecetas = rh.ListarRecetas();
                ViewBag.idTurno = _idTurno;
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
            }

            return View("EditClase", clase);
        }

        public ActionResult BorrarClase(int id)
        {
            try
            {
                ClasesHandle ch = new ClasesHandle();
                if (!ch.BorrarClase(id))
                {
                    _mensajeExito = "Clase borrada exitosamente";
                }
                return RedirectToAction("CrearClases2", new { id = _idTurno.ToString() });
            }
            catch (SqlException ex)
            {
                if (ex.Errors.Count > 0) // Assume the interesting stuff is in the first error
                {
                    switch (ex.Errors[0].Number)
                    {
                        case 547: // Foreign Key violation
                            _mensajeError = "La clase no puede ser borrada porque ya fue dictada";
                            break;
                            /*
                        case 2601: // Primary key violation
                            throw new DuplicateRecordException("Some other helpful description", ex);
                            break;
                        default:
                            throw new DataAccessException(ex);*/
                    }
                }

                return RedirectToAction("CrearClases2", new { id = _idTurno.ToString() });
            };
        }

        public ActionResult VerDetalle(int id, string nombre, string apellido) //recibe el id del alumno nombre y apellido
        {
            CursosHandle ch = new CursosHandle();
            _idAlumno = id;
            TurnosHandle th = new TurnosHandle();

            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }

            ViewBag.mensajeExito = _mensajeExito;
            ViewBag.mensajeError = _mensajeError;
            _mensajeExito = "";
            _mensajeError = "";

            try
            {
                var costo = ch.TraerCostoCurso(_idCurso).FirstOrDefault().Costo;
                var sumaPago = th.DetallePagoAlumno(_idTurno, id).Sum(x => x.Monto);
                var Adeuda = costo - sumaPago;
                ViewBag.sumaPago = sumaPago;
                ViewBag.deuda = Adeuda;
                ViewBag.idTurno = _idTurno;
                ViewBag.idalumno = id;
                ViewBag.alumno = apellido + " " + nombre;
                ViewBag.nombre = nombre;
                ViewBag.apellido = apellido;
                return View(th.DetallePagoAlumno(_idTurno, id));
            }
            catch (Exception e)
            {
            }
            return View();
        }

        public ActionResult PagoAlumno(int monto, string usuario, string Nombre, string Apellido, int idAlumno, string fecha)
        {
            DateTime fechaFinal;
            if (_cultura == "en")
            {
                string[] fech = fecha.Split('/');
                fechaFinal = Convert.ToDateTime(fech[1] + "/" + fech[0] + "/" + fech[2]);
            }
            else
            {
                fechaFinal = Convert.ToDateTime(fecha);
            }

            TurnosHandle th = new TurnosHandle();
            try
            {
                if (!th.PagoAlumno(monto, _idAlumno, _idTurno, _idCurso, usuario, fechaFinal))
                {
                    _mensajeExito = "Pago realizado exitosamente";
                }

                return RedirectToAction("VerDetalle", "Turnos", new { id = idAlumno, nombre = Nombre, apellido = Apellido });
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
            }
            return View("AlumnosInscriptos", th.AlumnosInscriptos(_idTurno));
        }

        public ActionResult AlumnosInscriptos(string id) //recibe idturno
        {
            var idT = Convert.ToInt32(id);
            _idTurno = idT;
            TurnosHandle th = new TurnosHandle();
            try
            {
                var t1 = th.ListarTurnosDeUnCursoSinRestriccion(_idCurso).Find(y => y.idTurno == idT);
                _nombreTurno = t1.Dia.ToUpper() + " de " + t1.Inicio + " a " + t1.Fin;
                var ListaA = th.AlumnosInscriptos(idT);
                ViewBag.nombreCurso = _nombreCurso;
                ViewBag.nombreTurno = _nombreTurno;
                ViewBag.idCurso = _idCurso;
                ViewBag.idTurno = idT;
                return View(ListaA);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
            }
            ViewBag.nombreCurso = _nombreCurso;
            ViewBag.mensajeError = _mensajeError;
            _mensajeError = "";
            return View("Turno", th.ListarTurnosDeUnCurso(_idCurso));
        }

        public ActionResult AlumnosInscriptos2(int idTurno, int idCurso, string nombrecurso) //recibe idturno y idCurso para la vista cronograma
        {
            _idTurno = idTurno;
            _idCurso = idCurso;
            _nombreCurso = nombrecurso;
            TurnosHandle th = new TurnosHandle();
            try
            {
                var t1 = th.ListarTurnosDeUnCurso(_idCurso).Find(y => y.idTurno == _idTurno);
                _nombreTurno = t1.Dia.ToUpper() + " de " + t1.Inicio + " a " + t1.Fin;
                var ListaA = th.AlumnosInscriptos(_idTurno);
                ViewBag.nombreCurso = _nombreCurso;
                ViewBag.nombreTurno = _nombreTurno;
                return View("AlumnosInscriptos", ListaA);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
            }
            ViewBag.nombreCurso = _nombreCurso;
            ViewBag.mensajeError = _mensajeError;
            return View("Turno", th.ListarTurnosDeUnCurso(_idCurso));
        }

        public ActionResult ResumenTurnos()
        {
            TurnosHandle th = new TurnosHandle();
            CursosTurnos ct = new CursosTurnos();
            CursosHandle ch = new CursosHandle();
            ct.l_Cursos = ch.ListarCursos2();
            ct.l_Turnos = th.ListarTurnos2();

            return View(ct);
        }

        public ActionResult AsistenciaClases(int idclase, int idturno, int idcurso, string fecha) //recibe el id de la clase seleccionada
        {
            int idt;
            _idClase = idclase;
            _idCurso = idcurso;
            _idTurno = idturno;
            TurnosHandle th = new TurnosHandle();
            ClasesHandle ch = new ClasesHandle();
            CursosHandle cuh = new CursosHandle();

            try
            {
                idt = th.ListarTurnoDeClase(idclase).Find(x => x.idClase == idclase).idTurno;
                _idTurno = idt;
                ViewBag.idTurno = idt;
                var ListaA = th.AlumnosInscriptos(idt);

                _lista2 = ch.AsistenciaTomada(_idClase, _idTurno);

                if (_lista2.Where(x => x.Mail == "Ausente").Count() == ListaA.Count())
                {
                    ViewBag.control = 1;
                }

                else
                {
                    ViewBag.control = 0;
                }

                ViewBag.listaAsistencia = ch.AsistenciaTomada(_idClase, _idTurno);
                ViewBag.nombreCurso = _nombreCurso;
                ViewBag.nombreTurno = _nombreTurno;
                ViewBag.fecha = fecha;
                return View(ListaA);
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return View();
            }
        }

        public ActionResult Asistencia(string vector)
        {
            ClasesHandle ch = new ClasesHandle();
            VentasHandle vh = new VentasHandle();
            LineaVentaHandle lvh = new LineaVentaHandle();
            try
            {
                Char delimiter = ',';
                String[] substrings = vector.Split(delimiter);
                for (int i = 0; i < substrings.Length; i++)
                {
                    if (_lista2.Count != 0)
                    {
                        foreach (var item in _lista2)
                        {
                            if (Convert.ToInt32(substrings[i]) == item.idAlumno && item.Mail == "Ausente")
                            {
                                ch.AsistenciaDeAlumnoEnClase(_idCurso, _idTurno, _idClase, Convert.ToInt32(substrings[i]));
                                _mensajeExito = "Asistencia tomada exitosamente";
                            }
                        }
                    }
                }
                return RedirectToAction("CrearClases2", "Turnos", new { id = _idTurno });
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
            }
            return RedirectToAction("CrearClases2", "Turnos", new { id = _idTurno });
        }

        public ActionResult AsistenciaClaseDictada()
        {
            ClasesHandle ch = new ClasesHandle();
            try
            {
                return View(ch.ListaDeAsistencia(_idTurno));
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("CrearClases2", "Turnos", new { id = _idTurno });
            }
        }

        public ActionResult FullCalendar()
        {
            _control = 0;
            try
            {
                ClasesHandle ch = new ClasesHandle();
                return View(ch.ListarEventosFullCalendar());
            }

            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Index", "Index");
            }
        }

        public ActionResult PosponerTurno(int idTurno)  //recibe el id de la primera clase para luego correr la fecha de esa primera clase (se corren todas las demás).
        {
            _idTurno = Convert.ToInt32(idTurno);
            TurnosHandle th = new TurnosHandle();
            _control2 = 2;

            try
            {
                var turno = th.ListarTurnosDeUnCursoSinRestriccion(_idCurso).Find(t => t.idTurno == _idTurno);
                _fechaTurno = turno.FechaInicio.ToString("dd/MM/yyyy");
                _horaInicioTurno = turno.Inicio;
                _horaFinTurno = turno.Fin;
                var ini = turno.FechaInicio;
                ViewBag.dia = turno.Dia;
                ViewBag.inicio = turno.Inicio;
                ViewBag.fin = turno.Fin;
                return View("PosponerTurno", turno);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("CrearClases2", new { id = idTurno });
            }
        }

        public ActionResult Desinscribir(int id)
        {
            TurnosHandle th = new TurnosHandle();
            try
            {
                th.Desinscribir(id, _idTurno);
                _mensajeExito = "Alumnos desinscripto correctamente";
                return RedirectToAction("CrearClases2", new { id =Convert.ToString (_idTurno) });

            }
            catch (Exception e) {
                _mensajeError = e.Message;
                return RedirectToAction("CrearClases2", new { id = Convert.ToString(_idTurno) });
            }

        }

        public ActionResult ImprimirClase(int idClase)
        {
            _idClase = idClase;
            ClasesHandle ch = new ClasesHandle();
            RecetasParaImprimir recetasImprimir = new RecetasParaImprimir();
            var receta = new Recetas();

            ViewBag.idTurno = _idTurno;
            ViewBag.nombreCurso = _nombreCurso;

            try
            {
                var listaRecetas = ch.ListarRecetasDeClase(idClase);

                if (listaRecetas.Count > 0)
                {
                    receta = listaRecetas.First();
                    recetasImprimir.r1 = receta;
                    listaRecetas.Remove(receta);
                }

                if (listaRecetas.Count > 0)
                {
                    receta = listaRecetas.First();
                    recetasImprimir.r2 = receta;
                    listaRecetas.Remove(receta);
                }

                if (listaRecetas.Count > 0)
                {
                    receta = listaRecetas.First();
                    recetasImprimir.r3 = receta;
                    listaRecetas.Remove(receta);
                }

                if (listaRecetas.Count > 0)
                {
                    receta = listaRecetas.First();
                    recetasImprimir.r4 = receta;
                    listaRecetas.Remove(receta);
                }

                if (listaRecetas.Count > 0)
                {
                    receta = listaRecetas.First();
                    recetasImprimir.r5 = receta;
                    listaRecetas.Remove(receta);
                }

                if (listaRecetas.Count > 0)
                {
                    receta = listaRecetas.First();
                    recetasImprimir.r6 = receta;
                    listaRecetas.Remove(receta);
                }

                if (listaRecetas.Count > 0)
                {
                    receta = listaRecetas.First();
                    recetasImprimir.r7 = receta;
                    listaRecetas.Remove(receta);
                }

                if (listaRecetas.Count > 0)
                {
                    receta = listaRecetas.First();
                    recetasImprimir.r8 = receta;
                    listaRecetas.Remove(receta);
                }

                if (listaRecetas.Count > 0)
                {
                    receta = listaRecetas.First();
                    recetasImprimir.r9 = receta;
                    listaRecetas.Remove(receta);
                }

                if (listaRecetas.Count > 0)
                {
                    receta = listaRecetas.First();
                    recetasImprimir.r10 = receta;
                    listaRecetas.Remove(receta);
                }

                return View(recetasImprimir);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("","");
            }
        }
    }
}
