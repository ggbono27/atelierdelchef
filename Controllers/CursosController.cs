﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_1.Models;
using Proyecto_1.ViewModels;
using System.IO;
using System.Drawing;
using OfficeOpenXml.Style;
using OfficeOpenXml;
using System.Web.Configuration;

namespace Proyecto_1.Controllers
{
    public class CursosController : Controller
    {
        private static int _idCurso;
        private static string _nombreCurso;
        private static string _nombreTurno;
        private static int _idAlumno;
        private static string _mensajeExito;
        private static string _mensajeError;
        private static IList<Pagos> _list;
        private static IList<Cursos> _listCursos;
        private static int _idTurno;
        private static string _mensaje;
        private static int _total;
        private static int _control;
        private static string cultura = WebConfigurationManager.AppSettings["cultura"];
        public ActionResult Index()
        {
            return View("index");
        }

        public void SetMensajeError(string mensaje)
        {
            _mensajeError = mensaje;
        }

        public ActionResult Curso()
        {
            CursosHandle ch = new CursosHandle();
            ModelState.Clear();

            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }
            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }
            _control = 1;
            ViewBag.control = _control;

            _mensajeError = "";
            _mensajeExito = "";
            return View(ch.ListarCursos());
        }

        public ActionResult CrearCurso()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AgregarCurso(Cursos c)
        {
            try
            {
                CursosHandle ch = new CursosHandle();
                if (!ch.AgregarCursos(c))
                {
                    _mensajeExito = "Curso creado exitosamente";
                    ModelState.Clear();
                }

                return RedirectToAction("Curso");
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return View("CrearCurso");
            }
        }

        public ActionResult EditCurso(int id, string nombre)
        {
            CursosHandle ch = new CursosHandle();
            return View(ch.ListarCursos().Find(amodel => amodel.idCurso == id));
        }

        [HttpPost]
        public ActionResult Edit(Cursos curso)
        {
            try
            {
                CursosHandle ch = new CursosHandle();
                ch.EditarCursos(curso);
                _mensajeExito = "Curso editado exitosamente";
                return RedirectToAction("Curso");
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Curso");
            }
        }

        public ActionResult DeleteCurso(int id)
        {
            try
            {
                CursosHandle ch = new CursosHandle();
                if (!ch.BorrarCurso(id))
                {
                    _mensajeExito = "Curso borrado exitosamente";
                    ModelState.Clear();
                }
                return RedirectToAction("Curso");
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Curso");
            }
        }

        [HttpPost]
        public ActionResult BuscarCurso(string cadena)
        {
            CursosHandle ch = new CursosHandle();
            try
            {
                _listCursos = ch.BuscarCursos(cadena);
                if (_listCursos.Count() == 0)
                {
                    ViewBag.mensajeError = "No se encontro el curso buscado";
                }
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
            }
            return View("Curso", _listCursos);
        }

        public ActionResult CrearAlumnoParaInscripcion(string control)
        {
            _control = Convert.ToInt32(control);
            ViewBag.control = _control;
            var x = _idCurso;

            return View();
        }

        //Este action primero agrega el alumno a la bd y si hay exito, sigue a la vista "PagoInscripcion" para terminar de inscribir al alumno.
        public ActionResult InscribirAlumnoRecienCreadoPaso2(Alumno alumno, int control)
        {
            if (alumno.Direccion == null)
            {
                alumno.Direccion = "-";
            }

            if (alumno.Mail == null)
            {
                alumno.Mail = "-";
            }

            if (alumno.Telefono2 == null)
            {
                alumno.Telefono2 = "-";
            }

            if (alumno.Usuario == null)
            {
                alumno.Usuario = "-";
            }

            if (alumno.Dni == 0)
            {
                alumno.Dni = 1111111;
            }

            string[] f1;
            try
            {

                if (alumno.fecha != null)
                {
                    f1 = alumno.fecha.Split('/');
                    var fecha2 = f1[1] + "/" + f1[0] + "/" + f1[2];
                    alumno.FechaNac = Convert.ToDateTime(fecha2);
                }
                else
                {
                    alumno.FechaNac = new DateTime(1888, 01, 01);
                }

                AlumnoHandle ah = new AlumnoHandle();

                _idAlumno = ah.AgregarAlumnoYRecuperarId(alumno);
               // _mensajeExito = "Alumno creado exitosamente";
                ModelState.Clear();

                ViewBag.nombreC = _nombreCurso;
                ViewBag.nombreA = alumno.Nombre;
                ViewBag.apellidoA = alumno.Apellido;
                ViewBag.control = control;
                ViewBag.nombreTurno = _nombreTurno;
                ViewBag.idTurno = _idTurno;
                // var lista = ah.ListarAlumnos().Where(m => m.Dni == a1.Dni);

                ViewBag.usuario = alumno.Usuario;
                TurnosHandle th = new TurnosHandle();
                // ViewBag.turno= th.ListarTurnosDeUnCurso(_idCurso).Find(x=> x.idTurno=)
                return View("PagoInscripcion", th.ListarTurnosDeUnCurso(_idCurso));
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return View("CrearAlumnoParaInscripcion");
            }
        }

        //id es el idCurso.  si control = 1, viene desde abm turno, sino desde el calendario.
        public ActionResult InscribirAlumnoPaso1(string Nombre, int id, int idTurno, int control, string nombreTurno)
        {
            try
            {
                _control = control;
                ViewBag.nombre = Nombre;
                _nombreTurno = nombreTurno;
                _idTurno = idTurno;
                _idCurso = id;
                _nombreCurso = Nombre;
                ViewBag.control = _control;

                AlumnoHandle ah = new AlumnoHandle();
                var listaAlumnos = ah.AlumnosQueNoCursaron(_idCurso);

                return View("Inscripcion", listaAlumnos);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("curso", "Cursos");
            }
            
        }

        //Este action es llamado desde el boton cancelar de la vista CrearAlumnoParaInscripcion, se usa para volver a la vista donde se selecciona
        //el alumno para inscribir en un curso ya seleccionado
        public ActionResult InscribirAlumnoPaso1_2()
        {
            ViewBag.nombre = _nombreCurso;
            AlumnoHandle ah = new AlumnoHandle();
            return View("Inscripcion", ah.ListarAlumnos());
        }

        public ActionResult InscribirAlumnoPaso2(int idAlumno, string nombreAlumno, string apellidoAlumno)
        {
            ViewBag.nombreTurno = _nombreTurno.ToLower();
            ViewBag.control = _control;
            ViewBag.idTurno = _idTurno;
            ViewBag.nombreC = _nombreCurso;
            ViewBag.nombreA = nombreAlumno;
            ViewBag.apellidoA = apellidoAlumno;
            ViewBag.idTurno = _idTurno;
            _idAlumno = idAlumno;

            TurnosHandle th = new TurnosHandle();

            return View("PagoInscripcion", th.ListarTurnosDeUnCurso(_idCurso)); //CrearAlumnoParaInscripcion
        }

        public ActionResult BuscarAlumnoParaInscripcion(string cadena) // cadena puede ser apellido o nombre, depende lo que pongan en la barra de busqueda...
        {
            AlumnoHandle ah = new AlumnoHandle();
            ModelState.Clear();

            ViewBag.nombre = _nombreCurso;
            return View("Inscripcion", ah.BuscarAlumno(cadena));
        }

        [HttpPost]
        public ActionResult Inscripcion(int idTurno, string monto, string usuario, string control2 = "")
        {
            String[] m2;
            m2 = monto.Split('.');

            if (m2.Length == 2)
            {
                monto = m2[0] + ',' + m2[1];
            }

            try
            {
                TurnosHandle th = new TurnosHandle();
                if (th.AlumnosInscriptos(idTurno).Find(x => x.idAlumno == _idAlumno) != null)
                {
                    ViewBag.nombreC = _nombreCurso;
                    ViewBag.mensajeError = "Este alumno ya se encuentra inscripto en este turno";
                    return View("PagoInscripcion", th.ListarTurnosDeUnCurso(_idCurso));

                }
                if (th.AlumnosInscriptos(idTurno).Count() >= th.CantidadAlumnosPorTurno(idTurno).FirstOrDefault().Alumnos)
                {
                    ViewBag.nombreC = _nombreCurso;
                    ViewBag.mensajeError = "Este turno completo el limite de 10 alumnos ";
                    return View("PagoInscripcion", th.ListarTurnosDeUnCurso(_idCurso));
                }
                if (control2 == null)
                {
                    control2 = "-";
                }
                Pagos pago = new Pagos(Int32.Parse(monto), "Inscripcion", _idAlumno, _idCurso, idTurno, usuario, control2);
                PagosHandle ph = new PagosHandle();
                if (!ph.AgregarPago(pago))
                {
                    ModelState.Clear();
                }
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
            }
            TurnosController t = new TurnosController();
            t.setMensaje("Alumno inscripto exitosamente");
            return RedirectToAction("CrearClases2", "Turnos", new { id =Convert.ToString(idTurno)});
        }

        public ActionResult AlumnosPotenciales(int idcurso)
        {
            CursosHandle ch = new CursosHandle();
            try
            {
                return View(ch.AlumnosPotenciales(idcurso));
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Curso");
            }
        }

        public ActionResult IngresosPorCursos()
        {
            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }
            _mensajeError = "";
            CursosHandle ch = new CursosHandle();
            try
            {
                _total = ch.IngresosPorCursos().Sum(p => p.Monto);
                ViewBag.total = _total;
                _list = ch.IngresosPorCursos();
                if (_list.Count() == 0)
                {
                    ViewBag.mensajeInicio = "No hay ingresos actualmente";
                }
                // ViewBag.fechas = "Ingresos generados por los cursos";
                return View(_list);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return View("curso","cursos");
            }
        }

        public ActionResult IngresosPorCursosEntreFechas(string fecha)
        {
            DateTime fecha1 = new DateTime();
            DateTime fecha2 = new DateTime();
            String[] f1;
            f1 = fecha.Split(' ');
            var fec1 = f1[0];
            var fec2 = f1[2];

            if (cultura == "en")
            {
                string[] fech = fec1.Split('/');
                var dd = fech[0];
                var mm = fech[1];
                var yy = fech[2];
                var fech1 = mm + "/" + dd + "/" + yy;

                string[] fech_2 = fec2.Split('/');
                var dd2 = fech_2[0];
                var mm2 = fech_2[1];
                var yy2 = fech_2[2];
                var fech2 = mm2 + "/" + dd2 + "/" + yy2;

                fecha1 = Convert.ToDateTime(fech1);
                fecha2 = Convert.ToDateTime(fech2);
            }
            else
            {
                fecha1 = Convert.ToDateTime(fec1);
                fecha2 = Convert.ToDateTime(fec2);
            }

            fecha2 = fecha2.AddHours(23).AddMinutes(59).AddSeconds(59);
            CursosHandle ch = new CursosHandle();
            try
            {
                _total = ch.IngresosPorCursosEntreFecha(fecha1, fecha2).Sum(p => p.Monto);
                ViewBag.total = _total;
                _list = ch.IngresosPorCursosEntreFecha(fecha1, fecha2);
                if (_list.Count() == 0)
                {
                    // _list = null;
                    ViewBag.fechas = "No hay ingresos entre las fechas " + fecha1.ToString("dd/MM/yyyy") + " - " + fecha2.ToString("dd/MM/yyyy");
                    return View("IngresosPorCursos", _list);
                }
                ViewBag.fechas = "Ingresos generados por cursos entre " + fecha1.ToString("dd/MM/yyyy") + " - " + fecha2.ToString("dd/MM/yyyy");
                return View("IngresosPorCursos", _list);
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return View("IngresosPorCursos");
            }
        }


        public ActionResult GenerarExcel()
        {
            if (_list.Count() == 0)
            {
                _mensajeError = "No es posible imprimir un informe vacio. Por favor generelo otra vez.";
                return RedirectToAction("IngresosPorCursos");
            }
            // MemoryStream memStream;

            ExcelPackage ExcelPkg = new ExcelPackage();
            ExcelWorksheet Sheet1 = ExcelPkg.Workbook.Worksheets.Add("Sheet1");
            using (ExcelRange Rng = Sheet1.Cells[1, 1, 1, 1])
            {
                Rng.Value = _mensaje;
                Sheet1.Cells[3, 1].Value = "Curso";
                Sheet1.Cells[3, 2].Value = "Ingresos";
                Sheet1.Cells["A3:B3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet1.Cells["A3:B3"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                Sheet1.Cells["A3:B3"].Style.Font.Bold = true;
                Rng.Style.Font.Size = 12;
                Rng.Style.Font.Bold = true;
            }

            var i = 4;
            foreach (var item in _list)
            {
                Sheet1.Cells[i, 1].Value = item.Concepto;
                Sheet1.Cells[i, 2].Value = item.Monto;
                i++;
            }
            Sheet1.Cells[i, 1].Value = "Total";
            Sheet1.Cells[i, 2].Value = _total;
            Sheet1.Cells[i, 1].Style.Font.Bold = true;
            Sheet1.Cells[i, 2].Style.Font.Bold = true;

            Sheet1.Protection.IsProtected = false;
            Sheet1.Protection.AllowSelectLockedCells = false;
            // Sheet1.Cells.AutoFitColumns();
            Sheet1.Column(1).Width = 50;
            Sheet1.Column(2).Width = 30;
            //Sheet1.Column(3).Width = 25;
            //Sheet1.Column(4).Width = 33;
            //Sheet1.Column(5).Width = 25;
            //   ExcelPkg.SaveAs(new FileInfo(@"D:\\InformeDeIngresosPorCursos.xlsx"));
            //ExcelPkg.SaveAs(new FileInfo("Informe.xlsx"));

            byte[] excelEnBytes;
            using (MemoryStream ms = new MemoryStream())
            {
                ExcelPkg.SaveAs(ms);
                excelEnBytes = ms.ToArray();
            }

            //  memStream = new MemoryStream(ExcelPkg.GetAsByteArray());
            //  return File(memStream, "application/vnd.ms-excel", "InformeAlumnos.xlsx");
            return File(excelEnBytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "InformeDeIngresosPorCursos.xlsx");

            //return RedirectToAction("FiltradoAlumnos");
        }

    }
}
