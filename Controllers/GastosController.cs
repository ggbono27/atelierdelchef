﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Proyecto_1.Models;
using Proyecto_1.ViewModels;
using System.IO;
using System.Drawing;
using OfficeOpenXml.Style;
using OfficeOpenXml;
using System.Globalization;
using System.Web.Configuration;

namespace Proyecto_1.Controllers
{
    public class GastosController : Controller
    {
        private static String _mensajeExito;
        private static String _mensajeError;
        private static int _editar;
        private static String _vacia;
        private static int _idgasto;
        private static Decimal _total;
        private static Decimal _total2;
        private static GastosGeneral _gastoGral = new GastosGeneral();
        private static IList<Gastos> _list;
        private static IList<Productos> _listProductos;
        private static String _mensaje;
        private static IList<Caja> _list2;
        private static Decimal _totalGastos;
        private static Decimal _totalPagos;
        private static Decimal _totalVentas;
        private static decimal _totalParcial;
        private static int _control;
        private static Decimal _totalIngresosLibres;
        private static string cultura = WebConfigurationManager.AppSettings["cultura"];


        // GET: Gastos
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ComprasInsumos()
        {
            _total = 0;
            GastosHandle gh = new GastosHandle();
            _gastoGral = new GastosGeneral();
            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }

            _mensajeError = "";
            _mensajeExito = "";

            return View(gh.ListarComprasInsumo());
        }

        public ActionResult ComprasProdVentas()
        {
            _totalParcial = 0;
            _control = 0;
            _editar = 0;
            GastosHandle gh = new GastosHandle();
            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }

            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            _mensajeExito = "";
            _mensajeError = "";

            try
            {
                if (gh.ListarComprasProductosVenta().Count() == 0)
                {
                    ViewBag.mensajeFecha = "";
                    ViewBag.mensajeInicio = "No se realizaron compras actualmente";
                }
                else
                {
                    ViewBag.mensajeFecha = "Listado de compras realizadas";
                }

                return View(gh.ListarComprasProductosVenta());
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("ComprasProdVentas");
            }

        }

        public ActionResult Gastos()
        {
            GastosHandle gh = new GastosHandle();
            TipoGastosHandle tgh = new TipoGastosHandle();

            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }
            _mensajeError = "";
            _mensajeExito = "";

            try
            {
                _list = gh.ListarGastos().Concat(gh.ListarComprasProductosVenta()).OrderByDescending(p => p.FechaGasto).ToList();
                _total2 = _list.Sum(x => x.Total);
                ViewBag.total = _total2;
                ViewBag.listaTipo = tgh.ListarTipoGastos();
                if (_list.Count() == 0)
                {
                    ViewBag.mensajeInicio = "No existen egresos registrados actualmente";
                }
                else
                {
                    ViewBag.fechas = "Egresos hasta el día de hoy";
                }

                return View(_list);
            }

            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Gastos", "Gastos");
            }
        }

        public ActionResult NuevoGasto()
        {
            TipoGastosHandle tgh = new TipoGastosHandle();
            GastosHandle gh = new GastosHandle();

            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }
            _mensajeError = "";
            _mensajeExito = "";

            try
            {
                ViewBag.listaTipo = tgh.ListarTipoGastos2();
                return View(gh.ListarGastosParaPagar());
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return RedirectToAction("NuevoGasto");
            }
        }

        public ActionResult BuscarGastoPorTipo(string idTipoGasto)
        {
            TipoGastosHandle tgh = new TipoGastosHandle();
            GastosHandle gh = new GastosHandle();
            var idtipoG = Convert.ToInt32(idTipoGasto);
            try
            {
                ViewBag.listaTipo = tgh.ListarTipoGastos2();
                return View("NuevoGasto", gh.BuscarGastoPorTipo(idtipoG));
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("NuevoGasto");
            }
        }

        public ActionResult DetalleCompraInsumo(int id, DateTime fecha)
        {
            GastosHandle gh = new GastosHandle();
            ViewBag.Fecha = fecha.ToString("dd/MM/yyyy HH:mm");
            return View(gh.DetalleCompraInsumo(id));
        }

        public ActionResult DetalleCompraBazar(int id, DateTime fecha)
        {
            GastosHandle gh = new GastosHandle();
            ViewBag.Fecha = fecha.ToString("dd/MM/yyyy");

            try
            {
                var total = gh.DetalleCompraBazar(id).Sum(x => x.SubTotal);
                ViewBag.Total = total;
                return View(gh.DetalleCompraBazar(id));
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("ComprasProdVentas");
            }
        }

        public ActionResult AgregarLineaCompraInsumo(Decimal cantidad, int idproducto, String nombre, String unidad)
        {
            if (_gastoGral.l_LineaCompras == null)
            {
                _gastoGral.l_LineaCompras = new List<LineaGastos>();
            }

            if (_gastoGral.l_LineaCompras.Find(x => x.idProducto == idproducto) != null)
            {
                _gastoGral.l_LineaCompras.Find(x => x.idProducto == idproducto).Cantidad += cantidad;
            }
            else
            {
                LineaGastos lc = new LineaGastos(idproducto, cantidad, nombre, unidad);
                _gastoGral.l_LineaCompras.Add(lc);
            }
            return View("CrearCompraInsumo", _gastoGral);
        }

        public ActionResult QuitarLineaCompraInsumo(int idproducto, String nombre)
        {
            if (_gastoGral.l_LineaCompras.Find(x => x.idProducto == idproducto) != null)
            {
                _gastoGral.l_LineaCompras.Remove(_gastoGral.l_LineaCompras.Find(x => x.idProducto == idproducto));
                if (_gastoGral.l_LineaCompras.Count == 0)
                {
                    ProductosHandle ph = new ProductosHandle();
                    _gastoGral = new GastosGeneral();
                    _gastoGral.l_Producto = ph.BuscarProductosInsumo(nombre);
                    return View("CrearCompraInsumo", _gastoGral);
                }
            }

            return View("CrearCompraInsumo", _gastoGral);
        }

        public ActionResult AgregarLineaCompraBazar(Decimal cantidad, Decimal precio, int idproducto, String nombre)
        {
            if (_gastoGral.l_LineaCompras == null)
            {
                _gastoGral.l_LineaCompras = new List<LineaGastos>();
            }

            if (_gastoGral.l_LineaCompras.Find(x => x.idProducto == idproducto) != null)
            {
                _gastoGral.l_LineaCompras.Find(x => x.idProducto == idproducto).Cantidad += cantidad;
                _gastoGral.l_LineaCompras.Find(x => x.idProducto == idproducto).SubTotal += cantidad * precio;
                _totalParcial += (cantidad * precio);
            }
            else
            {
                LineaGastos lc = new LineaGastos(idproducto, cantidad, precio, precio * cantidad, nombre);
                _gastoGral.l_LineaCompras.Add(lc);
                _totalParcial = _totalParcial + lc.SubTotal;
            }
            ViewBag.control = _control;
            ViewBag.Total = "$" + _totalParcial.ToString();
            if (_editar == 1)
            {
                return View("EditarCompraBazar", _gastoGral);
            }
            else
            {
                return View("CrearCompraPV", _gastoGral);
            }
        }

        public ActionResult QuitarLineaCompraBazar(int idproducto, String nombre)
        {
            ViewBag.control = _control;
            if (_gastoGral.l_LineaCompras.Find(x => x.idProducto == idproducto) != null)
            {
                _totalParcial -= _gastoGral.l_LineaCompras.Find(x => x.idProducto == idproducto).SubTotal;
                _gastoGral.l_LineaCompras.Remove(_gastoGral.l_LineaCompras.Find(x => x.idProducto == idproducto));
                if (_gastoGral.l_LineaCompras.Count == 0)
                {
                    ProductosHandle ph = new ProductosHandle();
                    _gastoGral = new GastosGeneral();
                    try
                    {
                        _gastoGral.l_Producto = ph.BuscarProductosVentas(nombre);
                        _totalParcial = 0;
                        if (_editar == 0)
                        {
                            return View("CrearCompraPV", _gastoGral);
                        }
                        else
                        {
                            return View("EditarCompraBazar", _gastoGral);
                        }
                    }
                    catch (Exception e)
                    {
                        _mensajeError = e.Message;
                        return RedirectToAction("CrearCompraPV");
                    }
                }
            }

            ViewBag.Total = "$" + _totalParcial.ToString();
            if (_editar == 1)
            {
                return View("EditarCompraBazar", _gastoGral);
            }
            else
            {
                return View("CrearCompraPV", _gastoGral);
            }
        }


        public ActionResult CrearCompraInsumo2(String cadena)
        {
            ProductosHandle ph = new ProductosHandle();
            _gastoGral.l_Producto = ph.BuscarProductosInsumo(cadena);
            if (_gastoGral.l_Producto.Count() == 0)
            {
                List<LineaGastos> lc = _gastoGral.l_LineaCompras;
                _gastoGral = new GastosGeneral();
                _gastoGral.l_LineaCompras = lc;
                _vacia = "No existe el insumo " + cadena;
                ViewBag.vacia = _vacia;
            }
            _vacia = "";
            ViewBag.Total = "$" + _total.ToString();
            return View("CrearCompraInsumo", _gastoGral);
        }

        public ActionResult CrearCompraInsumo()
        {
            _gastoGral = new GastosGeneral();
            return View("CrearCompraInsumo", _gastoGral);
        }

        public ActionResult CrearCompraPV(int control)
        {
            _totalParcial = 0;
            _control = control;
            _gastoGral = new GastosGeneral();
            ProductosHandle ph = new ProductosHandle();

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }
            try
            {
                _listProductos = ph.ListarProductosBazar();
                ViewBag.control = _control;
                if (_listProductos.Count() == 0)
                {
                    ViewBag.mensajeInicio = "No hay productos cargados para realizar una compra.";
                }
                _gastoGral.l_Producto = ph.ListarProductosBazar();
                return View("CrearCompraPV", _gastoGral);

            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Caja", "Gastos");
            }
        }

        public ActionResult CrearCompraPV2(String cadena)
        {
            ProductosHandle ph = new ProductosHandle();
            try
            {
                if (ph.BuscarProductosVentas(cadena).Count() == 0)
                {
                    ViewBag.vacia = "No Existe el producto: " + cadena;
                }
                else
                {
                    _gastoGral.l_Producto = ph.BuscarProductosVentas(cadena);
                }

                ViewBag.Total = "$" + _totalParcial.ToString();
                return View("CrearCompraPV", _gastoGral);

            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("CrearCompraPV");
            }
        }

        // GET: Gastos/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        [HttpPost]
        // GET: Gastos/Create
        public ActionResult NuevaCompraInsumo(String usuario)
        {
            _total = 0;
            try
            {
                GastosHandle gh = new GastosHandle();
                gh.AgregarCompra(_gastoGral.l_LineaCompras, usuario);
                _mensajeExito = "Compra realizada exitosamente";
                return RedirectToAction("Insumos", "Productos");
            }
            catch (Exception e)
            {
                _gastoGral = new GastosGeneral();
                _mensajeError = e.Message;
                return View("ComprasInsumos");
            }
        }

        [HttpPost]
        public ActionResult NuevaCompraBazar(String usuario)
        {
            var controller = "";
            if (_control == 1)
            { controller = "Caja"; }
            else
            {
                controller = "ComprasProdVentas";
            }

            try
            {
                GastosHandle gh = new GastosHandle();
                if (gh.AgregarCompraBazar(_gastoGral.l_LineaCompras, usuario))
                {
                    _mensajeExito = "Compra realizada exitosamente";
                }
                _totalParcial = 0;
                return RedirectToAction(controller);
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return View("CrearCompraPV");
            }
        }
        public ActionResult BuscarGasto(string fecha, int idTipoGasto = 0)
        {
            TipoGastosHandle tgh = new TipoGastosHandle();
            DateTime fecha1 = new DateTime();
            DateTime fecha2 = new DateTime();
            string[] f1 = fecha.Split(' ');

            var fec1 = f1[0];
            var fec2 = f1[2];

            if (cultura == "en")
            {
                string[] fech = fec1.Split('/');
                var dd = fech[0];
                var mm = fech[1];
                var yy = fech[2];
                var fech1 = mm + "/" + dd + "/" + yy;

                string[] fech_2 = fec2.Split('/');
                var dd2 = fech_2[0];
                var mm2 = fech_2[1];
                var yy2 = fech_2[2];
                var fech2 = mm2 + "/" + dd2 + "/" + yy2;

                fecha1 = Convert.ToDateTime(fech1);
                fecha2 = Convert.ToDateTime(fech2);
            }
            else
            {
                fecha1 = Convert.ToDateTime(fec1);
                fecha2 = Convert.ToDateTime(fec2);
            }
            fecha2 = fecha2.AddHours(23).AddMinutes(59).AddSeconds(59);
            GastosHandle gh = new GastosHandle();

            try
            {
                if (idTipoGasto == 0)
                {
                    _list = gh.BuscarGasto(fecha1, fecha2).Concat(gh.BuscarCompraBazar(fecha1, fecha2)).OrderByDescending(p => p.FechaGasto).ToList();
                    if (_list.Count() == 0)
                    {
                        // _list = null;
                        ViewBag.listaTipo = tgh.ListarTipoGastos();
                        ViewBag.fechas = "No hay egresos entre " + fecha1.ToString("dd/MM/yyyy") + " - " + fecha2.ToString("dd/MM/yyyy");
                        return View("Gastos", _list);
                    }
                    ViewBag.listaTipo = tgh.ListarTipoGastos();
                    _total2 = _list.Sum(p => p.Total);
                    ViewBag.total = _total2;
                    _mensaje = "Egresos entre  " + fecha1.ToString("dd/MM/yyyy") + " y " + fecha2.ToString("dd/MM/yyyy");
                    ViewBag.fechas = _mensaje;
                    return View("Gastos", _list);
                }
                else
                {
                    if (idTipoGasto == 3)
                    {
                        //  var lista = gh.BuscarCompraBazar(fecha1, fecha2).OrderByDescending(p => p.FechaGasto);
                        _list = gh.BuscarCompraBazar(fecha1, fecha2).OrderByDescending(p => p.FechaGasto).ToList();
                        ViewBag.listaTipo = tgh.ListarTipoGastos();
                        _total2 = _list.Sum(p => p.Total);
                        ViewBag.total = _total2;
                        _mensaje = "Egresos entre  " + fecha1.ToString("dd/MM/yyyy") + " y " + fecha2.ToString("dd/MM/yyyy");
                        ViewBag.fechas = _mensaje;
                        return View("Gastos", _list);
                    }
                    else
                    {
                        // var lista = gh.BuscarGasto2(fecha1, fecha2, idTipoGasto).OrderByDescending(p => p.FechaGasto);
                        _list = gh.BuscarGasto2(fecha1, fecha2, idTipoGasto).OrderByDescending(p => p.FechaGasto).ToList();
                        ViewBag.listaTipo = tgh.ListarTipoGastos();
                        _total2 = _list.Sum(p => p.Total);
                        ViewBag.total = _total2;
                        _mensaje = "Egresos entre  " + fecha1.ToString("dd/MM/yyyy") + " y " + fecha2.ToString("dd/MM/yyyy");
                        ViewBag.fechas = _mensaje;
                        return View("Gastos", _list);
                    }
                }
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Gastos");
            }
        }

        public ActionResult BuscarCompraInsumo(DateTime fecha)
        {
            GastosHandle gh = new GastosHandle();
            return View("ComprasInsumos", gh.BuscarCompraInsumo(fecha));
        }


        public ActionResult BuscarCompraBazar(string fecha)
        {
            DateTime fecha1 = new DateTime();
            DateTime fecha2 = new DateTime();
            String[] f1;
            f1 = fecha.Split(' ');
            var fec1 = f1[0];
            var fec2 = f1[2];

            if (cultura == "en")
            {
                string[] fech = fec1.Split('/');
                var dd = fech[0];
                var mm = fech[1];
                var yy = fech[2];
                var fech1 = mm + "/" + dd + "/" + yy;

                string[] fech_2 = fec2.Split('/');
                var dd2 = fech_2[0];
                var mm2 = fech_2[1];
                var yy2 = fech_2[2];
                var fech2 = mm2 + "/" + dd2 + "/" + yy2;

                fecha1 = Convert.ToDateTime(fech1);
                fecha2 = Convert.ToDateTime(fech2);
            }
            else
            {
                fecha1 = Convert.ToDateTime(fec1);
                fecha2 = Convert.ToDateTime(fec2);
            }
            fecha2 = fecha2.AddHours(23).AddMinutes(59).AddSeconds(59);
            GastosHandle gh = new GastosHandle();
            try
            {
                if (gh.BuscarCompraBazar(fecha1, fecha2).Count() == 0)
                {
                    ViewBag.mensajeFecha = "No se registraron compras entre las fechas " + fecha1.ToString("dd/MM/yyyy") + " - " + fecha2.ToString("dd/MM/yyyy");
                    ViewBag.mensajeInicio = "";
                }
                else
                {
                    ViewBag.mensajeFecha = "Compras realizadas entre las fechas " + fecha1.ToString("dd/MM/yyyy") + " - " + fecha2.ToString("dd/MM/yyyy");
                }
                return View("ComprasProdVentas", gh.BuscarCompraBazar(fecha1, fecha2));
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("ComprasProdVenta", "Gastos");
            }
        }

        public ActionResult PagarGasto(string monto, int idTipoG, string nombre, string usuario, string fecha)
        {
            DateTime fe;
            if (cultura == "en")
            {
                string[] fech = fecha.Split('/');
                var dd = fech[0];
                var mm = fech[1];
                var yy = fech[2];
                var fech1 = mm + "/" + dd + "/" + yy;

                fe = Convert.ToDateTime(fech1);
            }
            else
            {
                fe = Convert.ToDateTime(fecha);
            }

            String[] m2;
            m2 = monto.Split('.');

            if (m2.Length == 2)
            {
                monto = m2[0] + "," + m2[1];
            }

            try
            {
                GastosHandle gh = new GastosHandle();
                if (gh.PagarGasto(usuario, idTipoG, Convert.ToDecimal(monto), nombre, fe))
                {
                    _mensajeExito = "Pago realizado exitosamente";
                }

                return RedirectToAction("NuevoGasto", "Gastos");
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return View("NuevoGasto");
            }

        }
        // GET: Gastos/Edit/5
        public ActionResult EditarCompraBazar(int idGasto)
        {
            _idgasto = idGasto;
            _editar = 1;
            ProductosHandle ph = new ProductosHandle();
            GastosHandle gh = new GastosHandle();
            _gastoGral = new GastosGeneral();
            try
            {
                var gasto = gh.DetalleCompraBazar(idGasto);
                _gastoGral.l_LineaCompras = gasto;
                _gastoGral.l_Producto = ph.ListarProductosBazar();
                _totalParcial = gasto.Sum(x => x.SubTotal);
                ViewBag.Total = _totalParcial;
                return View(_gastoGral);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("ComprasProdVentas");
            }
        }

        // POST: Gastos/Edit/5
        [HttpPost]
        public ActionResult EditCompraBazar(string usuario)
        {
            try
            {
                GastosHandle gh = new GastosHandle();
                gh.EditarCompraBazar(_gastoGral.l_LineaCompras, usuario, _idgasto);
                _mensajeExito = "Compra modificada exitosamente";
                return RedirectToAction("ComprasProdVentas");
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return View("CrearCompraPV");
            }
        }

        // GET: Gastos/Delete/5
        public ActionResult BorrarCompra(int idGasto)
        {
            GastosHandle gh = new GastosHandle();
            try
            {
                gh.BorrarCompraBazar(idGasto);
                _mensajeExito = "Compra borrada exitosamente";
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
            }
            return RedirectToAction("ComprasProdVentas");
        }

        // POST: Gastos/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult CrearGasto()
        {
            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }
            _mensajeError = "";

            try
            {
                TipoGastosHandle tgh = new TipoGastosHandle();
                ViewBag.listaTipo = tgh.ListarTipoGastos2();
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return View();
            }

            return View();
        }

        [HttpPost]
        public ActionResult InsertarGasto(string gasto, string usuario, string monto, string fecha, int idTipoGasto = 0, string nuevoTipo = "")
        {
            String[] m2;
            m2 = monto.Split('.');

            if (m2.Length == 2)
            {
                monto = m2[0] + "," + m2[1];
            }

            var monto2 = Convert.ToDecimal(monto.Replace(',', '.'), CultureInfo.InvariantCulture);

            string[] fech = fecha.Split('/');
            var dd = fech[0];
            var mm = fech[1];
            var yy = fech[2];
            var fech1 = mm + "/" + dd + "/" + yy;

            var fechaD = Convert.ToDateTime(fech1);

            GastosHandle gh = new GastosHandle();
            TipoGastosHandle tgh = new TipoGastosHandle();
            try
            {
                if (nuevoTipo == "")
                {
                    gh.PagarGasto(usuario, idTipoGasto, monto2, gasto, fechaD);
                }
                else
                {
                    if (tgh.EsTipoGastoRepetido(nuevoTipo))
                    {
                        _mensajeError = "Ya existe un tipo de gasto con ese nombre";
                        return RedirectToAction("NuevoGasto", "Gastos");
                    }
                    else
                    {
                        gh.PagarGastoConNuevoTipo(gasto, monto2, usuario, nuevoTipo, fechaD);
                    }
                }

                _mensajeExito = "Gasto pagado exitosamente";
                return RedirectToAction("Caja", "Gastos");
            }

            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("CrearGasto");
            }
        }

        public ActionResult Caja()
        {
            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }

            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }
            _mensajeError = "";
            _mensajeExito = "";
            var fecha = DateTime.UtcNow;

            var fecha2 = new DateTime(fecha.AddMonths(-1).Year , fecha.AddMonths(-1).Month, 01);
            var fecha1 =  DateTime.UtcNow.Date.AddHours(23).AddMinutes(59).AddSeconds(58);
            PagosHandle ph = new PagosHandle();
            VentasHandle vh = new VentasHandle();
            GastosHandle gh = new GastosHandle();
            try
            {
                var listaGastosCaja = gh.ListarGastosCajaPorRangoFecha(fecha2, fecha1).ToList();
                var listaPagosCaja = ph.ListarPagosCajaPorRangoFecha(fecha2, fecha1).ToList();
                var listaVentasCaja = vh.ListarVentasPorRangoFecha(fecha2, fecha1).ToList();
                var listaIngresosLibreCaja = vh.ListarIngresosLibrePorFecha(fecha2, fecha1);
                _list2 = listaGastosCaja.Union(listaVentasCaja).Union(listaPagosCaja).Union(listaIngresosLibreCaja).OrderByDescending(p => p.Fecha).ToList();

                _totalGastos = listaGastosCaja.Sum(p => p.Monto);
                _totalVentas = listaVentasCaja.Sum(p => p.Monto);
                _totalPagos = listaPagosCaja.Sum(p => p.Monto);
                _totalIngresosLibres = listaIngresosLibreCaja.Sum(p => p.Monto);
                ViewBag.totalGastos = _totalGastos;
                // ViewBag.totalIngresoslibres = _totalIngresosLibres;
                ViewBag.totalVentas = _totalVentas + _totalPagos + _totalIngresosLibres;
                ViewBag.total = _totalPagos + _totalVentas - _totalGastos + _totalIngresosLibres;

                if (_list2.Count() == 0)
                {
                    ViewBag.mensajeInicio = "No hay movimientos de caja actualmente";
                    _mensaje = "";
                }
                else
                {
                    ViewBag.mensaje = "Caja entre fechas " + fecha2.ToString("dd/MM/yyyy") + " - " + fecha.ToString("dd/MM/yyyy");
                }
                return View(_list2);
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = "Ocurrio un problema inesperado, intente nuevamente";
                return View();
            }
        }

        public ActionResult Caja2(string fecha)
        {
            PagosHandle ph = new PagosHandle();
            GastosHandle gh = new GastosHandle();
            VentasHandle vh = new VentasHandle();
            DateTime fecha1 = new DateTime();
            DateTime fecha2 = new DateTime();
            String[] f1;
            f1 = fecha.Split(' ');
            var fec1 = f1[0];
            var fec2 = f1[2];
            var fe1 = fec1.Split('/');
            var fe2 = fec2.Split('/');

            fecha1 = Convert.ToDateTime(fe1[1] + "/" + fe1[0] + "/" + fe1[2]);
            fecha2 = Convert.ToDateTime(fe2[1] + "/" + fe2[0] + "/" + fe2[2]);
            fecha2 = fecha2.AddHours(23).AddMinutes(59).AddSeconds(59);

            try
            {
                var listaGastosPorFecha = gh.ListarGastosCajaPorRangoFecha(fecha1, fecha2).ToList();
                var listaPagosPorFecha = ph.ListarPagosCajaPorRangoFecha(fecha1, fecha2).ToList();
                var listadoVentasPorFecha = vh.ListarVentasPorRangoFecha(fecha1, fecha2).ToList();
                var listadoIngresosLibrePorFecha = vh.ListarIngresosLibrePorFecha(fecha1, fecha2);
                _list2 = listaGastosPorFecha.Union(listaPagosPorFecha).Union(listadoVentasPorFecha).Union(listadoIngresosLibrePorFecha).OrderByDescending(p => p.Fecha).ToList();
                _totalGastos = listaGastosPorFecha.Sum(p => p.Monto);
                _totalIngresosLibres = listadoIngresosLibrePorFecha.Sum(p => p.Monto);
                _totalVentas = listadoVentasPorFecha.Sum(p => p.Monto);
                _totalPagos = listaPagosPorFecha.Sum(p => p.Monto);
                _total = _totalPagos + _totalVentas - _totalGastos + _totalIngresosLibres;
                ViewBag.totalGastos = _totalGastos;
                ViewBag.totalVentas = _totalVentas + _totalIngresosLibres + _totalPagos;
                // ViewBag.totalPagos = _totalPagos;
                ViewBag.total = _total;

                if (_list2.Count() == 0)
                {
                    _mensaje = "No hay movimientos de caja entre las fechas " + fecha1.ToString("dd/MM/yyyy") + " - " + fecha2.ToString("dd/MM/yyyy");
                }
                else
                {
                    _mensaje = "Caja entre fechas " + fecha1.ToString("dd/MM/yyyy") + " - " + fecha2.ToString("dd/MM/yyyy");
                }
                ViewBag.mensaje = _mensaje;
                return View("Caja", _list2);
            }

            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Caja", "Gastos");
            }
        }

        public ActionResult GenerarExcelCaja()
        {
            if (_list2.Count() == 0)
            {
                _mensajeError = "No es posible imprimir un informe vacio. Por favor generelo otra vez.";
                return RedirectToAction("Caja");
            }


            ExcelPackage ExcelPkg = new ExcelPackage();
            ExcelWorksheet Sheet1 = ExcelPkg.Workbook.Worksheets.Add("Sheet1");
            using (ExcelRange Rng = Sheet1.Cells[1, 1, 1, 1])
            {
                Rng.Value = _mensaje;
                Sheet1.Cells[3, 1].Value = "Fecha";
                Sheet1.Cells[3, 2].Value = "Usuario";
                Sheet1.Cells[3, 3].Value = "Concepto";
                Sheet1.Cells[3, 4].Value = "Ingresos";
                Sheet1.Cells[3, 5].Value = "Egresos";
                Sheet1.Cells["A3:F3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet1.Cells["A3:F3"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                Sheet1.Cells["A3:F3"].Style.Font.Bold = true;
                Rng.Style.Font.Size = 12;
                Rng.Style.Font.Bold = true;
            }

            var i = 4;
            foreach (var item in _list2)
            {
                Sheet1.Cells[i, 1].Value = item.Fecha.ToShortDateString();
                Sheet1.Cells[i, 2].Value = item.Usuario;
                Sheet1.Cells[i, 3].Value = item.Concepto;
                if (item.Concepto2 == "Cuota" || item.Concepto2 == "Inscripcion" || item.Concepto2 == "Venta bazar" || item.Concepto2 == "Ingreso libre")
                {
                    Sheet1.Cells[i, 4].Value = item.Monto;
                    Sheet1.Cells[i, 5].Value = "-";
                }
                else
                {
                    Sheet1.Cells[i, 4].Value = "-";
                    Sheet1.Cells[i, 5].Value = item.Monto;
                }

                i++;
            }
            //Sheet1.Cells[i, 5].Value = _total2;
            Sheet1.Cells[i, 1].Style.Font.Bold = true;
            Sheet1.Cells[i, 5].Style.Font.Bold = true;

            Sheet1.Cells["H3"].Value = "Ventas bazar: " + " $" + _totalVentas;
            Sheet1.Cells["H4"].Value = "Ingresos cursos: " + " $" + _totalPagos;
            Sheet1.Cells["H5"].Value = "Gastos: " + " $" + _totalGastos;
            Sheet1.Cells["H6"].Value = "Total bruto: " + " $" + _total;

            Sheet1.Protection.IsProtected = false;
            Sheet1.Protection.AllowSelectLockedCells = false;
            Sheet1.Column(1).Width = 20;
            Sheet1.Column(2).Width = 35;
            Sheet1.Column(3).Width = 25;
            Sheet1.Column(4).Width = 25;

            byte[] excelEnBytes;
            using (MemoryStream ms = new MemoryStream())
            {
                ExcelPkg.SaveAs(ms);
                excelEnBytes = ms.ToArray();
            }
            return File(excelEnBytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "InformeDeCaja.xlsx");
        }

        public ActionResult SetearMensaje(string mensaje)
        {
            _mensajeExito = mensaje;
            return RedirectToAction("Caja");
        }

        public ActionResult GenerarExcel()
        {
            if (_list.Count() == 0)
            {
                _mensajeError = "No es posible imprimir un informe vacio. Por favor generelo otra vez.";
                return RedirectToAction("Gastos");
            }
            ExcelPackage ExcelPkg = new ExcelPackage();
            ExcelWorksheet Sheet1 = ExcelPkg.Workbook.Worksheets.Add("Sheet1");
            using (ExcelRange Rng = Sheet1.Cells[1, 1, 1, 1])
            {
                Rng.Value = _mensaje;
                Sheet1.Cells[3, 1].Value = "Fecha";
                Sheet1.Cells[3, 2].Value = "Tipo de Gasto";
                Sheet1.Cells[3, 3].Value = "Gasto";
                Sheet1.Cells[3, 4].Value = "Usuario";
                Sheet1.Cells[3, 5].Value = "Monto";
                Sheet1.Cells["A3:E3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet1.Cells["A3:E3"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                Sheet1.Cells["A3:E3"].Style.Font.Bold = true;
                Rng.Style.Font.Size = 12;
                Rng.Style.Font.Bold = true;
            }

            var i = 4;
            foreach (var item in _list)
            {
                Sheet1.Cells[i, 1].Value = item.FechaGasto.ToShortDateString();
                Sheet1.Cells[i, 2].Value = item.Tipo;
                Sheet1.Cells[i, 3].Value = item.Nombre;
                Sheet1.Cells[i, 4].Value = item.Usuario;
                Sheet1.Cells[i, 5].Value = item.Total;
                i++;
            }

            Sheet1.Cells[i, 1].Value = "Total";
            Sheet1.Cells[i, 5].Value = _total2;
            Sheet1.Cells[i, 1].Style.Font.Bold = true;
            Sheet1.Cells[i, 5].Style.Font.Bold = true;
            Sheet1.Protection.IsProtected = false;
            Sheet1.Protection.AllowSelectLockedCells = false;
            Sheet1.Column(1).Width = 20;
            Sheet1.Column(2).Width = 30;
            Sheet1.Column(3).Width = 25;
            Sheet1.Column(4).Width = 25;
            Sheet1.Column(5).Width = 15;

            byte[] excelEnBytes;
            using (MemoryStream ms = new MemoryStream())
            {
                ExcelPkg.SaveAs(ms);
                excelEnBytes = ms.ToArray();
            }
            return File(excelEnBytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "InformeDeEgresos.xlsx");
        }

        public ActionResult cuentas()
        {
            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }
            _mensajeError = "";
            _mensajeExito = "";

            GastosHandle gh = new GastosHandle();
            var fecha = DateTime.UtcNow;
            var fecha1 = new DateTime(fecha.AddMonths(-1).Year, fecha.AddMonths(-1).Month, 01);
            var fecha2 = DateTime.UtcNow.Date.AddHours(23).AddMinutes(59).AddSeconds(58);

            var listaCuenta = gh.listaCuentas(fecha2, fecha1);

            if (listaCuenta.Count() == 0)
            {
                ViewBag.mensajeInicio = "No hay pagos de cuentas registrados actualmente";
                _mensaje = "";
            }
            else
            {
                ViewBag.mensajeFecha = "Caja entre fechas " + fecha2.ToString("dd/MM/yyyy") + " - " + fecha.ToString("dd/MM/yyyy");
            }

            try
            {

                return View(listaCuenta);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("caja");
            }
        }

        public ActionResult BorrarCuenta(int idgasto)
        {
            try
            {
                GastosHandle gh = new GastosHandle();
                gh.borrarCuenta(idgasto);
                _mensajeExito = "cuenta borrada exitosamente";
                return RedirectToAction("cuentas");
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("cuentas");
            }
        }
    }
}
