﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Proyecto_1.Models;
using Proyecto_1.ViewModels;
using System.Web.Configuration;

namespace Proyecto_1.Controllers
{
    public class VentasController : Controller
    {
        private static int _control;
        private static VentaGeneral _ventaGral;
        private static string _mensajeExito;
        private static string _mensajeError;
        private static int _editar;
        private static Decimal _total = 0;
        private static int _idVenta;
        private IList<Ventas> listVentas;
        private static String total;
        private static string cultura = WebConfigurationManager.AppSettings["cultura"];


        // GET: Ventas
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Ventas()
        {
            VentasHandle vh = new VentasHandle();
            _total = 0;
            _editar = 0;
            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }
            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }

            _mensajeError = "";
            _mensajeExito = "";
            try
            {
                listVentas = vh.ListarVentas();
                if (listVentas.Count() == 0)
                {
                    ViewBag.mensajeInicio = "No se registraron ventas actualmente";
                    ViewBag.mensajeFecha = "";
                }
                else
                {
                    ViewBag.mensajeFecha = "Lista de ventas realizadas";
                }
                return View(listVentas);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return View("Ventas");
            }
        }

        public ActionResult CrearVenta(int control)
        {
            _control = control;
            _ventaGral = new VentaGeneral();
            ProductosHandle ph = new ProductosHandle();
            try
            {

                if (ph.ListarProductosBazar().Count() == 0)
                {
                    ViewBag.mensajeInicio = "No hay productos agregados actualmente";
                }
                _ventaGral.l_Producto = ph.ListarProductosBazar();
                return View("CrearVenta", _ventaGral);
            }
            catch (Exception e)
            {
                return RedirectToAction("", "");
            }
        }

        public ActionResult IngresoLibre()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("", "");
            }
        }

        public ActionResult CrearVenta2(String cadena)
        {
            ProductosHandle ph = new ProductosHandle();
            try
            {

                if (ph.BuscarProductosVentas(cadena).Count() == 0)
                {
                    ViewBag.mensajeError = "No existe el producto: " + cadena;
                }
                else
                {
                    _ventaGral.l_Producto = ph.BuscarProductosVentas(cadena);
                }

                ViewBag.Total = "$" + _total.ToString();
                return View("CrearVenta", _ventaGral);
            }

            catch (Exception e)
            {
                _mensajeError = e.Message;
                return View("CrearVenta");
            }
        }

        public ActionResult AgregarLinea(int idproducto, String nombre, Decimal cantidad, Decimal precio, int stock)
        {
            var vista = "";
            ViewBag.control = _control;
            if (_editar == 1)
            {
                vista = "EditarVenta";
            }
            else
            {
                vista = "CrearVenta";
            }

            if (cantidad > stock)
            {
                ViewBag.mensajeError = "La cantidad ingresada supera el stock actual";
                ViewBag.Total = '$' + _total.ToString();
                return View(vista, _ventaGral);
            }

            if (_ventaGral.l_LineaVenta == null)
            {
                _ventaGral.l_LineaVenta = new List<LineaVentas>();
            }


            if (_ventaGral.l_LineaVenta.Find(x => x.idProducto == idproducto) != null)
            {
                _ventaGral.l_Producto.Find(x => x.idProducto == idproducto).Stock -= cantidad;
                _ventaGral.l_LineaVenta.Find(x => x.idProducto == idproducto).Cantidad += cantidad;
                _ventaGral.l_LineaVenta.Find(x => x.idProducto == idproducto).SubTotal = _ventaGral.l_LineaVenta.Find(x => x.idProducto == idproducto).Cantidad * _ventaGral.l_LineaVenta.Find(x => x.idProducto == idproducto).Precio;
                _total = _total + (cantidad * precio);
            }
            else
            {
                _ventaGral.l_Producto.Find(x => x.idProducto == idproducto).Stock -= cantidad;
                LineaVentas lv = new LineaVentas(idproducto, precio, cantidad, nombre, precio * cantidad);
                _ventaGral.l_LineaVenta.Add(lv);
                _total = _total + (precio * cantidad);
            }

            total = _total.ToString();
            ViewBag.Total = "$" + total;
            return View(vista, _ventaGral);
        }

        public ActionResult RestarLinea(String nombre, int idProd, Decimal precio, int cantidad)
        {
            var vista = "";
            if (_editar == 1)
            {
                vista = "EditarVenta";
            }
            else
            {
                vista = "CrearVenta";
            }
            try
            {
                if (_ventaGral.l_LineaVenta.Find(x => x.idProducto == idProd) != null)
                {
                    _ventaGral.l_Producto.Find(x => x.idProducto == idProd).Stock += cantidad;
                    _ventaGral.l_LineaVenta.Remove(_ventaGral.l_LineaVenta.Find(x => x.idProducto == idProd));
                    _total = _total - (cantidad * precio);

                    if (_ventaGral.l_LineaVenta.Count == 0)
                    {
                        _total = 0;
                    }
                    total = "$" + _total.ToString();
                    ViewBag.Total = total;
                }
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
            }
            ViewBag.control = _control;
            return View(vista, _ventaGral);
        }

        [HttpPost]
        public ActionResult NuevaVenta(String usuario)
        {
            _total = 0;
            try
            {
                VentasHandle vh = new VentasHandle();
                vh.AgregarVenta(_ventaGral.l_LineaVenta, usuario);
                if (_control == 1)
                {
                    return RedirectToAction("SetearMensaje", "Gastos", new { mensaje = "Venta realizada exitosamente" });
                }
                else {
                    _mensajeExito = "VentaRealizada exitosamente";
                    return RedirectToAction("Ventas", "Ventas");
                }
                
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return View("CrearVenta");
            }
        }

        [HttpPost]
        public ActionResult NuevoIngresoLibre(string usuario, string descripcion, int monto, string fecha)
        {
            VentasHandle vh = new VentasHandle();
            try
            {
                string fecha2;
                if (cultura == "en")
                {
                    string[] f1 = fecha.Split('/');
                    var dd = f1[0];
                    var mm = f1[1];
                    var yy = f1[2];
                    fecha2 = mm + "/" + dd + "/" + yy;
                }
                else
                {
                    fecha2 = fecha;
                }

                vh.NuevoIngresoLibre(usuario, descripcion, monto, fecha2);
                return RedirectToAction("SetearMensaje", "Gastos", new { mensaje= "Ingreso libre pagado exitosamente"});
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return View("CrearVenta");
            }
        }


        // GET: Ventas/Edit/5
        public ActionResult DetalleVenta(int id, DateTime fecha)
        {
            VentasHandle vh = new VentasHandle();
            try
            {
                ViewBag.Fecha = fecha.ToString("dd/MM/yyyy");
                var total = vh.DetalleVenta(id).Sum(x => x.SubTotal);
                ViewBag.Total = total;
                return View(vh.DetalleVenta(id));
            }

            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Ventas");
            }
        }

        public ActionResult BuscarVenta(string fecha)
        {
            DateTime fecha1 = new DateTime();
            DateTime fecha2 = new DateTime();
            String[] f1;
            f1 = fecha.Split(' ');
            var fec1 = f1[0];
            var fec2 = f1[2];

            if (cultura == "en")
            {
                string[] fech = fec1.Split('/');
                var dd = fech[0];
                var mm = fech[1];
                var yy = fech[2];
                var fech1 = mm + "/" + dd + "/" + yy;

                string[] fech_2 = fec2.Split('/');
                var dd2 = fech_2[0];
                var mm2 = fech_2[1];
                var yy2 = fech_2[2];
                var fech2 = mm2 + "/" + dd2 + "/" + yy2;

                fecha1 = Convert.ToDateTime(fech1);
                fecha2 = Convert.ToDateTime(fech2);
            }
            else
            {
                fecha1 = Convert.ToDateTime(fec1);
                fecha2 = Convert.ToDateTime(fec2);
            }
            fecha2 = fecha2.AddHours(23).AddMinutes(59).AddSeconds(59);

            VentasHandle vh = new VentasHandle();

            try
            {
                listVentas = vh.BuscarVenta(fecha1, fecha2);
                if (listVentas.Count() == 0)
                {
                    ViewBag.mensajeFecha = "No se realizaron ventas entre las fechas " + fecha1.ToString("dd/MM/yyyy") + " - " + fecha2.ToString("dd/MM/yyyy");
                    ViewBag.mensajeInicio = "";
                }
                else
                {
                    ViewBag.mensajeFecha = "Ventas realizadas entre las fechas " + fecha1.ToString("dd/MM/yyyy") + " - " + fecha2.ToString("dd/MM/yyyy");
                }
                return View("Ventas", listVentas);
            }

            catch (Exception e)
            {
                _mensajeError = e.Message;
                return View("Ventas");
            }
        }

        // GET: Ventas/Delete/5
        public ActionResult BorrarVenta(int idVenta)
        {
            VentasHandle vh = new VentasHandle();
            try
            {
                vh.BorrarVenta(idVenta);
                _mensajeExito = "Venta borrarda exitosamente";
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
            }
            return RedirectToAction("Ventas");
        }

        public ActionResult EditarVenta(int idVenta)
        {
            _idVenta = idVenta;
            _editar = 1;
            VentasHandle vh = new VentasHandle();
            ProductosHandle ph = new ProductosHandle();
            _ventaGral = new VentaGeneral();
            try
            {
                var venta = vh.DetalleVenta(idVenta);
                _ventaGral.l_LineaVenta = venta;
                _ventaGral.l_Producto = ph.ListarProductosBazar();
                _total = venta.Sum(x => x.SubTotal);
                ViewBag.Total = _total;
                return View(_ventaGral);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Ventas");
            }
        }

        // POST: Ventas/Delete/5
        [HttpPost]
        public ActionResult EditVenta(string usuario)
        {
            try
            {
                VentasHandle vh = new VentasHandle();
                vh.EditarVenta(_ventaGral.l_LineaVenta, usuario, _idVenta);
                _mensajeExito = "Venta modificada exitosamente";
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
            }
            return RedirectToAction("Ventas");
        }
    }
}
