﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_1.Models;
using Proyecto_1.ViewModels;

namespace Proyecto_1.Controllers
{
    public class ProductosController : Controller
    {

        private static String _mensajeExito;
        private static String _mensajeError;
        private static String _productoModificar;
        private static String _vacia;
       
        // GET: Productos
        public ActionResult Index()
        {
            return View("index");
        }

        public ActionResult Insumos()
        {
            ProductosHandle ph = new ProductosHandle();

            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }

            _mensajeExito = "";
            _mensajeError = "";

            return View(ph.ListarProductosInsumos());
        }


        public ActionResult ProductosBazar()
        {
            ProductosHandle ph = new ProductosHandle();
         
            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }

            _mensajeExito = "";
            _mensajeError = "";
            return View(ph.ListarProductosBazar());

        }

        [HttpPost]
        public ActionResult BuscarPV(string cadena)
        {
            ProductosHandle ph = new ProductosHandle();
            try {
                if (ph.BuscarProductosVentas(cadena).Count() == 0)
                {
                    _vacia = "No existe el producto " + cadena;
                    ViewBag.Vacia = _vacia;
                }
                _vacia = "";
                return View("ProductosBazar", ph.BuscarProductosVentas(cadena));
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("ProductosBazar");
            }
           
        }

        // GET: Productos/Details/5
        public ActionResult ListProductos()
        {
            ProductosHandle ph = new ProductosHandle();
            return View(ph.ListarProductosInsumos());
        }

    
        // GET: Productos/Create

            public ActionResult CorregirStock(int idproducto, string StockReal, int stock ,string nombre, string unidad)
        {
            var stockr = Convert.ToInt32(StockReal);
            ProductosHandle ph = new ProductosHandle();

            if (stockr < stock)
            {
                try
                {
                    VentasHandle vh = new VentasHandle();
                    if (vh.RestarCorreccion(idproducto,  stock- stockr, nombre, unidad))
                    {
                        _mensajeExito = "Se corrigio el stock exitosamente";
                        ViewBag.MensajeExito = _mensajeExito;
                    }

                    return View("Insumos", ph.ListarProductosInsumos());
                }
                catch (Exception e)
                {
                    _mensajeError = e.Message;
                    ViewBag.MensajeError = _mensajeError;
                    return RedirectToAction("Insumos", "Productos");
                }
            }
            else
            {
                try
                {
                    GastosHandle gh = new GastosHandle();
                    if (gh.SumarCorreccion(idproducto, stockr-stock, nombre, unidad))
                    {
                        _mensajeExito = "Se corrigio el stock exitosamente";
                        ViewBag.MensajeExito = _mensajeExito;
                    }

                    return View("Insumos", ph.ListarProductosInsumos());
                }
                catch (Exception e)
                {
                    _mensajeError = e.Message;
                    ViewBag.MensajeError = _mensajeError;
                    return RedirectToAction("Insumos", "Productos");
                }
            }
        }

        public ActionResult Productos_formView()
        {
            return View();
        }

        public ActionResult ProductosBazar_formView()
        {
            return View();
        }

        // POST: Productos/Create
        [HttpPost]
        public ActionResult CrearProductoInsumo(Productos P)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ProductosHandle ph = new ProductosHandle();
                    if (!ph.EsInsumoRepetido(P.Nombre))
                    {
                        if (!ph.AgregarProductoInsumo(P))
                        {
                            _mensajeExito = "Insumo creado exitosamente";
                            ModelState.Clear();
                        }
                    }
                    else
                    {
                        ViewBag.mensajeError = "Ya existe un insumo con ese nombre: " + P.Nombre;
                        return View("Productos_formView");
                    }
                }
                return RedirectToAction("Insumos");
            }
            catch (Exception e)
            {
                ViewBag.Message = e.Message;
                return View("Productos_formView");
            }
        }


        [HttpPost]
        public ActionResult CrearProductoBazar(Productos P)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ProductosHandle ph = new ProductosHandle();
                    if (!ph.EsProductoRepetido(P.Nombre))
                    {
                        if (!ph.AgregarProductoBazar(P))
                        {
                            _mensajeExito = "Producto de Bazar creado exitosamente";
                            ModelState.Clear();
                        }
                    }
                    else
                    {
                        ViewBag.mensajeError = "Ya existe un producto de bazar con ese nombre: " + P.Nombre;
                        return View("ProductosBazar_formView");
                    }
                }
                return RedirectToAction("ProductosBazar");
            }
            catch (Exception e)
            {
                ViewBag.Message = e.Message;
                return View("Productos_formView");
            }
        }

        // GET: Productos/Edit/5
        public ActionResult EditProductoInsumo(int id)
        {
            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }

            _mensajeExito = "";
            _mensajeError = "";

            ProductosHandle ph = new ProductosHandle();
            try
            {
                ViewBag.Unidad = ph.ListarInsumos().Find(x => x.idProducto == id).Unidad;
                _productoModificar = ph.ListarProductosInsumos().Find(x => x.idProducto == id).Nombre;
                ViewBag.mensaje = _productoModificar;
                return View(ph.ListarProductosInsumos().Find(amodel => amodel.idProducto == id));
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Insumos");
            }
        }

        public ActionResult EditProductoBazar(int id)
        {

            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }

            _mensajeExito = "";
            _mensajeError = "";

            try
            {
                ProductosHandle ph = new ProductosHandle();
                _productoModificar = ph.ListarProductosBazar().Find(x => x.idProducto == id).Nombre;
                ViewBag.mensaje = _productoModificar;
                return View(ph.ListarProductosBazar().Find(amodel => amodel.idProducto == id));
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("ProductosBazar");
            }
        }

        [HttpPost]
        public ActionResult BuscarProdInsumo(string cadena)
        {
            ProductosHandle ph = new ProductosHandle();

            try
            {
                if (ph.BuscarProductosInsumo(cadena).Count() == 0)
                {
                    _vacia = "No existe el insumo " + cadena;
                    ViewBag.Vacia = _vacia;
                }
                _vacia = "";
                return View("Insumos", ph.BuscarProductosInsumo(cadena));
            }

            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Insumos");
            }
        }

        
        // POST: Productos/Edit/5
        [HttpPost]
        public ActionResult EditPB(Productos p)
        {
            try
            {
                ProductosHandle ph = new ProductosHandle();
                if (ph.EsProductoRepetido(p.Nombre) && _productoModificar!= p.Nombre)
                {
                    ViewBag.mensaje = _productoModificar;
                    ViewBag.mensajeRepetido = "El nombre ingresado ya existe";
                    return View("EditProductoBazar");
                }
                ph.EditarProductoBazar(p);
                _mensajeExito = "Producto de bazar modificado exitosamente";
                return RedirectToAction("ProductosBazar");
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("ProductosBazar");
            }
        }


        [HttpPost]
        public ActionResult EditarPI(Productos p)
        {
            try
            {
                ProductosHandle ph = new ProductosHandle();
                if (ph.EsProductoRepetido(p.Nombre) && _productoModificar!= p.Nombre)
                {
                    ViewBag.mensaje = _productoModificar;
                    ViewBag.mensajeRepetido = "el nombre ingresado ya existe";
                    return View("EditProductoInsumo");
                }
  
                if (!ph.EditarProducto(p))
                {
                    _mensajeExito = "Insumo modificado exitosamente";
                    _productoModificar = "";
                }
               
                return RedirectToAction("Insumos");
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("EditProductoInsumo");
            }
        }

        // GET: Productos/Delete/5
        public ActionResult BorrarProducto(int id)
        {
            ProductosHandle ph = new ProductosHandle();
            try
            {
                ph.BorrarProducto(id);
                _mensajeExito = "Producto borrado exitosamente";
                return RedirectToAction("ProductosBazar");
            }
            catch (Exception e)
            {
              _mensajeError = "No es posible borrar el producto";
               return RedirectToAction("ProductosBazar");
            }
            //return View();
        }
    }
}
