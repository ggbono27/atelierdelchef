﻿using Proyecto_1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using OfficeOpenXml;
using System.IO;
using System.Drawing;
using OfficeOpenXml.Style;
using System.Web.Configuration;

namespace Proyecto_1.Controllers
{
    public class AlumnoController : Controller
    {
        private static String _mensajeExito;
        private static String _mensajeError;
        private static IList<Alumno> _listaA = new List<Alumno>();
        private static IList<Alumno> _list;
        private static string _mensaje;
        private static string cultura = WebConfigurationManager.AppSettings["cultura"];


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Alumno()
        {
            AlumnoHandle ah = new AlumnoHandle();
            ModelState.Clear();

            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }

            _mensajeExito = "";
            _mensajeError = "";

            try
            {
                return View(ah.ListarAlumnos());
            }

            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Alumno");
            }
        }

        [HttpPost]
        public ActionResult Buscar(string cadena)
        {
            AlumnoHandle ah = new AlumnoHandle();
            ModelState.Clear();
            try
            {
                _list = ah.BuscarAlumno(cadena);
                if (_list.Count() == 0)
                {
                    ViewBag.mensajeError = "No existe el alumno buscado";
                }
                return View("Alumno", _list);
            }

            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Alumno");
            }
        }

        public ActionResult CrearA()
        {
            try
            {
                return View();
            }
            catch (Exception e) {
                _mensajeError = e.Message;
                return RedirectToAction("Alumno");
            }
            
        }

        [HttpPost]
        public ActionResult Agregar(Alumno alumno)
        {
            if (alumno.Direccion == null)
            {
                alumno.Direccion = "-";
            }

            if (alumno.Mail == null)
            {
                alumno.Mail = "-";
            }

            if (alumno.Telefono2 == null)
            {
                alumno.Telefono2 = "-";
            }

            if (alumno.Usuario == null)
            {
                alumno.Usuario = "-";
            }

            if (alumno.Dni == 0)
            {
                alumno.Dni = 1111111;
            }
            string[] f1;
            // f1 = alumno.fecha.Split('/');

            try
            {
                if (alumno.fecha != null)
                {
                    f1 = alumno.fecha.Split('/');
                    var fecha2 = f1[1] + "/" + f1[0] + "/" + f1[2];
                    alumno.FechaNac = Convert.ToDateTime(fecha2);
                }
                else
                {
                    alumno.FechaNac = new DateTime(1888, 01, 01);
                }
                //if (ModelState.IsValid)
                //{
               

                AlumnoHandle ah = new AlumnoHandle();
                if (!ah.AgregarAlumno(alumno))
                {
                    _mensajeExito = "Alumno creado exitosamente";
                    ModelState.Clear();
                }
                return RedirectToAction("Alumno");
                //}
                //else
                //{
                //    ViewBag.mensajeError = "Alumno no agregado. Revise los campos, aquellos marcados con (*) son obligatorios";
                //    return View("CrearA");
                //}

            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return View("CrearA");
            }
        }

        public ActionResult EditA(int id)
        {
            AlumnoHandle ah = new AlumnoHandle();
            // var x = ah.ListarAlumnos().Find(amodel => amodel.idAlumno == id);

            try
            {
                var alumno =ah.ListarAlumnos().Find(amodel => amodel.idAlumno == id);
                if (alumno.fecha == "01/01/1888")
                {
                    alumno.fecha = "-";
                }
                return View(alumno);
            }

            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Alumno");
            }
        }

        [HttpPost]
        public ActionResult Edit(Alumno alumno)
        {
            if (alumno.Mail == null)
            {
                alumno.Mail = "-";
            }
            if (alumno.Telefono2 == null)
            {
                alumno.Telefono2 = "-";
            }
            if (alumno.Usuario == null)
            {
                alumno.Usuario = "-";
            }

            if (alumno.Direccion == null)
            {
                alumno.Direccion = "-";
            }

            if (alumno.Dni == 0)
            {
                alumno.Dni = 1111111;
            }

            string[] f1;
            var dd = "";
            var mm = "";
            var yy = "";
            var fecha2 = "";

            try
            {
                if (alumno.fecha == "" || alumno.fecha == null || alumno.fecha=="-")
                {
                    alumno.FechaNac = new DateTime(1888, 01, 01);
                }
                else {
                    f1 = alumno.fecha.Split('/');
                    dd = f1[0];
                    mm = f1[1];
                    yy = f1[2];
                    fecha2 = mm + "/" + dd + "/" + yy;
                    alumno.FechaNac = Convert.ToDateTime(fecha2);
                }
                AlumnoHandle ah = new AlumnoHandle();
                ah.EditarAlumno(alumno);
                _mensajeExito = "Alumno modificado exitosamente";
                return RedirectToAction("Alumno");
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Alumno");
            }
        }

        public ActionResult Borrar(int id)
        {
            try
            {
                AlumnoHandle ah = new AlumnoHandle();
                if (!ah.BorrarAlumno(id))
                {
                    _mensajeExito = "Alumno borrado exitosamente";
                    ModelState.Clear();
                }
                return RedirectToAction("Alumno");
            }
            catch (Exception e)
            {
                if (e.HResult == -2146232060)
                {
                    _mensajeError = "No es posible borrar un alumno que ya ha participado en los cursos de la escuela";
                }
                else
                {
                    _mensajeError = e.Message;
                }
                return RedirectToAction("Alumno");
            }
        }

        public ActionResult FiltradoAlumnos()
        {
            CursosHandle ch = new CursosHandle();
            try
            {
                ViewBag.mensajeError = _mensajeError;
                _mensajeError = "";
                ViewBag.listadoCursos = ch.ListarCursos();
                return View();
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return View();
            }
        }
        public ActionResult FiltroCurso()
        {
            AlumnoHandle ah = new AlumnoHandle();

            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }

            _mensajeExito = "";
            _mensajeError = "";

            try
            {
                ViewBag.fecha = _mensaje;
                _list = ah.FiltroCurso2();
                return View(_list);
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return View("FiltroCurso");
            }
        }

        public ActionResult FiltroCurso2(string fecha) // Modificado 20/10/2017
        {
            _listaA.Clear();
            DateTime fecha1 = new DateTime();
            DateTime fecha2 = new DateTime();
            String[] f1 = fecha.Split(' ');
            var fec1 = f1[0];
            var fec2 = f1[2];

            if (cultura == "en")
            {
                string[] fech = fec1.Split('/');
                var dd = fech[0];
                var mm = fech[1];
                var yy = fech[2];
                var fech1 = mm + "/" + dd + "/" + yy;

                string[] fech_2 = fec2.Split('/');
                var dd2 = fech_2[0];
                var mm2 = fech_2[1];
                var yy2 = fech_2[2];
                var fech2 = mm2 + "/" + dd2 + "/" + yy2;

                fecha1 = Convert.ToDateTime(fech1);
                fecha2 = Convert.ToDateTime(fech2);
            }
            else
            {
                fecha1 = Convert.ToDateTime(fec1);
                fecha2 = Convert.ToDateTime(fec2);
            }
            fecha2 = fecha2.AddHours(23).AddMinutes(59).AddSeconds(59);

            AlumnoHandle ah = new AlumnoHandle();
            try
            {
                _list = ah.FiltroCurso(fecha1, fecha2);
                if (_list.Count() == 0)
                {
                    _mensaje = "No hay alumnos que cursaron entre " + fecha1.ToString("dd/MM/yyyy") + " - " + fecha2.ToString("dd/MM/yyyy");
                    return RedirectToAction("FiltroCurso");
                }
                ViewBag.fecha = "Alumnos que tomaron cursos entre " + fecha1.ToString("dd/MM/yyyy") + " - " + fecha2.ToString("dd/MM/yyyy");
                return View("FiltroCurso", _list);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("FiltroCurso");
            }
        }

        [HttpPost]
        public ActionResult Filtro(int idcurso1 = 0, int idcurso2 = 0, string sexo = "")
        {
            AlumnoHandle ah = new AlumnoHandle();
            CursosHandle ch = new CursosHandle();
            try
            {
                if (idcurso2 == 0 && idcurso1 != 0)
                {
                    _list = ah.AlumnosQueCursaron(idcurso1);
                    var nombre = ch.TraerNombreCurso(idcurso1).FirstOrDefault().Nombre;
                    _mensaje = "Alumnos que cursaron " + nombre.ToUpper();
                }
                else if (idcurso2 != 0 && idcurso1 == 0)
                {
                    _list = ah.AlumnosQueNoCursaron(idcurso2);
                    var nombre = ch.TraerNombreCurso(idcurso2).FirstOrDefault().Nombre;
                    _mensaje = "Alumnos que NO cursaron " + nombre.ToUpper();
                }
                else if (idcurso2 != 0 && idcurso1 != 0)
                {
                    _list = ah.AlumnosConFiltro2(idcurso1, idcurso2);
                    var nombre = ch.TraerNombreCurso(idcurso1).FirstOrDefault().Nombre;
                    var nombre2 = ch.TraerNombreCurso(idcurso2).FirstOrDefault().Nombre;
                    _mensaje = "Alumnos que cursaron " + nombre.ToUpper() + " y NO " + nombre2.ToUpper();
                }
                else
                {
                    ViewBag.mensajeError = "Debe seleccionar al menos un filtro de curso";
                    return RedirectToAction("FiltradoAlumnos");
                }
                if (sexo != "")
                {
                    if (sexo == "Mujer")
                    {
                        _list = _list.Where(x => x.Sexo == "True").ToList();
                        _mensaje = _mensaje.Replace("Alumnos", "Mujeres");
                    }
                    else
                    {
                        _list = _list.Where(x => x.Sexo == "False").ToList();
                        _mensaje = _mensaje.Replace("Alumnos", "Varones");
                    }
                }
                ViewBag.bandera = "a";
                ViewBag.mensaje = _mensaje;
                ViewBag.listadoCursos = ch.ListarCursos();
                return View("FiltradoAlumnos", _list);
            }

            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("FiltradoAlumnos");
            }
        }

        public ActionResult GenerarExcel()
        {
            if (_list.Count() == 0 || _list == null)
            {
                _mensajeError = "No es posible imprimir un informe vacio. Por favor generelo otra vez.";
                return RedirectToAction("FiltradoAlumnos");
            }

            ExcelPackage ExcelPkg = new ExcelPackage();
            ExcelWorksheet Sheet1 = ExcelPkg.Workbook.Worksheets.Add("Sheet1");
            using (ExcelRange Rng = Sheet1.Cells[1, 1, 1, 1])
            {
                Rng.Value = _mensaje;
                Sheet1.Cells[3, 1].Value = "Apellido";
                Sheet1.Cells[3, 2].Value = "Nombre";
                Sheet1.Cells[3, 3].Value = "Telefono";
                Sheet1.Cells[3, 4].Value = "Mail";
                Sheet1.Cells[3, 5].Value = "Fecha";
                Sheet1.Cells["A3:E3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet1.Cells["A3:E3"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                Sheet1.Cells["A3:E3"].Style.Font.Bold = true;
                Rng.Style.Font.Size = 12;
                Rng.Style.Font.Bold = true;
            }

            var i = 4;
            foreach (var item in _list)
            {
                Sheet1.Cells[i, 1].Value = item.Apellido;
                Sheet1.Cells[i, 2].Value = item.Nombre;
                Sheet1.Cells[i, 3].Value = item.Telefono;
                Sheet1.Cells[i, 4].Value = item.Mail;
                Sheet1.Cells[i, 5].Value = item.FechaNac.ToShortDateString();
                i++;
            }

            Sheet1.Protection.IsProtected = false;
            Sheet1.Protection.AllowSelectLockedCells = false;
            // Sheet1.Cells.AutoFitColumns();
            Sheet1.Column(1).Width = 30;
            Sheet1.Column(2).Width = 30;
            Sheet1.Column(3).Width = 25;
            Sheet1.Column(4).Width = 31;
            Sheet1.Column(5).Width = 20;
            //ExcelPkg.SaveAs(new FileInfo(@"D:\\InformeAlumnos.xlsx"));

            byte[] excelEnBytes;
            using (MemoryStream ms = new MemoryStream())
            {
                ExcelPkg.SaveAs(ms);
                excelEnBytes = ms.ToArray();
            }

            return File(excelEnBytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "InformeAlumnos.xlsx");
        }

        public ActionResult GenerarExcel2()
        {
            if (_list.Count() == 0)
            {
                _mensajeError = "No es posible imprimir un informe vacio. Por favor generelo otra vez.";
                return RedirectToAction("FiltroCurso");
            }

            ExcelPackage ExcelPkg = new ExcelPackage();
            ExcelWorksheet Sheet1 = ExcelPkg.Workbook.Worksheets.Add("Sheet1");
            using (ExcelRange Rng = Sheet1.Cells[1, 1, 1, 1])
            {
                Rng.Value = _mensaje;
                Sheet1.Cells[3, 1].Value = "Alumno";
                Sheet1.Cells[3, 2].Value = "Telefono";
                Sheet1.Cells[3, 3].Value = "Curso";
                Sheet1.Cells[3, 4].Value = "Fecha inicio cursado";

                Sheet1.Cells["A3:E3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet1.Cells["A3:E3"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                Sheet1.Cells["A3:E3"].Style.Font.Bold = true;
                Rng.Style.Font.Size = 12;
                Rng.Style.Font.Bold = true;
            }

            var i = 4;
            foreach (var item in _list)
            {
                Sheet1.Cells[i, 1].Value = item.Apellido + "  " + item.Nombre;
                Sheet1.Cells[i, 2].Value = item.Telefono;
                Sheet1.Cells[i, 3].Value = item.Direccion;
                Sheet1.Cells[i, 4].Value = item.FechaNac.ToShortDateString();
                i++;
            }

            Sheet1.Protection.IsProtected = false;
            Sheet1.Protection.AllowSelectLockedCells = false;
            // Sheet1.Cells.AutoFitColumns();
            Sheet1.Column(1).Width = 30;
            Sheet1.Column(2).Width = 30;
            Sheet1.Column(3).Width = 25;
            Sheet1.Column(4).Width = 31;
            //Sheet1.Column(5).Width = 20;
            //ExcelPkg.SaveAs(new FileInfo(@"D:\\InformeAlumnos.xlsx"));

            byte[] excelEnBytes;
            using (MemoryStream ms = new MemoryStream())
            {
                ExcelPkg.SaveAs(ms);
                excelEnBytes = ms.ToArray();
            }

            return File(excelEnBytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "InformeAlumnosConCursos.xlsx");
        }
    }
}
