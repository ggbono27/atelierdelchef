﻿using System;
using System.Web.Mvc;
using Proyecto_1.Models;
using System.Linq;
using Proyecto_1.ViewModels;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Drawing;
using OfficeOpenXml.Style;
using OfficeOpenXml;

namespace Proyecto_1.Controllers
{
    public class TurnosController : Controller
    {
        private static String _mensajeExito;
        private static String _mensajeError;
        private static int _idCurso;
        private static int _idTurno;
        private static int _idClase;
        public static int _idAlumno;
        private static String _nombreCurso;
        private static String _nombreTurno;
        private static String _fechaTurno;
        private static string _horaInicioTurno;
        private static string _horaFinTurno;
        private static string _fechaClase;
        private static string _fechaClaseMesPrimero;
        private static string _horaInicioClase;
        private static string _horaFinClase;
        private static IList<Alumno> _lista2;
        private static Decimal _total;
        private static string _mensaje;
        private static int _control;

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult prueba(string cantidad, string precio, string idProducto)
        {
            var x = precio;
            return View();
        }

        // recibe id y nombre del curso del cual se muestran los turnos
        public ActionResult Turno(int id, String Nombre)
        {
            _idCurso = id;
            _nombreCurso = Nombre;
            TurnosHandle th = new TurnosHandle();
            ModelState.Clear();

            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }

            _control = 1;
            _mensajeExito = "";
            _mensajeError = "";
            ViewBag.nombreCurso = Nombre;
            return View(th.ListarTurnosDeUnCurso(id));
        }

        //Este action se lo usa cuando se hace click en el boton "guardar" de la vista "CrearTurno" para retornar a la vista "Turno" (primero pasa por el action "agregar" que es quien lo llama)
        //Difiere del action anterior en que no recibe parametros, puesto q estos ya estan guardados en variables estaticas.
        public ActionResult Turno2()
        {
            TurnosHandle th = new TurnosHandle();
            ModelState.Clear();

            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }

            _mensajeExito = "";
            _mensajeError = "";
            ViewBag.nombreCurso = _nombreCurso;
            return View("Turno", th.ListarTurnosDeUnCurso(_idCurso));
        }

        public ActionResult CrearTurno()
        {
            ViewBag.nombre = _nombreCurso;
            return View("CrearTurno");
        }

        public ActionResult CrearClases()
        {
            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }
            CursosHandle cuh =  new CursosHandle();
            TurnosHandle th = new TurnosHandle();
            ClasesHandle ch = new ClasesHandle();
            try
            {
                RecetasHandle rh = new RecetasHandle();
                ViewBag.listaRecetas = rh.ListarRecetas();
                ViewBag.listaAlumnos = th.AlumnosInscriptos(_idTurno);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
            }


            ViewBag.idturno = _idTurno;
            ViewBag.fecha = _fechaTurno;
            ViewBag.nombreCurso = _nombreCurso;
            ViewBag.nombreTurno = _nombreTurno;
            ViewBag.mensajeExito = _mensajeExito;
            ViewBag.mensajeError = _mensajeError;
           
            _mensajeError = "";
            _mensajeExito = "";

            List<ClasesConRecetas> ListaClasesConRecetas = new List<ClasesConRecetas>();
            var ListaClasesDeUnTurno = ch.ListarClasesDeUnTurno(_idTurno);
            var ListaRecetasDeUnTurno = ch.ListarRecetasDeTurno(_idTurno);

            foreach (var item in ListaClasesDeUnTurno)
            {
                var ccr = new ClasesConRecetas(item.idClase, item.idCurso, item.FechaInicio, item.FechaFin, item.Descripcion);
                ListaClasesConRecetas.Add(ccr);
            }

            var i = 0;
            var nombres = "";
            foreach (var item in ListaRecetasDeUnTurno)
            {
                if (i == item.idClase)
                {
                    nombres = nombres + " - " + item.Nombre;
                    ListaClasesConRecetas.Find(x => x.idClase == item.idClase).receta = nombres;
                }
                else
                {
                    ListaClasesConRecetas.Find(x => x.idClase == item.idClase).receta = item.Nombre;
                    nombres = item.Nombre;
                }
                i = item.idClase;
            }

            return View("CrearClases", ListaClasesConRecetas);

            //return View(ch.ListarClasesDeUnTurno(_idTurno));
        }
        public ActionResult CrearClases2(string id) //recibe idTurno y manda la vista para la creacion de las clases
        {
             _idTurno = Convert.ToInt32(id);
            CursosHandle cuh = new CursosHandle();
            TurnosHandle th = new TurnosHandle();
            if (_idCurso == 0)
            {
                _idCurso = th.TraeridCurso(_idTurno).FirstOrDefault().idCurso;
            }
            var t1 = th.ListarTurnosDeUnCursoSinRestriccion(_idCurso).Find(y => y.idTurno == _idTurno);
            _nombreTurno = t1.Dia.ToUpper() + " de " + t1.Inicio + " a " + t1.Fin;

            ClasesHandle ch = new ClasesHandle();
            try
            {
                _fechaTurno = ch.ListarClasesDeUnTurno(_idTurno).Max(t => t.FechaInicio).AddDays(7).ToShortDateString();
            }
            catch (Exception e)
            {
                _fechaTurno = t1.FechaInicio.ToShortDateString();
            }

            try
            {
                RecetasHandle rh = new RecetasHandle();
                ViewBag.listaRecetas = rh.ListarRecetas();
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
            }

            ViewBag.idTurno = _idTurno;
            ViewBag.fecha = _fechaTurno;
            ViewBag.nombreCurso = _nombreCurso;
            ViewBag.nombreTurno = _nombreTurno;
            ViewBag.idCurso = _idCurso;
            ViewBag.mensajeExito = _mensajeExito;
            ViewBag.mensajeError = _mensajeError;
            _mensajeError = "";
            _mensajeExito = "";

            List<ClasesConRecetas> ListaClasesConRecetas = new List<ClasesConRecetas>();
            var ListaClasesDeUnTurno = ch.ListarClasesDeUnTurno(_idTurno);
            var ListaRecetasDeUnTurno = ch.ListarRecetasDeTurno(_idTurno);

            foreach (var item in ListaClasesDeUnTurno)
            {
                var ccr = new ClasesConRecetas(item.idClase, item.idCurso, item.FechaInicio, item.FechaFin, item.Descripcion);
                ListaClasesConRecetas.Add(ccr);
            }

            var i = 0;
            var nombres = "";
            foreach (var item in ListaRecetasDeUnTurno)
            {
                if (i == item.idClase)
                {
                    nombres = nombres + " - " + item.Nombre;
                    ListaClasesConRecetas.Find(x => x.idClase == item.idClase).receta = nombres;
                }
                else
                {
                    ListaClasesConRecetas.Find(x => x.idClase == item.idClase).receta = item.Nombre;
                    nombres = item.Nombre;
                }

                i = item.idClase;
            }
            //var costo = cuh.TraerCostoCurso(_idCurso).FirstOrDefault().Costo;
            //var sumaPago = th.DetallePagoAlumno(_idTurno, id).Sum(x => x.Monto);
            //var Adeuda = costo - sumaPago;
          
            //ViewBag.deuda = Adeuda;
            ViewBag.control = _control;
            ViewBag.listaAlumnos = th.AlumnosInscriptos2(_idTurno, _idCurso);
            return View("CrearClases", ListaClasesConRecetas);
        }


        public ActionResult ClasesDeTurnoCalendario(int id) //recibe el id del turno para listar sus clases
        {
            TurnosHandle th = new TurnosHandle();
            CursosHandle ch = new CursosHandle();
            try
            {
               
                var idcurso = th.TraeridCurso(id).Max().idCurso;
                _idCurso = idcurso;
                _nombreCurso = ch.ListarCursos().Find(x => x.idCurso == idcurso).Nombre;
                return RedirectToAction("CrearClases2", "Turnos", new { id = id });
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return View("FullCalendar");
            }

        }
        public ActionResult ImprimirDiploma()
        {
            ViewBag.nombreCurso = "sushi 8";
            return View();
        }

        [HttpPost]
        public ActionResult AgregarClase(Clases c, int idReceta, int idReceta2 = 0, int idReceta3 = 0)
        {
            c.idTurno = _idTurno;
            c.idCurso = _idCurso;

            if (c.Descripcion == null)
                c.Descripcion = "Sin descripcion";

            //si se crea la clase directamente despues de crear el turno, entonces las variables privadas estaticas sirven. Sino hay que recuperar hora de inicio y fin del turno desde la DB
            if (_horaInicioTurno != null)
            {
                var horaInicio = Convert.ToDateTime(_horaInicioTurno);
                c.FechaInicio = new DateTime(c.FechaInicio.Year, c.FechaInicio.Month, c.FechaInicio.Day, horaInicio.Hour, horaInicio.Minute, horaInicio.Second);
                var horaFin = Convert.ToDateTime(_horaFinTurno);
                c.FechaFin = new DateTime(c.FechaInicio.Year, c.FechaInicio.Month, c.FechaInicio.Day, horaFin.Hour, horaFin.Minute, horaFin.Second);
            }
            else
            {
                TurnosHandle th = new TurnosHandle();
                var horaInicio = Convert.ToDateTime(th.ListarTurnosDeUnCurso(_idCurso).Find(t => t.idTurno == _idTurno).Inicio);
                c.FechaInicio = new DateTime(c.FechaInicio.Year, c.FechaInicio.Month, c.FechaInicio.Day, horaInicio.Hour, horaInicio.Minute, horaInicio.Second);
                var horaFin = Convert.ToDateTime(th.ListarTurnosDeUnCurso(_idCurso).Find(t => t.idTurno == _idTurno).Fin);
                c.FechaFin = new DateTime(c.FechaInicio.Year, c.FechaInicio.Month, c.FechaInicio.Day, horaFin.Hour, horaFin.Minute, horaFin.Second);
            }
            _fechaTurno = c.FechaInicio.AddDays(7).ToShortDateString();

            ClasesHandle ch = new ClasesHandle();
            try
            {
                var listita = ch.ListarClasesDeUnaFecha(c.FechaInicio, c.FechaFin, _idTurno);
                if (ch.ListarClasesDeUnaFecha(c.FechaInicio, c.FechaFin, _idTurno).Count() == 0)
                {
                    if (!ch.AgregarClase(c))
                    {
                        ch.AsignarRecetaClase(idReceta, _idCurso, _idTurno, ch.RecuperarIdClase());
                        if (idReceta2 != 0)
                        {
                            ch.AsignarRecetaClase(idReceta2, _idCurso, _idTurno, ch.RecuperarIdClase());
                        }
                        if (idReceta3 != 0)
                        {
                            ch.AsignarRecetaClase(idReceta3, _idCurso, _idTurno, ch.RecuperarIdClase());
                        }
                        ViewBag.mensajeExito = "Clase agregada exitosamente";
                        ModelState.Clear();
                    }
                }
                else
                {
                    ViewBag.mensajeError = "ya existe una clase en ese horario";
                }

                RecetasHandle rh = new RecetasHandle();
                ViewBag.listaRecetas = rh.ListarRecetas();

                ViewBag.fecha = _fechaTurno;
                ViewBag.nombreTurno = _nombreTurno;
                ViewBag.nombreCurso = _nombreCurso;

                List<ClasesConRecetas> ListaClasesConRecetas = new List<ClasesConRecetas>();
                var ListaClasesDeUnTurno = ch.ListarClasesDeUnTurno(_idTurno);
                var ListaRecetasDeUnTurno = ch.ListarRecetasDeTurno(_idTurno);

                foreach (var item in ListaClasesDeUnTurno)
                {
                    var ccr = new ClasesConRecetas(item.idClase, item.idCurso, item.FechaInicio, item.FechaFin, item.Descripcion);
                    ListaClasesConRecetas.Add(ccr);
                }

                var i = 0;
                var nombres = "";
                foreach (var item in ListaRecetasDeUnTurno)
                {
                    if (i == item.idClase)
                    {
                        nombres = nombres + " - " + item.Nombre;
                        ListaClasesConRecetas.Find(x => x.idClase == item.idClase).receta = nombres;
                    }
                    else
                    {
                        ListaClasesConRecetas.Find(x => x.idClase == item.idClase).receta = item.Nombre;
                        nombres = item.Nombre;
                    }
                    i = item.idClase;
                }

                return View("CrearClases", ListaClasesConRecetas);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("CrearClases", "Turnos");
            }
        }

        [HttpPost]
        public ActionResult AgregarTurnoNuevo(Turnos t, int cantidadClases = 0)
        {
            _horaInicioTurno = t.Inicio;
            _horaFinTurno = t.Fin;
            //var day = t.FechaInicio.DayOfWeek.ToString();
            t.idCurso = _idCurso;

            try
            {
                var hInicio = Convert.ToDateTime(t.Inicio);
                var hFin = Convert.ToDateTime(t.Fin);
                TurnosHandle th = new TurnosHandle();
                ClasesHandle ch = new ClasesHandle();
                //var listaTurnosDeUnaFecha = th.ListarTurnos().Where(x => x.FechaInicio.Date == t.FechaInicio.Date);

                Clases c = new Clases();
                c.FechaInicio = new DateTime(t.FechaInicio.Year, t.FechaInicio.Month, t.FechaInicio.Day, hInicio.Hour, hInicio.Minute, hInicio.Second);
                c.FechaFin = new DateTime(t.FechaInicio.Year, t.FechaInicio.Month, t.FechaInicio.Day, hFin.Hour, hFin.Minute, hFin.Second);
                c.idCurso = _idCurso;
                c.Descripcion = "Sin descripcion";

                for (int i = 0; i < cantidadClases - 1; i++)
                {
                    if (ch.ListarClasesDeUnaFecha(c.FechaInicio, c.FechaFin, _idTurno).Count() == 0)
                    {
                        c.FechaInicio = c.FechaInicio.AddDays(7);
                        c.FechaFin = c.FechaFin.AddDays(7);
                    }
                    else
                    {
                        ViewBag.mensajeError = "No se puede crear el turno en ese horario, debido a que coincide con otro, o una de sus clases coincide con alguna de otro turno";
                        return View("CrearTurno");
                    }
                }

                if (!th.AgregarTurno(t))
                {
                    _nombreTurno = t.Dia + " de " + t.Inicio + " a " + t.Fin;
                    _fechaTurno = t.FechaInicio.ToShortDateString();
                    ModelState.Clear();
                }

                var idTurno = th.RecuperarIdTurno();
                if (idTurno != 0)
                {
                    c.idTurno = idTurno;
                    _idTurno = idTurno;

                    for (int i = 0; i < cantidadClases; i++)
                    {
                        if (!ch.AgregarClase(c))
                        {
                            c.FechaInicio = c.FechaInicio.AddDays(-7);
                            c.FechaFin = c.FechaFin.AddDays(-7);
                        }
                        else
                        {
                            ViewBag.mensajeError = "Ocurrio un error inesperado. Intentelo mas tarde";
                            return View("CrearTurno");
                        }
                    }
                    _mensajeExito = "Turno agregado exitosamente con " + cantidadClases + " clases";
                    return RedirectToAction("Turno2", "Turnos");
                }
                ViewBag.mensajeError = "Error al intentar recuperar informacion de la base de datos. Intentelo mas tarde";
                return View("CrearTurno");
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return View("CrearTurno");
            }
        }

        public ActionResult EditTurno(string id)   //recibe id del turno a modificar
        {
            _idTurno = Convert.ToInt32(id);
            TurnosHandle th = new TurnosHandle();

            try
            {
                var lista = th.ListarTurnosDeUnCurso(_idCurso).Find(t => t.idTurno == _idTurno);
                _fechaTurno = lista.FechaInicio.ToString("dd/MM/yyyy");
                _horaInicioTurno = lista.Inicio;
                _horaFinTurno = lista.Fin;
                var ini = lista.FechaInicio;
                ViewBag.dia = lista.Dia;
                ViewBag.inicio = lista.Inicio;
                ViewBag.fin = lista.Fin;
                return View(lista);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Turno2");
            }
        }

        [HttpPost]
        public ActionResult Edit(Turnos t)
        {
            var hInicio = Convert.ToDateTime(t.Inicio);
            var hFin = Convert.ToDateTime(t.Fin);
            ClasesHandle ch = new ClasesHandle();
            List<Clases> listaClases = new List<Clases>();

            Clases c = new Clases();
            c.FechaInicio = new DateTime(t.FechaInicio.Year, t.FechaInicio.Month, t.FechaInicio.Day, hInicio.Hour, hInicio.Minute, hInicio.Second);
            c.FechaFin = new DateTime(t.FechaInicio.Year, t.FechaInicio.Month, t.FechaInicio.Day, hFin.Hour, hFin.Minute, hFin.Second);

            try
            {
                if (_fechaTurno != t.FechaInicio.ToShortDateString() || _horaInicioTurno != t.Inicio || _horaFinTurno != t.Fin)
                {
                    foreach (var item in ch.ListarClasesDeUnTurno(t.idTurno))
                    {
                        if (ch.ListarClasesDeUnaFecha(c.FechaInicio, c.FechaFin, _idTurno).Count() != 0)
                        {
                            ViewBag.mensajeError = "Existe una clase que se superpone con el horario " + c.FechaInicio.ToString("HH:mm") + " a " + c.FechaFin.ToString("HH:mm") + " para la fecha: " + c.FechaInicio.ToShortDateString();
                            return View("EditTurno");
                        }
                        item.FechaInicio = c.FechaInicio;
                        item.FechaFin = c.FechaFin;
                        listaClases.Add(item);
                        c.FechaInicio = item.FechaInicio.AddDays(7);
                        c.FechaFin = item.FechaFin.AddDays(7);
                    }

                    foreach (var item in listaClases)
                    {
                        ch.EditarClase(item);
                    }
                    ch.ListarClasesDeUnTurno(t.idTurno);
                    TurnosHandle th = new TurnosHandle();
                    th.EditarTurno(t);
                }
                _mensajeExito = "Turno modificado exitosamente";
                return RedirectToAction("Turno2");
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Turno2");
            }
        }

        public ActionResult BorrarTurno(int id)
        {
            try
            {
                TurnosHandle th = new TurnosHandle();
                if (th.BorrarTurno(id))
                {
                    _mensajeExito = "Turno borrado exitosamente";
                    ModelState.Clear();
                }
                return RedirectToAction("Turno2");
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Turno2");
            };
        }

        public ActionResult EditClase(int id)    //recibe id de la clase a editar
        {
            try
            {
                _idClase = id;
                RecetasHandle rh = new RecetasHandle();
                ClasesHandle ch = new ClasesHandle();
                var clase = ch.ListarClasesDeUnTurno(_idTurno).Find(t => t.idClase == id);
                _fechaClase = clase.fecha;
                _fechaClaseMesPrimero = clase.FechaInicio.ToString("MM/dd/yyyy");
                _horaInicioClase = clase.FechaInicio.ToString("HH:mm");
                _horaFinClase = clase.FechaFin.ToString("HH:mm");
                ViewBag.inicio = _horaInicioClase;
                ViewBag.fin = _horaFinClase;
                ViewBag.listaRecetasClase = ch.ListarRecetasDeClase(id);
                ViewBag.listaRecetas = rh.ListarRecetas();
                ViewBag.idTurno = _idTurno;
                return View(clase);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("CrearClases2", new { id = _idTurno });
            }
        }

        public ActionResult GuardarEditedClase(Clases c, string inicio, string fin, int idReceta = 0, int idReceta2 = 0, int idReceta3 = 0)
        {
            var fecha = Convert.ToDateTime(c.fecha);

            var horaInicio = Convert.ToDateTime(inicio);
            c.FechaInicio = new DateTime(fecha.Year, fecha.Month, fecha.Day, horaInicio.Hour, horaInicio.Minute, horaInicio.Second);
            var horaFin = Convert.ToDateTime(fin);
            c.FechaFin = new DateTime(fecha.Year, fecha.Month, fecha.Day, horaFin.Hour, horaFin.Minute, horaFin.Second);

            try
            {
                ClasesHandle ch = new ClasesHandle();
                var horita = c.FechaInicio;
                var horaString = c.FechaInicio.ToString("MM/dd/yyyy");
                var listaDeClases = ch.ListarClasesDeUnTurno(_idTurno);
                if (c.FechaInicio.ToString("MM/dd/yyyy") != _fechaClaseMesPrimero || inicio != _horaInicioClase || fin != _horaFinClase)
                {
                    if (ch.ListarClasesDeUnaFecha(c.FechaInicio, c.FechaFin, _idTurno).Count() != 0)
                    {
                        _mensajeError = "Existe una clase que se superpone con el horario " + c.FechaInicio.ToString("HH:mm") + " a " + c.FechaFin.ToString("HH:mm") + " para la fecha: " + c.FechaInicio.ToString("dd/MM/yyyy");
                        RecetasHandle rh = new RecetasHandle();
                        var clase = listaDeClases.Find(t => t.idClase == _idClase);
                        try
                        {
                            ViewBag.inicio = clase.FechaInicio.ToString("HH:mm");
                            ViewBag.fin = clase.FechaFin.ToString("HH:mm");
                            ViewBag.listaRecetasClase = ch.ListarRecetasDeClase(_idClase);
                            ViewBag.listaRecetas = rh.ListarRecetas();
                        }
                        catch (Exception e)
                        {
                            _mensajeError = e.Message;
                        }
                        ViewBag.mensajeError = _mensajeError;
                        //ViewBag.mensajeExito = _mensajeExito;

                        return View("EditClase", clase);
                    }
                }


                ch.EditarClase(c);
                horita = horita.AddDays(7);
               // foreach (var item in listaDeClases.Where(y => y.FechaInicio > (Convert.ToDateTime(_fechaClase).AddDays(1))))
               foreach (var item in listaDeClases.Where(y => y.FechaInicio > (Convert.ToDateTime(_fechaClase).AddDays(1))))
                {
                    item.FechaInicio = horita;
                    ch.EditarClase(item);
                    horita = horita.AddDays(7);
                }

                if (idReceta != 0)
                {
                    ch.AsignarRecetaClase(idReceta, _idCurso, _idTurno, _idClase);
                }
                if (idReceta2 != 0)
                {
                    ch.AsignarRecetaClase(idReceta2, _idCurso, _idTurno, _idClase);
                }
                if (idReceta3 != 0)
                {
                    ch.AsignarRecetaClase(idReceta3, _idCurso, _idTurno, _idClase);
                }
                _mensajeExito = "Clase modificada exitosamente";
                return RedirectToAction("CrearClases");
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("CrearClases");
            }
        }

        public ActionResult QuitarRecetaDeClase(int idReceta)
        {
            ClasesHandle ch = new ClasesHandle();
            RecetasHandle rh = new RecetasHandle();
            try
            {
                ch.QuitarRecetaClase(idReceta, _idClase);
                ViewBag.mensajeExito = "Receta quitada exitosamente";
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
            }

            var clase = ch.ListarClasesDeUnTurno(_idTurno).Find(t => t.idClase == _idClase);
            try
            {
                ViewBag.inicio = clase.FechaInicio.ToString("HH:mm");
                ViewBag.fin = clase.FechaFin.ToString("HH:mm");
                ViewBag.listaRecetasClase = ch.ListarRecetasDeClase(_idClase);
                ViewBag.listaRecetas = rh.ListarRecetas();
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
            }

            return View("EditClase", clase);
        }

        public ActionResult BorrarClase(int id)
        {
            try
            {
                ClasesHandle ch = new ClasesHandle();
                if (!ch.BorrarClase(id))
                {
                    _mensajeExito = "Clase borrada exitosamente";
                    ModelState.Clear();
                }
                return RedirectToAction("CrearClases");
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("CrearClases");
            };
        }

        public ActionResult VerDetalle(int id, string nombre, string apellido) //recibe el id del alumno nombre y apellido
        {
            CursosHandle ch = new CursosHandle();
            _idAlumno = id;
            TurnosHandle th = new TurnosHandle();

            if (_mensajeExito != null)
            {
                ViewBag.mensajeExito = _mensajeExito;
            }

            if (_mensajeError != null)
            {
                ViewBag.mensajeError = _mensajeError;
            }

            ViewBag.mensajeExito = _mensajeExito;
            ViewBag.mensajeError = _mensajeError;
            _mensajeExito = "";
            _mensajeError = "";

            try
            {
                var costo = ch.TraerCostoCurso(_idCurso).FirstOrDefault().Costo;
                var sumaPago = th.DetallePagoAlumno(_idTurno, id).Sum(x => x.Monto);
                var Adeuda = costo - sumaPago;
                ViewBag.sumaPago = sumaPago;
                ViewBag.deuda = Adeuda;
                ViewBag.idTurno = _idTurno;
                ViewBag.idalumno = id;
                ViewBag.alumno = apellido + " " + nombre;
                ViewBag.nombre = nombre;
                ViewBag.apellido = apellido;
                return View(th.DetallePagoAlumno(_idTurno, id));
            }
            catch (Exception e)
            {
            }
            return View();
        }

        public ActionResult PagoAlumno(int monto, string usuario, string Nombre, string Apellido, int idAlumno)
        {
            TurnosHandle th = new TurnosHandle();
            try
            {
                if (!th.PagoAlumno(monto, _idAlumno, _idTurno, _idCurso, usuario))
                {
                    _mensajeExito = "Pago realizado exitosamente";
                }

                return RedirectToAction("VerDetalle", "Turnos", new { id = idAlumno, nombre = Nombre, apellido = Apellido });
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
            }
            return View("AlumnosInscriptos", th.AlumnosInscriptos(_idTurno));
        }

        public ActionResult AlumnosInscriptos(string id) //recibe idturno
        {
            var idT = Convert.ToInt32(id);
            _idTurno = idT;
            TurnosHandle th = new TurnosHandle();
            try
            {
                var t1 = th.ListarTurnosDeUnCurso(_idCurso).Find(y => y.idTurno == idT);
                _nombreTurno = t1.Dia.ToUpper() + " de " + t1.Inicio + " a " + t1.Fin;
                var ListaA = th.AlumnosInscriptos(idT);
                ViewBag.nombreCurso = _nombreCurso;
                ViewBag.nombreTurno = _nombreTurno;
                ViewBag.idCurso = _idCurso;
                ViewBag.idTurno = idT;
                return View(ListaA);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
            }
            ViewBag.nombreCurso = _nombreCurso;
            ViewBag.mensajeError = _mensajeError;
            return View("Turno", th.ListarTurnosDeUnCurso(_idCurso));
        }

        public ActionResult AlumnosInscriptos2(int idTurno, int idCurso, string nombrecurso) //recibe idturno y idCurso para la vista cronograma
        {
            _idTurno = idTurno;
            _idCurso = idCurso;
            _nombreCurso = nombrecurso;
            TurnosHandle th = new TurnosHandle();
            try
            {
                var t1 = th.ListarTurnosDeUnCurso(_idCurso).Find(y => y.idTurno == _idTurno);
                _nombreTurno = t1.Dia.ToUpper() + " de " + t1.Inicio + " a " + t1.Fin;
                var ListaA = th.AlumnosInscriptos(_idTurno);
                ViewBag.nombreCurso = _nombreCurso;
                ViewBag.nombreTurno = _nombreTurno;
                return View("AlumnosInscriptos", ListaA);
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
            }
            ViewBag.nombreCurso = _nombreCurso;
            ViewBag.mensajeError = _mensajeError;
            return View("Turno", th.ListarTurnosDeUnCurso(_idCurso));
        }


        public ActionResult ResumenTurnos()
        {
            TurnosHandle th = new TurnosHandle();
            CursosTurnos ct = new CursosTurnos();
            CursosHandle ch = new CursosHandle();
            ct.l_Cursos = ch.ListarCursos2();
            ct.l_Turnos = th.ListarTurnos2();

            return View(ct);
        }

        public ActionResult AsistenciaClases(int idclase, int idturno, int idcurso, string fecha) //recibe el id de la clase seleccionada
        {
            DateTime fechaClase = Convert.ToDateTime(fecha);
            _idClase = idclase;
            _idCurso = idcurso;
            _idTurno = idturno;
            TurnosHandle th = new TurnosHandle();
            ClasesHandle ch = new ClasesHandle();
            CursosHandle cuh = new CursosHandle();

            try
            {
                _idTurno = th.ListarTurnoDeClase(idclase).Find(x => x.idClase == idclase).idTurno;
                ViewBag.idTurno = _idTurno;
                var ListaAlumnos = th.AlumnosInscriptos(_idTurno);

                _lista2 = ch.AsistenciaTomada(_idClase, _idTurno);

                if (_lista2.Where(x => x.Mail == "Ausente").Count() == ListaAlumnos.Count())
                {
                    ViewBag.control = 1;
                }

                else
                {
                    ViewBag.control = 0;
                }

                ViewBag.listaAsistencia = ch.AsistenciaTomada(_idClase, _idTurno);
                ViewBag.nombreCurso = _nombreCurso;
                ViewBag.nombreTurno = _nombreTurno;
                ViewBag.fecha = fechaClase.ToString("dd/MM/yyyy");
                return View(ListaAlumnos);
            }
            catch (Exception e)
            {
                ViewBag.mensajeError = e.Message;
                return View();
            }
        }

        public ActionResult Asistencia(string vector)
        {
            ClasesHandle ch = new ClasesHandle();
            VentasHandle vh = new VentasHandle();
            LineaVentaHandle lvh = new LineaVentaHandle();
            try
            {
                Char delimiter = ',';
                String[] substrings = vector.Split(delimiter);
                for (int i = 0; i < substrings.Length; i++)
                {
                    if (_lista2.Count != 0)
                    {
                        foreach (var item in _lista2)
                        {
                            if (Convert.ToInt32(substrings[i]) == item.idAlumno && item.Mail == "Ausente")
                            {
                                ch.AsistenciaDeAlumnoEnClase(_idCurso, _idTurno, _idClase, Convert.ToInt32(substrings[i]));
                                _mensajeExito = "Asistencia tomada exitosamente";
                            }
                        }

                    }

                    //else
                    //{
                    //    ch.AsistenciaDeAlumnoEnClase(_idCurso, _idTurno, _idClase, Convert.ToInt32(substrings[i]));
                    //    _mensajeExito = "Asistencia agregada exitosamente";
                    //}

                }

                // vh.AgregarUsoDeInsumo(lvh.LineasDeClase(_idClase), substrings.Length); 

                return RedirectToAction("CrearClases", "Turnos");
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
            }
            return RedirectToAction("CrearClases", "Turnos");
        }

        public ActionResult AsistenciaClaseDictada()
        {
            ClasesHandle ch = new ClasesHandle();
            try
            {
                return View(ch.ListaDeAsistencia(_idTurno));
            }
            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("CrearClases", "Turnos");
            }
        }

        public ActionResult FullCalendar()
        {
            _control = 0;
            try
            {
                ClasesHandle ch = new ClasesHandle();
                return View(ch.ListarEventosFullCalendar());
            }

            catch (Exception e)
            {
                _mensajeError = e.Message;
                return RedirectToAction("Index", "Index");
            }
        }
    }
}
