﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Proyecto_1.Models;

namespace Proyecto_1.ViewModels
{
    public class GastosGeneral
    {

        public List<Productos> l_Producto { get; set; }
        public List<LineaGastos> l_LineaCompras { get; set; }
        public Gastos CompraI { get; set; }
        
        public GastosGeneral()
        {
        
        }

        public GastosGeneral(List<Productos> lp, List<LineaGastos> lg, Gastos g)
        {
            l_Producto = lp;
            l_LineaCompras = lg;
            CompraI = g;
        }
    }
}