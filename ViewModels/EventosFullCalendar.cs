﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_1.ViewModels
{
    public class EventosFullCalendar
    {
        public string title { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public string url { get; set; }

        public int idTurno { get; set; }
        public int idCurso { get; set; }

        public int Alumnos { get; set; }
        public string backgroundColor { get; set; }

        public EventosFullCalendar(string Title, DateTime Start, DateTime End, string Url, int turno, int curso, int alumnos, string backgroundcolor)
        {
            title = Title + Convert.ToString(alumnos);
            start = Start;
            end = End;
            url = Url;
            idTurno = turno;
            idCurso = curso;
            Alumnos = alumnos;
            backgroundColor = backgroundcolor;
        }

        public EventosFullCalendar(string Title, DateTime Start, DateTime End, int alumnos, string backgroundcolor)
        {
            title = Title + Convert.ToString(alumnos);
            start = Start;
            end = End;
            Alumnos = alumnos;
            backgroundColor = backgroundcolor;
        }

        public EventosFullCalendar() { }
    }
}