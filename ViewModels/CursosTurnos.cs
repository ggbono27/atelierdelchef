﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Proyecto_1.Models;

namespace Proyecto_1.ViewModels
{
    public class CursosTurnos
    {
        public List<Turnos> l_Turnos { get; set; }
        public List<Cursos> l_Cursos { get; set; }

        public CursosTurnos()
        { }

        public CursosTurnos(List<Turnos> listaturnos, List<Cursos> listacursos)
        {
            l_Cursos = listacursos;
            l_Turnos = listaturnos;
        } 
    

    }
}