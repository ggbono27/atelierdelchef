﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Proyecto_1.ViewModels
{
    public class ClasesConRecetas
    {
        public int idClase { get; set; }
        public int idCurso { get; set; }
        public int idTurno { get; set; }

        [System.ComponentModel.DisplayName("Fecha inicio de clase:"),
         DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.DateTime)]
        [Required]
        public DateTime FechaInicio { set; get; }     //esta variable se usa como la fecha de la clase. 
        public DateTime FechaFin { get; set; }
        public String Descripcion { set; get; }
 

        [System.ComponentModel.DisplayName("Fecha inicio de clase:"),
         DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.DateTime)]
        [Required]
        public String fecha { get; set; }

        public string receta { get; set; }
        public ClasesConRecetas()
        { }

        public ClasesConRecetas(int idClas, int idCur, DateTime FI, DateTime FF, String Desc)
        {
            idClase = idClas;
            idCurso = idCur;
            FechaInicio = FI;
            FechaFin = FF;
            Descripcion = Desc;
            fecha = FI.ToShortDateString();
        }
        public ClasesConRecetas(int idClas, int idCur, DateTime FI, DateTime FF, String Desc, int idTur)
        {
            idClase = idClas;
            idCurso = idCur;
            FechaInicio = FI;
            FechaFin = FF;
            Descripcion = Desc;
            fecha = FI.ToShortDateString();
            idTurno = idTur;
        }
    }
}