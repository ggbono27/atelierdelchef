﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace Proyecto_1.ViewModels
{
    public static class MenuClass
    {
        //public static MvcHtmlString LiActionLink(this HtmlHelper html, string text, string action, string controller)
        //{
        //    var text2 = text.Substring(2);
        //    var context = html.ViewContext;
        //    if (context.Controller.ControllerContext.IsChildAction)
        //        context = html.ViewContext.ParentActionViewContext;
        //    var routeValues = context.RouteData.Values;
        //    var currentAction = routeValues["action"].ToString();
        //    var currentController = routeValues["controller"].ToString();

        //    var str = String.Format("<li role=\"presentation\"{0}>{1}</li>",
        //        currentAction.Equals(action, StringComparison.InvariantCulture) &&
        //        currentController.Equals(controller, StringComparison.InvariantCulture) ?
        //        "class=\"active\"" :
        //        String.Empty, html.ActionLink(text, action, controller).ToHtmlString()
        //    );
        //    return new MvcHtmlString(str);
        //}
        public static MvcHtmlString NavigationLink(this HtmlHelper html, string linkText, string action, string controller, object routeValues = null, object css = null)
        {
            TagBuilder aTag = new TagBuilder("a");
            TagBuilder liTag = new TagBuilder("li");
            var htmlAttributes = HtmlHelper.AnonymousObjectToHtmlAttributes(css);
            string url = (routeValues == null) ?
                (new UrlHelper(html.ViewContext.RequestContext)).Action(action, controller)
                : (new UrlHelper(html.ViewContext.RequestContext)).Action(action, controller, routeValues);

            aTag.MergeAttribute("href", url);
            aTag.InnerHtml = linkText;
            aTag.MergeAttributes(htmlAttributes);

            if (action.ToLower() == html.ViewContext.RouteData.Values["action"].ToString().ToLower() && controller.ToLower() == html.ViewContext.RouteData.Values["controller"].ToString().ToLower())
                liTag.MergeAttribute("class", "active");

            liTag.InnerHtml = aTag.ToString(TagRenderMode.Normal);
            return new MvcHtmlString(liTag.ToString(TagRenderMode.Normal));
        }

    }
}