﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Proyecto_1.Models;


namespace Proyecto_1.ViewModels
{
    public class Inscripcion
    {
        public List<Alumno> l_Alumno { get; set; }
        public List<Turnos> l_Turno { get; set; }

        public Inscripcion()
        { }

        public Inscripcion(List<Alumno> a, List<Turnos> t)
        {
            l_Alumno = a;
            l_Turno = t;
        }

    }
}