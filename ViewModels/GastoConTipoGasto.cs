﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Proyecto_1.Models;

namespace Proyecto_1.ViewModels
{
    public class GastoConTipoGasto
    {

        public String Nombre { get; set; }

        public int idTipoGasto { get; set; }

        public GastoConTipoGasto()
        { }

        public GastoConTipoGasto(string nombre,int idtipogasto)
        {
            Nombre = nombre;
            idTipoGasto = idtipogasto;
        }


    }
}