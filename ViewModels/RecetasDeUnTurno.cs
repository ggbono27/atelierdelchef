﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Proyecto_1.ViewModels
{
    public class RecetasDeUnTurno
    {
        public int idReceta { get; set; }

        [DisplayName("Preparacion:")]
        public string Preparacion { get; set; }

        [Required]
        [DisplayName("Nombre:")]
        public string Nombre { get; set; }

        public int idClase { get; set; }

        public RecetasDeUnTurno()
        { }

        public RecetasDeUnTurno(int idR, string prep, string nombre)
        {
            idReceta = idR;
            Nombre = nombre;
            Preparacion = prep;
        }
    }
}