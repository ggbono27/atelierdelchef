﻿using System;

namespace Proyecto_1.ViewModels
{
    public class Caja
    {
        public String Concepto { get; set; }
        public string Concepto2 { get; set; }
        public DateTime Fecha { set; get; }

        public Decimal Monto { get; set; }

        public string Usuario { get; set; }

        public Caja()
        { }

        public Caja(string concepto, DateTime fecha, decimal monto, string usuario, string concepto2)
        {
            Concepto2 = concepto2;
            Concepto = concepto;
            Fecha = fecha;
            Monto = monto;
            Usuario = usuario;
        }

    }
}