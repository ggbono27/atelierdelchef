﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Proyecto_1.Models;

namespace Proyecto_1.ViewModels
{
    public class VentaGeneral
    {

        public List<Productos> l_Producto { get; set; }
        public List<LineaVentas> l_LineaVenta { get; set; }
        public Ventas VentaP { get; set; }

        public VentaGeneral()
        {

        }
 

        public VentaGeneral(List<Productos> lp, List<LineaVentas> lv, Ventas v)
        {
            l_Producto = lp;
            l_LineaVenta = lv;
            VentaP = v;
        }

    }
}