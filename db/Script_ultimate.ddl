
CREATE TABLE [TIPOGASTOS]
( 
	[idTipoGasto]           int  NOT NULL  IDENTITY ( 1,1 ) NOT FOR REPLICATION ,
	[Tipo]           varchar(50)  NOT NULL 

)
go

ALTER TABLE [TIPOGASTOS]
	ADD CONSTRAINT [XPKTIPOGASTOS] PRIMARY KEY  CLUSTERED ([idTipoGasto] ASC)
go

CREATE TABLE [ALUMNOS]
( 
	[idAlumno]           int  NOT NULL  IDENTITY ( 1,1 ) NOT FOR REPLICATION ,
	[Apellido]           varchar(50)  NOT NULL ,
	[Nombre]             varchar(50)  NOT NULL ,
	[Direccion]          varchar(100)   NULL ,
	[DNI]                int  NOT NULL ,
	[FechaNac]           date   NULL ,
	[Mail]               varchar(40)  NULL ,
	[Telefono]          varchar(30)  NOT NULL,
        [Telefono2]         varchar(30) null,
        [Usuario]           varchar(30) null,
        [Borrado]            bit null,
	[Sexo] 		     bit null
)
go

ALTER TABLE [ALUMNOS]
	ADD CONSTRAINT [XPKALUMNOS] PRIMARY KEY  CLUSTERED ([idAlumno] ASC)
go

CREATE TABLE [ASISTENCIA]
( 
	[idAlumno]           int  NOT NULL ,
	[Estado]             varchar(15)  NULL ,
	[idClase]            int  NOT NULL ,
	[idCurso]            int  NOT NULL ,
	[idTurno]            int  NOT NULL 
)
go

ALTER TABLE [ASISTENCIA]
	ADD CONSTRAINT [XPKASISTENCIA] PRIMARY KEY  CLUSTERED ([idAlumno] ASC,[idClase] ASC,[idCurso] ASC,[idTurno] ASC)
go

CREATE TABLE [CLASES]
( 
	[idClase]            int  NOT NULL  IDENTITY ( 1,1 ) ,
	[Descripcion]        varchar(1500)  NULL ,
	[HoraInicio]         datetime  NULL ,
	[HoraFin]            datetime  NULL ,
	[idCurso]            int  NOT NULL ,
	[idTurno]            int  NOT NULL 
)
go

ALTER TABLE [CLASES]
	ADD CONSTRAINT [XPKCLASES] PRIMARY KEY  CLUSTERED ([idClase] ASC,[idCurso] ASC,[idTurno] ASC)
go

CREATE TABLE [CURSOS]
( 
	[Nombre]             varchar(75)  NOT NULL ,
	[Descripcion]        varchar(750)  NULL ,
	[Costo]              int  NOT NULL ,
	[idCurso]            int  NOT NULL  IDENTITY ( 1,1 ) NOT FOR REPLICATION 
)
go

ALTER TABLE [CURSOS]
	ADD CONSTRAINT [XPKCURSOS] PRIMARY KEY  CLUSTERED ([idCurso] ASC)
go

CREATE TABLE [GASTOS]
( 
	[idGasto]            int  NOT NULL  IDENTITY ( 1,1 ) NOT FOR REPLICATION ,
	[idTipoGasto]        int NOT NULL,
	[Fecha]              date  NOT NULL ,
        [Usuario]            varchar(30) null,
        [Nombre]             varchar(40)  NULL,
	[Monto]               decimal(6, 2)     NULL
)
go

CREATE TABLE [INTERESADOS]
( 
	[idInteresado]            int  NOT NULL  IDENTITY ( 1,1 ) NOT FOR REPLICATION ,
	[Nombre]              varchar(50)  NOT NULL ,
	[Fecha]			Date NOT NULL,
	[Usuario]		nvarchar(50) NOT NULL,	
        [Telefono]            varchar(30) not null, 
	[Observacion]		varchar(250) not null 
)
go

ALTER TABLE [INTERESADOS]
	ADD CONSTRAINT [XPKINTERESADOS] PRIMARY KEY  CLUSTERED ([idInteresado] ASC)
go



CREATE TABLE [INTERES]
( 
	[idInteresado]           int  NOT NULL ,
	[idCurso]            int  NOT NULL 
)
go

ALTER TABLE [INTERES]
	ADD CONSTRAINT [XPKINTERES] PRIMARY KEY  CLUSTERED ([idInteresado] ASC,[idCurso] ASC)
go

ALTER TABLE [GASTOS]
	ADD CONSTRAINT [XPKGASTOS] PRIMARY KEY  CLUSTERED ([idGasto] ASC, [idTipoGasto] ASC)
go

CREATE TABLE [LINEAGASTOS]
( 
	[idGasto]            int  NOT NULL ,
	[idProducto]         int  NOT NULL ,
	[idTipoGasto]        int NOT NULL,
	[Precio]             decimal(6,2)  Null  ,
	[Cantidad]           int  NOT NULL 
)
go

ALTER TABLE [LINEAGASTOS]
	ADD CONSTRAINT [XPKLINEAGASTOS] PRIMARY KEY  CLUSTERED ([idGasto] ASC,[idProducto] ASC, [idTipoGasto] ASC)
go

CREATE TABLE [LINEAVENTAS]
( 
	[idVenta]            int  NOT NULL ,
	[idProducto]         int  NOT NULL ,
	[Precio]             Decimal(6,2) NULL ,
	[Cantidad]           int  NOT NULL 
)
go

ALTER TABLE [LINEAVENTAS]
	ADD CONSTRAINT [XPKLINEAVENTAS] PRIMARY KEY  CLUSTERED ([idVenta] ASC,[idProducto] ASC)
go

CREATE TABLE [PAGOS]
( 
	[idPago]             int  NOT NULL  IDENTITY ( 1,1 ) ,
	[Concepto]           varchar(15)  NOT NULL ,
	[Monto]              int not NULL ,
	[Fecha]              datetime  NOT NULL ,
        [Usuario]             varchar(50) not null,
	[idAlumno]           int  NOT NULL ,
	[idTurno]            int  NOT NULL ,
	[idCurso]            int  NOT NULL,
        [Contr] 	     [nvarchar](200) NULL
)
go

ALTER TABLE [PAGOS]
	ADD CONSTRAINT [XPKPAGOS] PRIMARY KEY  CLUSTERED ([idPago] ASC,[idAlumno] ASC,[idTurno] ASC,[idCurso] ASC)
go

CREATE TABLE [PREPARACIONES]
( 
	[idReceta]           int  NOT NULL ,
	[idProducto]         int  NOT NULL ,
	[Cantidad]           int  NOT NULL,
        [Unidad]            varchar(15) not null
 )
go

ALTER TABLE [PREPARACIONES]
	ADD CONSTRAINT [XPKPREPARACIONES] PRIMARY KEY  CLUSTERED ([idReceta] ASC,[idProducto] ASC)
go

CREATE TABLE [PRODUCTOS]
( 
	[idProducto]         int  NOT NULL  IDENTITY ( 1,1 ) NOT FOR REPLICATION ,
	[Nombre]             varchar(75)  NOT NULL ,
	[Unidad]             varchar(15)  NULL ,
	[Tipo]               varchar(30)  NULL,
        [Precio]             int  NULL,
        [Borrado]            bit null 
)
go

ALTER TABLE [PRODUCTOS]
	ADD CONSTRAINT [XPKINGREDIENTES] PRIMARY KEY  CLUSTERED ([idProducto] ASC)
go

CREATE TABLE [RECETAS]
( 
	[idReceta]           int  NOT NULL  IDENTITY ( 1,1 ) NOT FOR REPLICATION ,
        [Nombre]             varchar(30) NOT NULL,
	[Preparacion]        varchar(2500)  NULL,
	[Ingredientes] 		varchar(2000) null,
	[Tips]			varchar(1000) null
)
go

ALTER TABLE [RECETAS]
	ADD CONSTRAINT [XPKRECETAS] PRIMARY KEY  CLUSTERED ([idReceta] ASC)
go

CREATE TABLE [TURNOS]
( 
	[idTurno]            int  NOT NULL  IDENTITY ( 1,1 ) ,
	[Dia]                varchar(10)  NOT NULL ,
	[idCurso]            int  NOT NULL ,
	[FechaInicio]        date  NULL,
	[Inicio] 	     varchar(5) NOT NULL,
	[Fin]		     varchar(5) NOT NULL,
	[CantidadAlumnos]    INT NULL
)
go

ALTER TABLE [TURNOS]
	ADD CONSTRAINT [XPKTURNOS] PRIMARY KEY  CLUSTERED ([idTurno] ASC,[idCurso] ASC)
go

CREATE TABLE [USOS]
( 
	[idReceta]           int  NOT NULL ,
	[idClase]            int  NOT NULL ,
	[idCurso]            int  NOT NULL ,
	[idTurno]            int  NOT NULL 
)
go

ALTER TABLE [USOS]
	ADD CONSTRAINT [XPKUSOS] PRIMARY KEY  CLUSTERED ([idReceta] ASC,[idClase] ASC,[idCurso] ASC,[idTurno] ASC)
go

CREATE TABLE [USUARIOS]
( 
	[idUsuario]          int  NOT NULL  IDENTITY ( 1,1 ) ,
	[Username]           varchar(35)  NOT NULL ,
	[Pass]               varchar(150)  NOT NULL,
	[Salt]		     varchar(150) NOT NULL
)
go

ALTER TABLE [USUARIOS]
	ADD CONSTRAINT [XPKUsuarios] PRIMARY KEY  CLUSTERED ([idUsuario] ASC)
go

CREATE TABLE [VENTAS]
( 
	[idVenta]            int  NOT NULL  IDENTITY ( 1,1 ) NOT FOR REPLICATION ,
	[FechaVenta]         datetime  NOT NULL, 
        [Concepto]            varchar(30) null,
         [Usuario]          varchar(30)   null,
	[Descripcion]		varchar(100) Null,
	[Monto]			int null
)
go

ALTER TABLE [VENTAS]
	ADD CONSTRAINT [XPKVENTAS] PRIMARY KEY  CLUSTERED ([idVenta] ASC)
go


ALTER TABLE [ASISTENCIA]
	ADD CONSTRAINT [R_16] FOREIGN KEY ([idAlumno]) REFERENCES [ALUMNOS]([idAlumno])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [ASISTENCIA]
	ADD CONSTRAINT [R_20] FOREIGN KEY ([idClase],[idCurso],[idTurno]) REFERENCES [CLASES]([idClase],[idCurso],[idTurno])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [CLASES]
	ADD CONSTRAINT [R_29] FOREIGN KEY ([idTurno],[idCurso]) REFERENCES [TURNOS]([idTurno],[idCurso])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [INTERES]
	ADD CONSTRAINT [R_30] FOREIGN KEY ([idCurso]) REFERENCES [CURSOS]([idCurso])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [INTERES]
	ADD CONSTRAINT [R_31] FOREIGN KEY ([idInteresado]) REFERENCES [INTERESADOS]([idInteresado])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [LINEAGASTOS]
	ADD CONSTRAINT [R_3] FOREIGN KEY ([idGasto], [idTipoGasto]) REFERENCES [GASTOS]([idGasto], [idTipoGasto])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [LINEAGASTOS]
	ADD CONSTRAINT [R_4] FOREIGN KEY ([idProducto]) REFERENCES [PRODUCTOS]([idProducto])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [GASTOS]
	ADD CONSTRAINT [R_33] FOREIGN KEY ([idTipoGasto]) REFERENCES [TIPOGASTOS]([idTipoGasto])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [LINEAVENTAS]
	ADD CONSTRAINT [R_5] FOREIGN KEY ([idVenta]) REFERENCES [VENTAS]([idVenta])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [LINEAVENTAS]
	ADD CONSTRAINT [R_6] FOREIGN KEY ([idProducto]) REFERENCES [PRODUCTOS]([idProducto])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [PAGOS]
	ADD CONSTRAINT [R_10] FOREIGN KEY ([idAlumno]) REFERENCES [ALUMNOS]([idAlumno])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [PAGOS]
	ADD CONSTRAINT [R_27] FOREIGN KEY ([idTurno],[idCurso]) REFERENCES [TURNOS]([idTurno],[idCurso])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [PREPARACIONES]
	ADD CONSTRAINT [R_7] FOREIGN KEY ([idReceta]) REFERENCES [RECETAS]([idReceta])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [PREPARACIONES]
	ADD CONSTRAINT [R_9] FOREIGN KEY ([idProducto]) REFERENCES [PRODUCTOS]([idProducto])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [TURNOS]
	ADD CONSTRAINT [R_25] FOREIGN KEY ([idCurso]) REFERENCES [CURSOS]([idCurso])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [USOS]
	ADD CONSTRAINT [R_15] FOREIGN KEY ([idReceta]) REFERENCES [RECETAS]([idReceta])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [USOS]
	ADD CONSTRAINT [R_19] FOREIGN KEY ([idClase],[idCurso],[idTurno]) REFERENCES [CLASES]([idClase],[idCurso],[idTurno])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

SET IDENTITY_INSERT TIPOGASTOS ON
insert into TIPOGASTOS (idTipoGasto, Tipo) values (8, 'AgregarCorreccion')
insert into TIPOGASTOS (idTipoGasto, Tipo) values (3, 'CompraBazar')
insert into TIPOGASTOS (idTipoGasto, Tipo) values (5, 'AgregarStock')
SET IDENTITY_INSERT TIPOGASTOS OFF
go



